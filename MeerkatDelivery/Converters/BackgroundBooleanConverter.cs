﻿using System;
using Xamarin.Forms;

namespace MeerkatDelivery.Converters
{
	public class BackgroundBooleanConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (!(bool)value)
				return "#eaeaea";
			else
				return "White";
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return "";
		}
	}
}
