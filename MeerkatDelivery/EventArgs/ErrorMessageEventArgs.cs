﻿using System;

public class ErrorMessageEventArgs : EventArgs
{
	public string Message { get; set; }
	public ErrorMessageEventArgs(string message)
	{
		Message = message;
	}
}
