﻿using System;

public class OrderStateChangedEventArgs : EventArgs
{
	public string State { get; set; }
	public OrderStateChangedEventArgs(string state)
	{
		State = state;
	}
}
