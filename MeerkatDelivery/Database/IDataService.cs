using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MeerkatDelivery.Database
{
	public interface IDataService {

		#region testing
		Task<int> QueryAsync(string query);
		int Query(string query);
		#endregion


		#region Generalised
		Task<int> SaveAsync(object dbObj);
		Task<int> InsertAsync(object dbObj);
		Task<int> UpdateAsync(object dbObj);
		Task<int> DeleteAsync(object dbObj);

		int Save(object dbObj);
		int Insert(object dbObj);
		int Update(object dbObj);
		int Delete(object dbObj);

		#endregion
		#region ExampleBusinessObject
		Task<List<ExampleBusinessObject>> AllExampleBusinessObjectsAsync ();
		Task<ExampleBusinessObject> GetExampleBusinessObjectAsync(int id);
		Task<ExampleBusinessObject> GetFirstExampleBusinessObjectAsync ();
		Task<int> DeleteAllExampleBusinessObjectsAsync ();

		List<ExampleBusinessObject> AllExampleBusinessObjects();
		ExampleBusinessObject GetExampleBusinessObject(int id);
		ExampleBusinessObject GetFirstExampleBusinessObject();
		int DeleteAllExampleBusinessObjects();
		#endregion
		#region Order
		Task<List<Order>> AllOrdersAsync();
		Task<List<Order>> AllOrdersAsync(int clientId);
		Task<Order> GetOrderAsync(int id);
		Task<int> DeleteAllOrdersAsync();
		Task<DateTime> MaxOrderUpdateTimestampAsync(int clientId);
		Task<List<Order>> AllChangedOrdersAsync();

		Task<ServerImageOrder> GetServerImageOrderAsync(int id);

		List<Order> AllOrders();
		List<Order> AllOrders(int clientId);
		Order GetOrder(int id);
		int DeleteAllOrders();

		#endregion
		#region User
		Task<List<User>> AllUsersAsync();
		Task<List<User>> AllUsersAsync(int clientId);
		Task<User> GetUserAsync(int id);
		Task<int> DeleteAllUsersAsync();
		Task<DateTime> MaxUserUpdateTimestampAsync(int clientId);
		Task<List<User>> AllChangedUsersAsync();

		List<User> AllUsers();
		List<User> AllUsers(int clientId);
		User GetUser(int id);
		int DeleteAllUsers();
		#endregion
		#region UserLog
		//Task<UserLog> GetUserLogAsync(int id);
		//Task<int> DeleteAllUserLogsAsync();
		//Task<List<UserLog>> AllUserLogsAsync();
		//Task<int> CountUserLogsAsync();

		//List<UserLog> AllUserLogs();
		//UserLog GetUserLog(int id);
		//int DeleteAllUserLogs();
		#endregion
		#region Cookie
		Task<MyCookie> GetCookieAsync();
		Task<List<MyCookie>> AllCookiesAsync();

		MyCookie GetCookie();
		List<MyCookie> AllCookies();
		#endregion
	}
}