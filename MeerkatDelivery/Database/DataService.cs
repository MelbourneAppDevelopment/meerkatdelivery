using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using MeerkatDelivery.Helpers;
using System.Threading.Tasks;
using SQLite;
using System.Diagnostics;
using System;
using Xamarin.Forms;

// Reference: https://github.com/praeclarum/sqlite-net

namespace MeerkatDelivery.Database
{
	// Using ORM syntax
	public class DataService : IDataService
	{
		#region Private

		static object locker = new object();
		static SQLiteAsyncConnection _connectionAsync;

		static SQLiteConnection _connection;
		#endregion
		#region Constuctor

		public DataService()
		{
			var dbpath = DependencyService.Get<IFileHelper>().GetLocalFilePath("MeerkatDB.db3");

			_connectionAsync = new SQLiteAsyncConnection(dbpath);
			_connection = new SQLiteConnection(dbpath);

			this.CreateTables();
		}
		#endregion
		#region CreateTables
		private void CreateTables()
		{
			// Dependent table creates

			// Independent table creates
			try
			{
				_connection.CreateTable<ExampleBusinessObject>();
			}
			catch
			{
				_connection.DropTable<ExampleBusinessObject>();
				_connection.CreateTable<ExampleBusinessObject>();
			}
			try
			{
				_connection.CreateTable<Order>();
			}
			catch
			{
				_connection.DropTable<Order>();
				_connection.CreateTable<Order>();
			}
			try
			{
				_connection.CreateTable<ServerImageOrder>();
			}
			catch
			{
				_connection.DropTable<ServerImageOrder>();
				_connection.CreateTable<ServerImageOrder>();
			}
			try
			{
				_connection.CreateTable<MyCookie>();
			}
			catch
			{
				_connection.DropTable<MyCookie>();
				_connection.CreateTable<MyCookie>();
			}

			try
			{
				_connection.CreateTable<User>();
			}
			catch
			{
				_connection.DropTable<User>();
				_connection.CreateTable<User>();
			}

			//try {
			//	_connection.CreateTable<UserLog>();
			//} catch {
			//	_connection.DropTable<UserLog>();
			//	_connection.CreateTable<UserLog>();
			//}
		}
		private async void CreateTablesAsync()
		{
			// Dependent table creates

			// Independent table creates
			try
			{
				await _connectionAsync.CreateTableAsync<ExampleBusinessObject>();
			}
			catch
			{
				await _connectionAsync.DropTableAsync<ExampleBusinessObject>();
				await _connectionAsync.CreateTableAsync<ExampleBusinessObject>();
			}
			try
			{
				await _connectionAsync.CreateTableAsync<Order>();
			}
			catch
			{
				await _connectionAsync.DropTableAsync<Order>();
				await _connectionAsync.CreateTableAsync<Order>();
			}
			try
			{
				await _connectionAsync.CreateTableAsync<MyCookie>();
			}
			catch
			{
				await _connectionAsync.DropTableAsync<MyCookie>();
				await _connectionAsync.CreateTableAsync<MyCookie>();
			}

			try
			{
				await _connectionAsync.CreateTableAsync<User>();
			}
			catch
			{
				await _connectionAsync.DropTableAsync<User>();
				await _connectionAsync.CreateTableAsync<User>();
			}

			//try {
			//	_connectionAsync.CreateTable<UserLog>();
			//} catch {
			//	_connectionAsync.DropTable<UserLog>();
			//	_connectionAsync.CreateTable<UserLog>();
			//		}
		}
		#endregion
		#region Generalised

		public async Task<int> QueryAsync(string query)
		{
			var result = await _connectionAsync.ExecuteScalarAsync<int>(query);
			return result;
		}

		//Some example generalised code form https://github.com/praeclarum/sqlite-net/blob/master/src/SQLite.cs
		//		public T FindWithQuery<T> (string query, params object[] args) where T : new()
		//		{
		//			return Query<T> (query, args).FirstOrDefault ();
		//		}


		public async Task<int> SaveAsync(object dbObj)
		{
			try
			{
				var result = await _connectionAsync.UpdateAsync(dbObj);
				if (result == 1)
					return result;
				return await _connectionAsync.InsertAsync(dbObj);
			}
			catch
			{
				return await _connectionAsync.InsertAsync(dbObj);
			}
		}

		public async Task<int> InsertAsync(object dbObj)
		{
			var result = await _connectionAsync.InsertAsync(dbObj);
			return result;
		}

		public async Task<int> UpdateAsync(object dbObj)
		{
			try
			{
				return await _connectionAsync.UpdateAsync(dbObj);
			}
			catch
			{
				return await _connectionAsync.InsertAsync(dbObj);
			}
		}

		public async Task<int> DeleteAsync(object dbObj)
		{
			return await _connectionAsync.DeleteAsync(dbObj);
		}


		public int Query(string query)
		{
			lock (locker)
			{

				var result = _connection.ExecuteScalar<int>(query);
				return result;
			}
		}

		//Some example generalised code form https://github.com/praeclarum/sqlite-net/blob/master/src/SQLite.cs
		//		public T FindWithQuery<T> (string query, params object[] args) where T : new()
		//		{
		//			return Query<T> (query, args).FirstOrDefault ();
		//		}


		public int Save(object dbObj)
		{
			try
			{
				lock (locker)
				{

					var result = _connection.Update(dbObj);
					if (result == 1)
						return result;
					return _connection.Insert(dbObj);
				}
			}
			catch
			{
				lock (locker)
				{
					return _connection.Insert(dbObj);
				}
			}
		}

		public int Insert(object dbObj)
		{
			lock (locker)
			{
				return _connection.Insert(dbObj);
			}
		}

		public int Update(object dbObj)
		{
			try
			{
				lock (locker)
				{
					return _connection.Update(dbObj);
				}
			}
			catch
			{
				lock (locker)
				{
					return _connection.Insert(dbObj);
				}
			}
		}

		public int Delete(object dbObj)
		{
			lock (locker)
			{
				return _connection.Delete(dbObj);
			}
		}

		#endregion
		#region User
		public async Task<List<User>> AllUsersAsync(int clientId)
		{
			return await _connectionAsync.Table<User>().Where(item => item.ClientId == clientId).ToListAsync();
		}

		public async Task<List<User>> AllUsersAsync()
		{
			return await _connectionAsync.Table<User>().ToListAsync();
		}

		public async Task<User> GetUserAsync(int id)
		{
			return await _connectionAsync.Table<User>().Where(item => item.Id == id).FirstOrDefaultAsync();
		}

		public async Task<User> GetFirstUserAsync()
		{
			return await _connectionAsync.Table<User>().FirstOrDefaultAsync();
		}

		public async Task<int> DeleteAllUsersAsync()
		{
			await _connectionAsync.DropTableAsync<User>();
			await _connectionAsync.CreateTableAsync<User>();
			return 0;
		}

		public async Task<DateTime> MaxUserUpdateTimestampAsync(int clientId)
		{
			var order = await _connectionAsync.Table<User>().Where(item => item.ClientId == clientId).OrderByDescending(item => item.UpdateTimestamp).FirstOrDefaultAsync();
			if (order == null)
				return DateTime.MinValue;
			else
				return order.UpdateTimestamp;
		}

		public async Task<List<User>> AllChangedUsersAsync()
		{
			try
			{
				var result = await _connectionAsync.Table<User>().Where(item => item.HasChanged == true).ToListAsync();
				return result;
			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION: {0}", e);
			}
			return null;
		}

	
		public List<User> AllUsers(int clientId)
		{
			lock (locker)
			{
				return _connection.Table<User>().Where(item => item.ClientId == clientId).ToList();
			}
		}

		public List<User> AllUsers()
		{
			lock (locker)
			{
				return _connection.Table<User>().ToList();
			}
		}

		public User GetUser(int id)
		{
			lock (locker)
			{
				return _connection.Table<User>().Where(item => item.Id == id).FirstOrDefault();
			}
		}

		public User GetFirstUser()
		{
			lock (locker)
			{
				return _connection.Table<User>().FirstOrDefault();
			}
		}

		public int DeleteAllUsers()
		{
			lock (locker)
			{
				_connection.DropTable<User>();
				_connection.CreateTable<User>();
				return 0;
			}
		}

		#endregion
		#region ExampleBusinessObject
		public async Task<List<ExampleBusinessObject>> AllExampleBusinessObjectsAsync()
		{
			return await _connectionAsync.Table<ExampleBusinessObject>().ToListAsync();
		}

		public async Task<ExampleBusinessObject> GetExampleBusinessObjectAsync(int id)
		{
			return await _connectionAsync.Table<ExampleBusinessObject>().Where(item => item.Id == id).FirstOrDefaultAsync();
		}

		public async Task<ExampleBusinessObject> GetFirstExampleBusinessObjectAsync()
		{
			return await _connectionAsync.Table<ExampleBusinessObject>().FirstOrDefaultAsync();
		}

		public async Task<int> DeleteAllExampleBusinessObjectsAsync()
		{
			await _connectionAsync.DropTableAsync<ExampleBusinessObject>();
			await _connectionAsync.CreateTableAsync<ExampleBusinessObject>();
			return 0;
		}

		public List<ExampleBusinessObject> AllExampleBusinessObjects()
		{
			lock (locker)
			{
				return _connection.Table<ExampleBusinessObject>().ToList();
			}
		}

		public ExampleBusinessObject GetExampleBusinessObject(int id)
		{
			lock (locker)
			{
				return _connection.Table<ExampleBusinessObject>().Where(item => item.Id == id).FirstOrDefault();
			}
		}

		public ExampleBusinessObject GetFirstExampleBusinessObject()
		{
			lock (locker)
			{
				return _connection.Table<ExampleBusinessObject>().FirstOrDefault();
			}
		}

		public int DeleteAllExampleBusinessObjects()
		{
			lock (locker)
			{
				_connection.DropTable<ExampleBusinessObject>();
				_connection.CreateTable<ExampleBusinessObject>();
				return 0;
			}
		}

		#endregion
		#region Order
		public async Task<List<Order>> AllOrdersAsync()
		{
			try
			{
				Debug.WriteLine("hello there");
				var result = await _connectionAsync.Table<Order>()
                               //.OrderBy(item => new {item.DeliveredTime, item.DeliverySequence, item.Id})
                               .ToListAsync();
				if (Settings.DebugMode) {
					var message = String.Format("Found orders (in order):");
					foreach (var order in result)
					{
						message = message + String.Format("\n     {0}", order.ToString());
					}
					Debug.WriteLine(message);
				}

				return result;
			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION: {0}", e);
			}
			return null;
		}

		public async Task<List<Order>> AllOrdersAsync(int clientId)
		{
			Debug.WriteLine("hello from dataservices allOrdersAsync");
			try
			{
				var result = await _connectionAsync.Table<Order>()
							.Where(item => item.ClientId == clientId)
							//.OrderBy(item => new { item.DeliveredTime, item.DeliverySequence, item.Id })
							.ToListAsync();
				if (Settings.DebugMode)
				{
					var message = String.Format("Found orders (in order):");
					foreach (var order in result)
					{
						message = message + String.Format("\n     {0}", order.ToString());
					}
					Debug.WriteLine(message);
				}
				return result;
			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION: {0}", e);
			}
			return null;
		}

		public async Task<Order> GetOrderAsync(int id)
		{
			var result = await _connectionAsync.Table<Order>().Where(item => (int)item.Id == id).FirstOrDefaultAsync();
			return result;
		}

		public async Task<Order> GetFirstOrderAsync()
		{
			return await _connectionAsync.Table<Order>().FirstOrDefaultAsync();
		}

		public async Task<int> DeleteAllOrdersAsync()
		{
			await _connectionAsync.DropTableAsync<Order>();
			await _connectionAsync.CreateTableAsync<Order>();
			return 0;
		}

		public async Task<DateTime> MaxOrderUpdateTimestampAsync(int clientId)
		{
			var order = await _connectionAsync.Table<Order>().Where(item => item.ClientId == clientId).OrderByDescending(item => item.UpdateTimestamp).FirstOrDefaultAsync();
			if (order == null)
				return DateTime.MinValue;
			else
				return order.UpdateTimestamp;
		}

		public async Task<List<Order>> AllChangedOrdersAsync()
		{
			try
			{
				var result = await _connectionAsync.Table<Order>().Where(item => item.HasChanged == true).ToListAsync();
				return result;
			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION: {0}", e);
			}
			return null;
		}

		public async Task<ServerImageOrder> GetServerImageOrderAsync(int id)
		{
			return await _connectionAsync.Table<ServerImageOrder>().Where(item => item.Id == id).FirstOrDefaultAsync();
		}


	
		public List<Order> AllOrders()
		{
			try
			{
				lock (locker)
				{
					var result = _connection.Table<Order>().ToList();
					return result;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION: {0}", e);
			}
			return null;
		}

		public List<Order> AllOrders(int clientId)
		{
			try
			{
				lock (locker)
				{
					var result = _connection.Table<Order>().Where(item => item.ClientId == clientId).ToList();
					return result;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION: {0}", e);
			}
			return null;
		}

		public Order GetOrder(int id)
		{
			lock (locker)
			{
				return _connection.Table<Order>().Where(item => item.Id == id).FirstOrDefault();
			}
		}

		public Order GetFirstOrder()
		{
			lock (locker)
			{
				return _connection.Table<Order>().FirstOrDefault();
			}
		}

		public int DeleteAllOrders()
		{
			lock (locker)
			{
				_connection.DropTable<Order>();
				_connection.CreateTable<Order>();
				return 0;
			}
		}


		#endregion
		#region UserLog
		//public UserLog GetUserLog(int id)
		//{
		//	lock (locker) {
		//		return _connectionAsync.Table<UserLog> ().Where(item => item.Id == id).FirstOrDefault ();
		//	}
		//}
		//public List<UserLog> AllUserLogs()
		//{
		//	lock (locker) {
		//		Debug.WriteLine("QUERY: select * from UserLog order by Id");
		//		var result = _connectionAsync.Table<UserLog> ().OrderBy(item => item.Id).ToList();
		//		Debug.WriteLine("QUERY returns: {0}", result.Count);
		//		return result;
		//	}
		//}
		//public int DeleteAllUserLogs ()
		//{
		//	lock (locker) {
		//		_connectionAsync.DropTable<UserLog> ();
		//		_connectionAsync.CreateTable<UserLog>();
		//		return 0;
		//	}
		//}
		//public int CountUserLogs()
		//{
		//	lock (locker) {
		//		var query = String.Format ("select count(*) from UserLog");
		//		Debug.WriteLine("QUERY: {0}", query);
		//		var result = _connectionAsync.ExecuteScalar<int> (query);
		//		Debug.WriteLine("QUERY returns: {0}", result);
		//		return result;
		//	}
		//}

		//public UserLog GetUserLog(int id)
		//{
		//	lock (locker) {
		//		return _connection.Table<UserLog ().Where(item => item.Id == id).FirstOrDefault ();
		//	}
		//}
		//public List<UserLog> AllUserLogs()
		//{
		//	lock (locker) {
		//		Debug.WriteLine("QUERY: select * from UserLog order by Id");
		//		var result = _connection.Table<UserLog> ().OrderBy(item => item.Id).ToList();
		//		Debug.WriteLine("QUERY returns: {0}", result.Count);
		//		return result;
		//	}
		//}
		//public int DeleteAllUserLogs ()
		//{
		//	lock (locker) {
		//		_connection.DropTable<UserLog> ();
		//		_connection.CreateTable<UserLog>();
		//		return 0;
		//	}
		//}
		//public int CountUserLogs()
		//{
		//	lock (locker) {
		//		var query = String.Format ("select count(*) from UserLog");
		//		Debug.WriteLine("QUERY: {0}", query);
		//		var result = _connection.ExecuteScalar<int> (query);
		//		Debug.WriteLine("QUERY returns: {0}", result);
		//		return result;
		//	}
		//}

		#endregion
		#region Cookie
		public async Task<MyCookie> GetCookieAsync()
		{
			return await _connectionAsync.Table<MyCookie>().Where(item => item.Id == 1).FirstOrDefaultAsync();
		}
		public async Task<List<MyCookie>> AllCookiesAsync()
		{
			return await _connectionAsync.Table<MyCookie>().ToListAsync();
		}

		public MyCookie GetCookie()
		{
			lock (locker)
			{
				return _connection.Table<MyCookie>().Where(item => item.Id == 1).FirstOrDefault();
			}
		}
		public List<MyCookie> AllCookies()
		{
			lock (locker)
			{
				return _connection.Table<MyCookie>().ToList();
			}
		}

		#endregion
	}
}