﻿using System;
using SQLite;

namespace MeerkatDelivery.Database
{
	public class MyCookie : BaseDatabaseObject
	{
		[PrimaryKey]
		public int Id { get; set; }        // = 1
		public string AccessToken { get; set; }
		public int UserId { get; set; }
		public int ClientId { get; set; }        // all, incomplete, not assigned for picking, not assigned for delivery, 
		public DateTime OrderStartDate { get; set; }        // time, person, suburb, delivery seq, reverse delivery seq
		public DateTime OrderEndDate { get; set; }
		public DateTime DeliverStartDate { get; set; }
		public DateTime DeliverEndDate { get; set; }
		public int? PickBy { get; set; }
		public int? DeliverBy { get; set; }
		public string OrderStatus { get; set; }
		public override string ToString()
		{
			return string.Format("{0} token:{1} user:{2} ", Id, AccessToken, UserId);
		}

		public MyCookie()
		{
		}
	}
}

