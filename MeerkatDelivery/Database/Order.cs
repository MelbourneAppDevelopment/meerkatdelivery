﻿using System;
using SQLite;

namespace MeerkatDelivery.Database
{
	public class Order : BaseDatabaseObject
	{
		public Order() : base()
		{
			DeliveredTime = DateTime.MaxValue;
			DeliveryDate = DateTime.Today;
			DeliveryTime = DateTime.MinValue;
			LoadedTime = DateTime.MinValue;
			PickedTime = DateTime.MinValue;
			PickingTime = DateTime.MinValue;

			Status = OrderStatus.Initialised.ToString();
		}

		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		public int ClientId { get; set; }

		public bool Deleted { get; set; }
		public bool HasChanged { get; set; }

		public double Amount { get; set; }
		public DateTime OrderDate { get; set; }
		public string CardText { get; set; }
		public int ClearingNumber { get; set; }
		public string ContactPhone { get; set; }
		public DateTime DeliveredTime { get; set; }
		public DateTime DeliveryDate { get; set; }
		public double DeliveredGpsLatitude { get; set; }
		public double DeliveredGpsLongitude { get; set; }
		public byte[] DeliveredImage { get; set; }
		public int DeliverySequence { get; set; }
		public DateTime DeliveryTime { get; set; }
		public string DeliveryTimeCode { get; set; }
		public int DeliveryUserId { get; set; }
		public double GpsLatitude { get; set; }
		public double GpsLongitude { get; set; }
		public byte[] Image1 { get; set; }
		public byte[] Image2 { get; set; }
		public DateTime LoadedTime { get; set; }
		public string Notes { get; set; }
		public string OrderNumber { get; set; }
		public string OrderSystem { get; set; }
		public string OrderSystemClientId { get; set; }
		public DateTime PickedTime { get; set; }
		public DateTime PickingTime { get; set; }
		public int PickingUserId { get; set; }
		public string Priority { get; set; }
		public string Product1Code { get; set; }
		public string Product1Description { get; set; }
		public string Product2Code { get; set; }
		public string Product2Description { get; set; }
		public string Product3Code { get; set; }
		public string Product3Description { get; set; }
		public string RecipientCompanyName { get; set; }
		public string RecipientEmail { get; set; }
		public string RecipientName { get; set; }
		public string RecipientPhone { get; set; }
		public string RecipientState { get; set; }
		public string RecipientStreetAddress { get; set; }
		public string RecipientSuburb { get; set; }
		public string RecipientZip { get; set; }
		public string SenderAddress { get; set; }
		public string SenderCompanyName { get; set; }
		public string SenderEmail { get; set; }
		public string SenderName { get; set; }
		public string SenderPhone { get; set; }
		public string SenderState { get; set; }
		public string SenderSuburb { get; set; }
		public string SenderZip { get; set; }
		public string SpecialInstructions { get; set; }
		public string Status { get; set; }        // ordered, cancelled, picked, loaded, delivered, failed delivery

		public override string ToString()
		{
			return String.Format("Id {0}|OrderSystem {1}|OrderNumber {2}|Deleted {52}|\rOrderSystemClientId {4}|SenderName {5}|SenderCompanyName {6}|SenderAddress {7}|SenderSuburb {8}|SenderState {9}|SenderZip {10}|SenderPhone {11}|SenderEmail {12}|RecipientName {13}|RecipientCompanyName {14}|RecipientStreetAddress {15}|RecipientSuburb {16}|RecipientState {17}|RecipientZip {18}|RecipientPhone {19}|RecipientEmail {20}|ContactPhone {21}|DeliveryDate {22}|DeliveryTime {23}|DeliveryTimeCode {24}|SpecialInstructions {25}|Priority {26}|Product1Code {27}|Product1Description {28}|Product2Code {29}|Product2Description {30}|Product3Code {31}|Product3Description {32}|Notes {33}|Amount {34}|CardText {35}|ClearingNumber {36}|GpsLatitude {37}|GpsLongitude {38}|DeliverySequence {39}|Image1 {40}|Image2 {41}|PickingUserId {42}|PickingTime {43}|PickedTime {44}|DeliveryUserId {45}|LoadedTime {46}|DeliveredImage {47}|DeliveredTime {48}|DeliveredGpsLatitude {49}|DeliveredGpsLongitude {50}|Status {51}|",
                this.Id,
				this.OrderSystem,
				this.OrderNumber,
				this.ClientId,
				this.OrderSystemClientId,
				this.SenderName,
				this.SenderCompanyName,
				this.SenderAddress,
				this.SenderSuburb,
				this.SenderState,
				this.SenderZip,
				this.SenderPhone,
				this.SenderEmail,
				this.RecipientName,
				this.RecipientCompanyName,
				this.RecipientStreetAddress,
				this.RecipientSuburb,
				this.RecipientState,
				this.RecipientZip,
				this.RecipientPhone,
				this.RecipientEmail,
				this.ContactPhone,
				this.DeliveryDate,
				this.DeliveryTime,
				this.DeliveryTimeCode,
				this.SpecialInstructions,
				this.Priority,
				this.Product1Code,
				this.Product1Description,
				this.Product2Code,
				this.Product2Description,
				this.Product3Code,
				this.Product3Description,
				this.Notes,
				this.Amount,
				this.CardText,
				this.ClearingNumber,
				this.GpsLatitude,
				this.GpsLongitude,
				this.DeliverySequence,
				this.Image1,
				this.Image2,
				this.PickingUserId,
				this.PickingTime,
				this.PickedTime,
			                     this.DeliveryUserId,
				this.LoadedTime,
				this.DeliveredImage,
				this.DeliveredTime,
				this.DeliveredGpsLatitude,
				this.DeliveredGpsLongitude,
				this.Status,
			                     this.Deleted
			                    );
		}
	}
}
