﻿using System;
using System.Reflection;
using SQLite;

namespace MeerkatDelivery.Database
{
	public class BaseDatabaseObject : IDisposable
	{
		public BaseDatabaseObject()
		{
			DeviceCreateTimestamp = DateTime.MinValue;
			DeviceUpdateTimestamp = DateTime.MinValue;
			UpdateTimestamp = DateTime.MinValue;
		}
		
		public string DeviceTimeZone { get; set; }
		public DateTime DeviceCreateTimestamp { get; set; }
		public DateTime DeviceUpdateTimestamp { get; set; }
		public int CreateUserId { get; set; }
		public int UpdateUserId { get; set; }
		[Indexed(Name = "UpdateTimestampIdx", Unique = false)]
		public DateTime UpdateTimestamp { get; set; }

		public void Copy(object copyFrom)
		{
			var fromProperties = copyFrom.GetType().GetRuntimeProperties();
			foreach (var fromProperty in fromProperties)
			{
				var name = fromProperty.Name;
				var value = fromProperty.GetValue(copyFrom, null);
				var toProperty = this.GetType().GetRuntimeProperty(name);

				toProperty.SetValue(this, value);
			}
		}
		#region IDisposable implementation
		private bool _disposed;

		~BaseDatabaseObject()
		{
			Dispose(disposing: false);
		}

		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				_disposed = true;

				DisposeUnmanagedResource();

				if (disposing)
				{
					DisposeOtherManagedResource ();
				}
			}
		}

		protected virtual void DisposeUnmanagedResource()
		{
		}
		protected virtual void DisposeOtherManagedResource()
		{
		}
		#endregion
	}
}

