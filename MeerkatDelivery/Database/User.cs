﻿
using System;
using SQLite;

namespace MeerkatDelivery.Database
{
	public class User : BaseDatabaseObject
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		public bool HasChanged { get; set; }

		public string Type { get; set; }        // admin, picker, courier, unknown
		public int ClientId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string ProposedPassword { get; set; }
		public string AccessToken { get; set; }
		public string FacebookId { get; set; }
		public string FacebookToken { get; set; }
		public string FacebookDetailsJson { get; set; }
		public string Token { get; set; }
		public string Status { get; set; }        // pending, ok, loggedin
		public string Manufacturer { get; set; }
		public string Model { get; set; }
		public string Serial { get; set; }
		public string AppVersion { get; set; }

		public override string ToString()
		{
			return String.Format("Id {0}|Type {1}|ClientId {2}|FirstName {3}|LastName {4}|Email {5}|Password {6}|ProposedPassword {7}|AccessToken {8}|FacebookId {9}|FacebookToken {10}|FacebookDetailsJson {11}|Token {12}|Status {13}|Manufacturer {14}|Model {15}|Serial {16}|AppVersion {17}",

				this.Id,
				this.Type,
				this.ClientId,
				this.FirstName,
				this.LastName,
				this.Email,
				this.Password,
				this.ProposedPassword,
				this.AccessToken,
				this.FacebookId,
				this.FacebookToken,
				this.FacebookDetailsJson,
				this.Token,
				this.Status,
				this.Manufacturer,
				this.Model,
				this.Serial,
				this.AppVersion);
		}
	}
}
