﻿using SQLite;

namespace MeerkatDelivery.Database
{
	public class ExampleBusinessObject : BaseDatabaseObject
	{
		[PrimaryKey, AutoIncrement]
		public int? Id { get; set; }
		public string DeviceName { get; set; }
		public string ExampleEnum { get; set; }
	}
}

