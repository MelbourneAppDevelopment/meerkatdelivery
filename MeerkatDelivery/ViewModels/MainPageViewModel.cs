﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using System.Diagnostics;
using Prism.Navigation;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;

namespace MeerkatDelivery.ViewModels
{
	public class MainPageViewModel : BindableBase, INotifyPropertyChanged
	{
		#region private Proterties
		TimerState batchTimer;

		#endregion
		#region UIProperties
		string _deviceName;
		public string DeviceName
		{
			get
			{
				return _deviceName;
			}
			set
			{
				SetProperty(ref _deviceName, value);
			}
		}

		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}
		#endregion
		#region ICommands
		//private DelegateCommand<AuthorisedPump> _onItemSelectedCommand;
		//public DelegateCommand<AuthorisedPump> OnItemSelectedCommand => _onItemSelectedCommand != null ? _onItemSelectedCommand : (_onItemSelectedCommand = new DelegateCommand<AuthorisedPump>(DoOnItemSelectedCommand));
		//private async void DoOnItemSelectedCommand(AuthorisedPump authorisedPump)
		//{
		//}


		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

			var message = String.Format("{0}", AppResources.ExampleHelpText);
			string[] values = { AppResources.Help, message, AppResources.GotIt };
			MessagingCenter.Send<MainPageViewModel,string[]>(this, "MainPageDisplayAlert", values);
		}

		#endregion
		#region public methods

		#endregion
		#region private Methods
		#endregion
		#region Batch Processing
		private void StartTimer()
		{
			batchTimer = new TimerState();
			TimerCallback timerDelegate = new TimerCallback(ThreadedBatchProcess);
			Timer timer = new Timer(timerDelegate, batchTimer, 1000, 1000);
			batchTimer.tmr = timer;  // So we have a handle to dispose
		}

		private void StopTimer()
		{
			if (batchTimer != null && batchTimer.tmr != null)
			{
				batchTimer.tmr.Cancel();
				batchTimer.tmr.Dispose();
				batchTimer = null;
			}
		}

		private void ThreadedBatchProcess(Object state)
		{
			Debug.WriteLine("Timer ran");
				// Do stuff
		}
		#endregion
		#region INavigateAware
		/// <summary>
		/// Triggered from code behind when page appears
		/// </summary>
		public void OnAppearing()
		{
		}

		public void OnDisappearing()
		{
		}
		//THESE DON'T WORK when triggered by back button on NavBar or Android physical button
		//public void OnNavigatedFrom(NavigationParameters parameters)
		//{
		//	// left this program
		//}

		//public void OnNavigatedTo(NavigationParameters parameters)
		//{
			
		//}

		//public void OnNavigatingTo(NavigationParameters parameters)
		//{
			
		//}
		#endregion

		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IExampleServices _exampleServices;

		public MainPageViewModel(
			IExampleServices exampleServices,
			INavigationService navigationService
		)
		{
			_exampleServices = exampleServices;
			_navigationService = navigationService;

			//Example code - delete
			var exampleObject = new ExampleBusinessObject();
			this.DeviceName = exampleObject.DeviceName;

		}
		#endregion
	}
}
