﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using System.Diagnostics;
using Prism.Navigation;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MeerkatDelivery.CustomControls;
using Xamarin.Forms.Maps;
using MeerkatDelivery.Helpers;

namespace MeerkatDelivery.ViewModels
{
	public class DeliveriesPageViewModel : BindableBase, INotifyPropertyChanged, INavigatedAware
	{
		#region private Proterties
		TimerState batchTimer;

		#endregion
		#region UIProperties
		bool _isRefreshing;
		public bool IsRefreshing
		{
			get
			{
				return _isRefreshing;
			}
			set
			{
				SetProperty(ref _isRefreshing, value);
			}
		}

		string _deviceName;
		public string DeviceName
		{
			get
			{
				return _deviceName;
			}
			set
			{
				SetProperty(ref _deviceName, value);
			}
		}

		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

		private CustomMap _map;
		public CustomMap Map
		{
			get
			{
				return _map;
			}
			set { SetProperty(ref _map, value); }
		}

		private Pin _selectedPin;
		public Pin SelectedPin
		{
			get
			{
				return _selectedPin;
			}
			set { 
				SetProperty(ref _selectedPin, value); 
			}
		}

		ObservableCollection<DeliveryItemViewModel> _orders = new ObservableCollection<DeliveryItemViewModel>();
		public ObservableCollection<DeliveryItemViewModel> Orders
		{
			get
			{
				return _orders;
			}
			set { SetProperty(ref _orders, value); }
		}

		ObservableCollection<CustomPin> _customPins = new ObservableCollection<CustomPin>();
		public ObservableCollection<CustomPin> CustomPins
		{
			get
			{
				return _customPins;
			}
			set { SetProperty(ref _customPins, value); }
		}

		ObservableCollection<Pin> _pins = new ObservableCollection<Pin>();
		public ObservableCollection<Pin> Pins
		{
			get
			{
				return _pins;
			}
			set { SetProperty(ref _pins, value); }
		}

		#endregion
		#region ICommands
		private DelegateCommand<DeliveryItemViewModel> _onItemTappedCommand;
		public DelegateCommand<DeliveryItemViewModel> OnItemTappedCommand => _onItemTappedCommand != null ? _onItemTappedCommand : (_onItemTappedCommand = new DelegateCommand<DeliveryItemViewModel>(DoOnItemTappedCommand));
		private async void DoOnItemTappedCommand(DeliveryItemViewModel item)
		{
			var param = new NavigationParameters();
			param.Add("OrderId", item.Order.Id);
			param.Add("UpdateMode", "Delivery");

			await _navigationService.NavigateAsync("OrderPage", param, false, true);
		}

		private DelegateCommand _createRouteCommand;
		public DelegateCommand CreateRouteCommand => _createRouteCommand != null ? _createRouteCommand : (_createRouteCommand = new DelegateCommand(DoCreateRouteCommand));
		private void DoCreateRouteCommand()
		{

		}

		private DelegateCommand _refreshListCommand;
		public DelegateCommand RefreshListCommand => _refreshListCommand != null ? _refreshListCommand : (_refreshListCommand = new DelegateCommand(DoRefreshListCommand));
		private async void DoRefreshListCommand()
		{
			IsRefreshing = true;
			//await Order.SyncWithServerAsync();
			IsRefreshing = false;
		}

		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

			var message = String.Format("{0}", AppResources.ExampleHelpText);
			string[] values = { AppResources.Help, message, AppResources.GotIt };
			MessagingCenter.Send<DeliveriesPageViewModel, string[]>(this, "DeliveriesPageDisplayAlert", values);
		}

		#endregion
		#region public methods

		public void SetFocus(int itemIndex)
		{
			string pinUrl;
			switch (CustomPins[itemIndex].PinUrl)
			{
				case "RedPin.png":
					pinUrl = "RedPinMeerkat.png";
					break;
				case "GrayPin.png":
					pinUrl = "GrayPinMeerkat.png";
					break;
					
				default:
					pinUrl = "RedPin.png";
					break;
			}

			UpdatePinAt(itemIndex,pinUrl);

		}
		/// <summary>
		/// Triggered from code behind when page appears
		/// </summary>
		public void OnAppearing()
		{
			LoadList(); // Don't need to await this
			//Task.Run(async () =>
			//{
			//	await LoadList();
			//});
			//StartTimer();
		}

		public void OnDisappearing()
		{
			StopTimer();
		}
		#endregion
		#region private Methods
		private async Task LoadList()
		{
			Debug.WriteLine("Hello from LoadList");
			Orders.Clear();
			CustomPins.Clear();
			Pins.Clear();

			//TODO should be GetAll(clientId);
			var orderObject = await Order.GetAllAsync();
			foreach (var obj in orderObject)
			{
				var item = new DeliveryItemViewModel();
				item.Order = obj;
				if (!item.Order.Deleted)
				{
					Orders.Add(item);

					// Only add Pins if still to be delivered
					if (item.Order.Status != OrderStatus.Cancelled && item.Order.Status != OrderStatus.Delivered && item.Order.Status != OrderStatus.FailedDelivery)
					{
						CustomPins.Add(item.Order.Pin);
						Pins.Add(item.Order.Pin.Pin);
					}
				}
			}
			Map.CustomPins = CustomPins;
			if (CustomPins.Count > 0)
				PositionMap();
		}

		/// <summary>
		/// Updates the pin at index with new pinUrl and position.
		/// </summary>
		/// <param name="index">Pin array index.</param>
		/// <param name="pinUrl">Pin URL.</param>
		/// <param name="position">Gps Position.</param>
		/// <param name="ResetOthersPinsToDefault">If set to <c>true</c> reset others pins to default url.</param>
		private void UpdatePinAt(int index, string pinUrl, Position? position = null, bool ResetOthersPinsToDefault = true)
		{
			Pin newPin;

			if (position == null)
			{
				newPin = Pins[index];
			}
			else
			{
				newPin = new Xamarin.Forms.Maps.Pin
				{
					Type = Pins[index].Type,
					Position = (Position)position,
					Label = Pins[index].Label,
					Address = Pins[index].Address
				};

				//Debug.WriteLine("Removing {0} ({1},{2}) replacing with ({3},{4})", index, Pins[index].Position.Latitude, Pins[index].Position.Longitude, newPin.Position.Latitude, newPin.Position.Longitude);
			}

			if (!ResetOthersPinsToDefault)
			{
				CustomPins[index].Pin = newPin;
				CustomPins[index].PinUrl = pinUrl;
				Pins.RemoveAt(index);
				Pins.Insert(index, newPin);
			}
			else
			{
				for (var i = 0; i < Pins.Count; i++)
				{
					if (i == index)
					{
						CustomPins[index].Pin = newPin;
						CustomPins[index].PinUrl = pinUrl;
						Pins.RemoveAt(index);
						Pins.Insert(index, newPin);
					}
					else
					{
						CustomPins[i].PinUrl = Settings.DefaultPin;
						var pin = Pins[i];
						Pins.RemoveAt(i);
						Pins.Insert(i, pin);
					}
				}
			}
		}

		private void PositionMap()
		{
			try
			{
				// Calculate map center for full screen
				var center = MapHelper.CenterPosition(Pins);
				var radius = MapHelper.RadiusOf(center, Pins);

				// Calculate map center for reduced screen size
				var distNorth = radius * 2.0 * 0.95;    // drop down a bit so pins still show
				radius = radius * 2.0;
				center = MapHelper.CalculateDerivedPosition(center, distNorth, 180.0);

				SetMapCenter(center, radius);

			}
			catch (Exception e)
			{
				Debug.WriteLine("EXCEPTION ViewModel: {0}", e);
			}
		}

		private void SetMapCenter(Position center, double distance)
		{
			var mapdist = Xamarin.Forms.Maps.Distance.FromMeters(distance);
			MapSpan region = MapSpan.FromCenterAndRadius(center, mapdist);
			Map.MoveToRegion(region);
		}




		#endregion
		#region Batch Processing
		private void StartTimer()
		{
			batchTimer = new TimerState();
			TimerCallback timerDelegate = new TimerCallback(ThreadedBatchProcess);
			Timer timer = new Timer(timerDelegate, batchTimer, 2000, 2000);
			batchTimer.tmr = timer;  // So we have a handle to dispose
		}

		private void StopTimer()
		{
			if (batchTimer != null && batchTimer.tmr != null)
			{
				batchTimer.tmr.Cancel();
				batchTimer.tmr.Dispose();
				batchTimer = null;
			}
		}

		int mapReposCount = 0;
		private void ThreadedBatchProcess(Object state)
		{
			//Debug.WriteLine("Timer ran");

			// Randomly change the pins
			var random = new Random();
			var index = random.Next(0, Pins.Count);
			double moveLat = (double)(random.Next(0, 10) - 5) / 1000.0;
			double moveLng = (double)(random.Next(0, 10) - 5) / 1000.0;

			Device.BeginInvokeOnMainThread(() =>
		   {
			   try
			   {
				   UpdatePinAt(index, "RedPinMeerkat", new Position(Pins[index].Position.Latitude + moveLat, Pins[index].Position.Longitude + moveLng), true);
	   				mapReposCount++;
				   if (mapReposCount > 5)
				   {
					   mapReposCount = 0;
					   PositionMap();
				   }

			   }
			   catch (Exception e)
			   {
				   Debug.WriteLine("EXCEPTION: ViewModel (01) {0}", e);
			   }
		   });
		
		}
		#endregion
		#region INavigateAware
		//THIS DON'T WORK when triggered by back button on NavBar or Android physical button
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
			// left this program
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{
		}
		#endregion

		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IExampleServices _exampleServices;

		public DeliveriesPageViewModel(
			INavigationService navigationService
		)
		{
			_navigationService = navigationService;

			// Update the Order List whenever database changes
			MessagingCenter.Subscribe<Order>(this, "OrdersUpdated", (sender) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
				  {
					  var orderObjects = await Order.GetAllAsync();
					  foreach (var obj in orderObjects)
					  {
						  //TODO a bit rough
						  var found = false;
						  DeliveryItemViewModel existingItem = null;
						  foreach (var oi in Orders)
							  if (oi.Order.Id == obj.Id)
							  {    // already in Orders
								  existingItem = oi;
								  found = true;
								  break;
							  }

						  if (found)
						  {
							  existingItem.Order.LoadFromDB(obj.DbOrder); // reload with new values
						  }
						  else
						  {
							  var item = new DeliveryItemViewModel();
							  item.Order = obj;
							  if (!item.Order.Deleted)
								  Orders.Add(item);
						  }

					  }
				  });

			});
		}
		#endregion
	}
}
