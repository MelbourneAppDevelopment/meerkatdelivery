﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using System.Diagnostics;
using Prism.Navigation;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MeerkatDelivery.ViewModels
{
	public class TestPageViewModel : BindableBase, INotifyPropertyChanged, INavigatedAware
	{
		#region private Proterties
		#endregion
		#region public Properties
		private Order _order = null;
		public Order Order
		{
			get
			{
				return _order;
			}
			set { 
				SetProperty(ref _order, value);
				this.LoadData(value);
			}
		}
		#endregion
		#region UIProperties
		// Manual Setup Properties
		private string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

		private bool _isEditMode = false;
		public bool IsEditMode
		{
			get
			{
				return _isEditMode;
			}
			set { SetProperty(ref _isEditMode, value); }
		}

		private bool _isInsertMode = false;
		public bool IsInsertMode
		{
			get
			{
				return _isInsertMode;
			}
			set { SetProperty(ref _isInsertMode, value); }
		}

		private UpdateMode _updateMode;
		public UpdateMode UpdateMode
		{
			get
			{
				return _updateMode;
			}
			set
			{
				SetProperty(ref _updateMode, value);
				switch (value)
				{
					case UpdateMode.Insert:
						IsInsertMode = true;
						IsEditMode = true;
						break;
					case UpdateMode.Edit:
						IsInsertMode = false;
						IsEditMode = true;
						break;
					case UpdateMode.Display:
						IsInsertMode = false;
						IsEditMode = false;
						break;
				}
			}
		}

		// Generated Properties

		private double _amount;
		public double Amount
		{
			get
			{
				return _amount;
			}
			set { SetProperty(ref _amount, value); }
		}
		private string _cardText;
		public string CardText
		{
			get
			{
				return _cardText;
			}
			set { SetProperty(ref _cardText, value); }
		}
		private int _clearingNumber;
		public int ClearingNumber
		{
			get
			{
				return _clearingNumber;
			}
			set { SetProperty(ref _clearingNumber, value); }
		}

		private string _contactPhone;
		public string ContactPhone
		{
			get
			{
				return _contactPhone;
			}
			set { SetProperty(ref _contactPhone, value); }
		}

		private DateTime _deliveryDate = DateTime.Today;
		public DateTime DeliveryDate
		{
			get
			{
				return _deliveryDate;
			}
			set { SetProperty(ref _deliveryDate, value); }
		}

		private DateTime _deliveryTime;
		public DateTime DeliveryTime
		{
			get
			{
				return _deliveryTime;
			}
			set { SetProperty(ref _deliveryTime, value); }
		}
		private DeliveryTime _deliveryTimeCode;
		public DeliveryTime DeliveryTimeCode
		{
			get
			{
				return _deliveryTimeCode;
			}
			set { SetProperty(ref _deliveryTimeCode, value); }
		}

		private int _id;
		public int Id
		{
			get
			{
				return _id;
			}
			set { SetProperty(ref _id, value); }
		}

		private string _notes;
		public string Notes
		{
			get
			{
				return _notes;
			}
			set { SetProperty(ref _notes, value); }
		}
		private string _orderNumber;
		public string OrderNumber
		{
			get
			{
				return _orderNumber;
			}
			set { SetProperty(ref _orderNumber, value); }
		}

		private string _orderSystemClientId;
		public string OrderSystemClientId
		{
			get
			{
				return _orderSystemClientId;
			}
			set { SetProperty(ref _orderSystemClientId, value); }
		}

		private string _priority;
		public string Priority
		{
			get
			{
				return _priority;
			}
			set { SetProperty(ref _priority, value); }
		}
		private string _product1Code;
		public string Product1Code
		{
			get
			{
				return _product1Code;
			}
			set { SetProperty(ref _product1Code, value); }
		}
		private string _product1Description;
		public string Product1Description
		{
			get
			{
				return _product1Description;
			}
			set { SetProperty(ref _product1Description, value); }
		}

		private string _recipientCompanyName;
		public string RecipientCompanyName
		{
			get
			{
				return _recipientCompanyName;
			}
			set { SetProperty(ref _recipientCompanyName, value); }
		}

		private string _recipientName;
		public string RecipientName
		{
			get
			{
				return _recipientName;
			}
			set { SetProperty(ref _recipientName, value); }
		}

		private string _recipientState;
		public string RecipientState
		{
			get
			{
				return _recipientState;
			}
			set { SetProperty(ref _recipientState, value); }
		}
		private string _recipientStreetAddress;
		public string RecipientStreetAddress
		{
			get
			{
				return _recipientStreetAddress;
			}
			set { SetProperty(ref _recipientStreetAddress, value); }
		}
		private string _recipientSuburb;
		public string RecipientSuburb
		{
			get
			{
				return _recipientSuburb;
			}
			set { SetProperty(ref _recipientSuburb, value); }
		}
		private string _recipientZip;
		public string RecipientZip
		{
			get
			{
				return _recipientZip;
			}
			set { SetProperty(ref _recipientZip, value); }
		}

		private string _senderEmail;
		public string SenderEmail
		{
			get
			{
				return _senderEmail;
			}
			set { SetProperty(ref _senderEmail, value); }
		}
		private string _senderName;
		public string SenderName
		{
			get
			{
				return _senderName;
			}
			set { SetProperty(ref _senderName, value); }
		}

		private string _specialInstructions;
		public string SpecialInstructions
		{
			get
			{
				return _specialInstructions;
			}
			set { SetProperty(ref _specialInstructions, value); }
		}
		private OrderStatus _status;
		public OrderStatus Status
		{
			get
			{
				return _status;
			}
			set { SetProperty(ref _status, value); }
		}
		#endregion
		#region ICommands
		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

			var message = String.Format("{0}", AppResources.ExampleHelpText);
			string[] values = { AppResources.Help, message, AppResources.GotIt };
			MessagingCenter.Send<TestPageViewModel, string[]>(this, "OrderPageDisplayAlert", values);
		}

		#endregion
		#region public methods
		public void OnAppear()
		{

		}
		public void OnDisappear()
		{
			this.LoadObject(Order);
			Order.SaveAsync();
		}
		#endregion
		#region private Methods
		private void LoadData(Order obj)
		{
		   this.Amount  = obj.Amount;
		   this.CardText  = obj.CardText;
		   this.ClearingNumber  = obj.ClearingNumber;
		   this.ContactPhone  = obj.ContactPhone;
		   this.DeliveryDate  = obj.DeliveryDate;
		   this.DeliveryTime  = obj.DeliveryTime;
		   this.DeliveryTimeCode  = obj.DeliveryTimeCode;
		   this.Id  = (int)obj.Id;
		   this.Notes  = obj.Notes;
		   this.OrderNumber  = obj.OrderNumber;
		   this.OrderSystemClientId  = obj.OrderSystemClientId;
		   this.Priority  = obj.Priority;
		   this.Product1Code  = obj.Product1Code;
		   this.Product1Description  = obj.Product1Description;
		   this.RecipientCompanyName  = obj.RecipientCompanyName;
		   this.RecipientName  = obj.RecipientName;
		   this.RecipientState  = obj.RecipientState;
		   this.RecipientStreetAddress  = obj.RecipientStreetAddress;
		   this.RecipientSuburb  = obj.RecipientSuburb;
		   this.RecipientZip  = obj.RecipientZip;
		   this.SenderEmail  = obj.SenderEmail;
		   this.SenderName  = obj.SenderName;
		   this.SpecialInstructions  = obj.SpecialInstructions;
		   this.Status  = obj.Status;		
		}
		private void LoadObject(Order obj)
		{
			obj.Amount = this.Amount;
			obj.CardText = this.CardText;
			obj.ClearingNumber = this.ClearingNumber;
			obj.ContactPhone = this.ContactPhone;
			obj.DeliveryDate = this.DeliveryDate;
			obj.DeliveryTime = this.DeliveryTime;
			obj.DeliveryTimeCode = this.DeliveryTimeCode;
			obj.Id = (int)this.Id;
			obj.Notes = this.Notes;
			obj.OrderNumber = this.OrderNumber;
			obj.OrderSystemClientId = this.OrderSystemClientId;
			obj.Priority = this.Priority;
			obj.Product1Code = this.Product1Code;
			obj.Product1Description = this.Product1Description;
			obj.RecipientCompanyName = this.RecipientCompanyName;
			obj.RecipientName = this.RecipientName;
			obj.RecipientState = this.RecipientState;
			obj.RecipientStreetAddress = this.RecipientStreetAddress;
			obj.RecipientSuburb = this.RecipientSuburb;
			obj.RecipientZip = this.RecipientZip;
			obj.SenderEmail = this.SenderEmail;
			obj.SenderName = this.SenderName;
			obj.SpecialInstructions = this.SpecialInstructions;
			obj.Status = this.Status;
		}
		#endregion

		#region INavigateAware
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
			// left this program
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			if (parameters.ContainsKey("OrderId"))
			{
				var orderId = (int)parameters["OrderId"];
				if (orderId == 0)
				{
					Order = new Order();
				}
				else
				{
					Order = new Order(orderId);
				}
			}
			else
			{
				Order = new Order();
			}

			if (parameters.ContainsKey("UpdateMode"))
			{
				switch ((string)parameters["UpdateMode"])
				{
					case "Insert":
						UpdateMode = UpdateMode.Insert;
						break;
					case "Edit":
						UpdateMode = UpdateMode.Edit;
						break;
					case "Display":
						UpdateMode = UpdateMode.Display;
						break;
					default:
						UpdateMode = UpdateMode.Display;
						break;
				}
			}
			else
				UpdateMode = UpdateMode.Display;

		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{

		}
		#endregion

		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IExampleServices _exampleServices;

		public TestPageViewModel(
		)
		{
		}
		#endregion
	}
}
