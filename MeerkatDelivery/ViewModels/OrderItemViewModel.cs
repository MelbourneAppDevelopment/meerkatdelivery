﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using System.Diagnostics;
using Prism.Navigation;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MeerkatDelivery.ViewModels
{
	public class OrderItemViewModel : MeerkatListItemViewModel
	{
	}

	public class MeerkatListItemViewModel : BindableBase, INotifyPropertyChanged
	{
		#region private Proterties
		private List<User> PickerList = new List<User>();
		private List<User> CourierList = new List<User>();
		#endregion
		#region UIProperties

		// --- MANUAL ---

		private string _courierFirstName;
		public string CourierFirstName
		{
			get
			{
				return _courierFirstName;
			}
			set
			{
				SetProperty(ref _courierFirstName, value);
				if (value == "")
					this.HasCourier = false;
				else
					this.HasCourier = true;
			}
		}

		private List<string> _courierStringsList = new List<string>() { "courier" };
		public List<string> CourierStringsList
		{
			get
			{
				return _courierStringsList;
			}
			set
			{
				SetProperty(ref _courierStringsList, value);
			}
		}

		private int _courierListSelected = -1;
		public int CourierListSelected
		{
			get
			{
				return _courierListSelected;
			}
			set
			{
				if (value > -1 && value != _courierListSelected)
				{
					if (value == 0)
						Order.CourierUserId = 0;
					else
					{
						Order.CourierUserId = CourierList[value - 1].Id;
					}
					Order.Save();
				}

				SetProperty(ref _courierListSelected, value);

				if (value == 0)
					this.CourierListTextColor = (Color)Application.Current.Resources["PlaceholderColor"];
				else
					//this.CourierListTextColor = (Color)Application.Current.Resources["BrandColor"];
                    this.CourierListTextColor = Color.Black;
			}
		}


		private Color _courierListTextColor = Color.Gray;
		public Color CourierListTextColor
		{
			get
			{
				return _courierListTextColor;
			}
			set
			{
				SetProperty(ref _courierListTextColor, value);
			}
		}

		private string _deliveryDateShort;
		public string DeliveryDateShort
		{
			get
			{
				return _deliveryDateShort;
			}
			set { SetProperty(ref _deliveryDateShort, value); }
		}

		private Order _order;
		public Order Order
		{
			get { return _order; }
			set
			{
				if (value != null)
				{
					_order = value;
					this.LoadFields(_order);
				}
			}
		}

		private string _priorityString;
		public string PriorityString
		{
			get
			{
				return _priorityString;
			}
			set
			{
				SetProperty(ref _priorityString, value);
			}
		}

		private bool _hasPicker;
		public bool HasPicker
		{
			get
			{
				return _hasPicker;
			}
			set
			{
				SetProperty(ref _hasPicker, value);
				this.HasNoPicker = !value;
			}
		}

		// Dodgy - can't work out how to ahve converters in a non-ContentLayer page
		private bool _hasNoPicker;
		public bool HasNoPicker
		{
			get
			{
				return _hasNoPicker;
			}
			set
			{
				SetProperty(ref _hasNoPicker, value);
			}
		}

		private bool _hasNoCourier;
		public bool HasNoCourier
		{
			get
			{
				return _hasNoCourier;
			}
			set
			{
				SetProperty(ref _hasNoCourier, value);
			}
		}

		private bool _hasCourier;
		public bool HasCourier
		{
			get
			{
				return _hasCourier;
			}
			set
			{
				SetProperty(ref _hasCourier, value);
                this.HasNoCourier = !value;
			}
		}

		private string _pickerFirstName;
		public string PickerFirstName
		{
			get
			{
				return _pickerFirstName;
			}
			set
			{
				SetProperty(ref _pickerFirstName, value);
				if (value == "")
                    this.HasPicker = false;
				else
					this.HasPicker = true;
			}
		}

		private List<string> _pickerStringsList = new List<string>() { "prepare" };
		public List<string> PickerStringsList
		{
			get
			{
				return _pickerStringsList;
			}
			set
			{
				SetProperty(ref _pickerStringsList, value);
			}
		}

		private string _pickerStringSelected;
		public string PickerStringSelected
		{
			get
			{
				return _pickerStringSelected;
			}
			set
			{
             			SetProperty(ref _pickerStringSelected, value);
			}
		}

		private int _pickerListSelected = -1;
		public int PickerListSelected
		{
			get
			{
				if (_order == null)
					Debug.WriteLine("Returning selected {0} for order 'null'", _pickerListSelected);
				else				
					Debug.WriteLine("Returning selected {0} for order {1}", _pickerListSelected, _order.Id);

				return _pickerListSelected;
			}
			set
			{
				if (value > -1 && value != _pickerListSelected)
				{
					if (value == 0)
						Order.PickingUserId = 0;
					else
					{
						Order.PickingUserId = PickerList[value - 1].Id;
					}
					Order.Save();
				}

				SetProperty(ref _pickerListSelected, value);
				if (_order == null)
					Debug.WriteLine("Setting selected {0} for order 'null'", _pickerListSelected);
				else				
					Debug.WriteLine("Setting selected {0} for order {1}", _pickerListSelected, _order.Id);
				
				if (value == 0)
                    this.PickerListTextColor = (Color)Application.Current.Resources["PlaceholderColor"];
				else
					//this.PickerListTextColor = (Color)Application.Current.Resources["BrandColor"];
                    this.PickerListTextColor = Color.Black;
			}
		}

		private Color _pickerListTextColor = Color.Gray;
		public Color PickerListTextColor
		{
			get
			{
				return _pickerListTextColor;
			}
			set
			{
				SetProperty(ref _pickerListTextColor, value);
			}
		}

		private string _pinUrl;
		public string PinUrl
		{
			get
			{
				return _pinUrl;
			}
			set
			{
				SetProperty(ref _pinUrl, value);
			}
		}

		private string _statusBackgroundColor;
		public string StatusBackgroundColor
		{
			get
			{
				return _statusBackgroundColor;
			}
			set
			{
				SetProperty(ref _statusBackgroundColor, value);
			}
		}

		private string _statusTextColor;
		public string StatusTextColor
		{
			get
			{
				return _statusTextColor;
			}
			set
			{
				SetProperty(ref _statusTextColor, value);
			}
		}

		private string _statusPin;
		public string StatusPin
		{
			get
			{
				return _statusPin;
			}
			set
			{
				SetProperty(ref _statusPin, value);
			}	}




		// --- GENERATED ---

		private DateTime _deliveryDate;
		public DateTime DeliveryDate
		{
			get
			{
				return _deliveryDate;
			}
			set
			{
				SetProperty(ref _deliveryDate, value);
				if (value == DateTime.Today)
					DeliveryDateShort = "Today";
				else if (value == DateTime.Today.AddDays(1))
					DeliveryDateShort = "Tomorrow";
				else if (value == DateTime.Today.AddDays(-1))
					DeliveryDateShort = "Yesterday";
				else 
					DeliveryDateShort = value.ToString("d/M");
			}
		}

		private string _recipientStreetAddress;
		public string RecipientStreetAddress
		{
			get
			{
				return _recipientStreetAddress;
			}
			set { SetProperty(ref _recipientStreetAddress, value); }
		}
		private string _recipientSuburb;
		public string RecipientSuburb
		{
			get
			{
				return _recipientSuburb;
			}
			set { SetProperty(ref _recipientSuburb, value); }
		}

		private DeliveryTime _deliveryTimeCode;
		public DeliveryTime DeliveryTimeCode
		{
			get
			{
				return _deliveryTimeCode;
			}
			set { 
				SetProperty(ref _deliveryTimeCode, value);
				DeliveryTimeString = value.ToString();
			}
		}

		private string _deliveryTimeString;
		public string DeliveryTimeString
		{
			get
			{
				return _deliveryTimeString;
			}
			set { SetProperty(ref _deliveryTimeString, value); }
		}

		private string _orderNumber;
		public string OrderNumber
		{
			get
			{
				return _orderNumber;
			}
			set { SetProperty(ref _orderNumber, value); }
		}

		private string _priority;
		public string Priority
		{
			get
			{
				return _priority;
			}
			set { 
                SetProperty(ref _priority, value);
				if (String.IsNullOrEmpty(value))
					PriorityString = "";
				else
					PriorityString = String.Format("({0})", value);
			}
		}

		private string _product1Code;
		public string Product1Code
		{
			get
			{
				return _product1Code;
			}
			set { SetProperty(ref _product1Code, value); }
		}

		private string _product1Description;
		public string Product1Description
		{
			get
			{
				return _product1Description;
			}
			set { SetProperty(ref _product1Description, value); }
		}

		private string _recipientName;
		public string RecipientName
		{
			get
			{
				return _recipientName;
			}
			set { SetProperty(ref _recipientName, value); }
		}

		private string _specialInstructions;
		public string SpecialInstructions
		{
			get
			{
				return _specialInstructions;
			}
			set { SetProperty(ref _specialInstructions, value); }
		}

		private OrderStatus _status;
		public OrderStatus Status
		{
			get
			{
				return _status;
			}
			set { 
				SetProperty(ref _status, value);
				StatusSummary = "force change";
			}
		}

		private string _statusSummary;
		public string StatusSummary
		{
			get
			{
				StatusSummaryTextColor = "";
				switch (Status)
				{
					case OrderStatus.Initialised:
					case OrderStatus.Ordered:
						_statusSummary = "";
						break;

					case OrderStatus.Cancelled:
						_statusSummary = "CANCELLED";
						StatusSummaryTextColor = "Red";
						break;

					case OrderStatus.Picked:
						_statusSummary = "ready to go";
						break;

					case OrderStatus.Loaded:
						_statusSummary = "loaded in van";
						break;

					case OrderStatus.Delivering:
						_statusSummary = "started turn by turn";
						break;

					case OrderStatus.Delivered:
						_statusSummary = "delivered";
						break;

					case OrderStatus.FailedDelivery:
						_statusSummary = "DELIVERY FAILED";
						StatusSummaryTextColor = "Red";
						break;
				}

				if (_statusSummary == "")
					return "";
				else 
					return String.Format("({0})",_statusSummary);
			}
			set { SetProperty(ref _statusSummary, value); }
		}

		private string _statusSummaryTextColor = "";
		public string StatusSummaryTextColor
		{
			get {
				if (_statusSummaryTextColor == "")
					return StatusTextColor;
				else
					return _statusSummaryTextColor;
			}
			set { _statusSummaryTextColor = value; }
		}


		#endregion
		#region ICommands
		//private DelegateCommand<AuthorisedPump> _onItemSelectedCommand;
		//public DelegateCommand<AuthorisedPump> OnItemSelectedCommand => _onItemSelectedCommand != null ? _onItemSelectedCommand : (_onItemSelectedCommand = new DelegateCommand<AuthorisedPump>(DoOnItemSelectedCommand));
		//private async void DoOnItemSelectedCommand(AuthorisedPump authorisedPump)
		//{
		//}

		private DelegateCommand<MeerkatListItemViewModel> _editCommand;
		public DelegateCommand<MeerkatListItemViewModel> EditCommand => _editCommand != null ? _editCommand : (_editCommand = new DelegateCommand<MeerkatListItemViewModel>(DoEditCommand));
		private async void DoEditCommand(MeerkatListItemViewModel item)
		{
			//var param = new NavigationParameters();
			//param.Add("OrderId", item.Order.Id);
			//param.Add("UpdateMode", "Edit");

			//await _navigationService.NavigateAsync("OrderPage", param, false, true);
		}

		private DelegateCommand<MeerkatListItemViewModel> _deleteCommand;
		public DelegateCommand<MeerkatListItemViewModel> DeleteCommand => _deleteCommand != null ? _deleteCommand : (_deleteCommand = new DelegateCommand<MeerkatListItemViewModel>(DoDeleteCommand));
		private async void DoDeleteCommand(MeerkatListItemViewModel item)
		{
			var message = String.Format("{0} {1}?", AppResources.DeleteOrderText, item.Order.Id);
			string[] values = { AppResources.Help, message, AppResources.Ok, AppResources.Cancel };
			var args = new Tuple<string[], MeerkatListItemViewModel>(values, item);

			// WARNING: This is listened to by the ListOrderPage
			MessagingCenter.Send<MeerkatListItemViewModel, Tuple<string[], MeerkatListItemViewModel>>(this, "OrderItemDeleteOrder", args);
		}

		private DelegateCommand _setFocusCommand;
		public DelegateCommand SetFocusCommand => _setFocusCommand != null ? _setFocusCommand : (_setFocusCommand = new DelegateCommand(DoSetFocusCommand));
		private void DoSetFocusCommand()
		{
		}

		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

			var message = String.Format("{0}", AppResources.ExampleHelpText);
			string[] values = { AppResources.Help, message, AppResources.GotIt };
			MessagingCenter.Send<MeerkatListItemViewModel,string[]>(this, "OrderItemDisplayAlert", values);
		}

		#endregion
		#region public methods

		#endregion
		#region private Methods
		private void LoadFields(Order order)
		{
			Debug.WriteLine("Loading Fields for order id {0}", order.Id);

			this.DeliveryDate = order.DeliveryDate;
			this.RecipientStreetAddress = order.RecipientStreetAddress;
			this.RecipientSuburb = order.RecipientSuburb;
			this.DeliveryTimeCode = order.DeliveryTimeCode;
			this.OrderNumber = order.OrderNumber;
			this.Priority = order.Priority;
			this.Product1Code = order.Product1Code;
			this.Product1Description = order.Product1Description;
			this.RecipientName = order.RecipientName;
			this.SpecialInstructions = order.SpecialInstructions;
			this.Status = order.Status;

			if (order.PickingUserId == 0)
			{
				this.PickerFirstName = "";
			}
			else
			{
				var pickerUser = new User(order.PickingUserId);
				this.PickerFirstName = pickerUser.FirstName;
			}

			if (order.CourierUserId == 0)
			{
				this.CourierFirstName = "";
			}
			else
			{
				var courierUser = new User(order.CourierUserId);
				this.CourierFirstName = courierUser.FirstName;
			}


			if (this.Status != null)
			{
				this.StatusBackgroundColor = Settings.OrderStatusBackgroundColor[this.Status.ToString()];
				this.StatusTextColor = Settings.OrderStatusTextColor[this.Status.ToString()];
				this.StatusPin = Settings.OrderStatusPin[this.Status.ToString()];
			}
			else
			{
				this.StatusBackgroundColor = "White";
				this.StatusTextColor = "Black";
				this.StatusPin = "RedPin.png";
			}

			// Build a list of Pickers and Couriers, refreshing the UI picker

			var pickers = User.GetPickers(order.ClientId);
			PickerList.Clear();
			PickerStringsList.Clear();
			PickerStringsList.Add("prepare");
			var selected = 0;
			var count = 0;
			foreach (var picker in pickers)
			{
				count++;
				PickerList.Add(picker);
				PickerStringsList.Add(picker.FirstName);
				if (order.PickingUserId == picker.Id)
					selected = count;
			}

			Debug.WriteLine("About to set selected {0} for order {1}", selected, order.Id);

			PickerListSelected = selected;

			PinUrl = Order.Pin.PinUrl;

			//PickerStringSelected = PickerStringsList[PickerListSelected];

			//var couriers = User.GetCouriers(order.ClientId);
			//CourierList.Clear();
			//CourierStringsList.Clear();
			//CourierStringsList.Add("none");
			//selected = 0;
			//count = 0;
			//foreach (var courier in couriers)
			//{
			//	count++;
			//	CourierList.Add(courier);
			//	CourierStringsList.Add(courier.FirstName);
			//	if (order.CourierUserId == courier.Id)
			//		selected = count;
			//}
			//CourierListSelected = selected;
		}
		#endregion
		#region INavigateAware
		/// <summary>
		/// Triggered from code behind when page appears
		/// </summary>
		public void OnAppearing()
		{
		}

		public void OnDisappearing()
		{
		}
		//THESE DON'T WORK when triggered by back button on NavBar or Android physical button
		//public void OnNavigatedFrom(NavigationParameters parameters)
		//{
		//	// left this program
		//}

		//public void OnNavigatedTo(NavigationParameters parameters)
		//{
			
		//}

		//public void OnNavigatingTo(NavigationParameters parameters)
		//{
			
		//}
		#endregion

		#region Constructors
		private readonly INavigationService _navigationService;
		public MeerkatListItemViewModel(
			//INavigationService navigationService
		)
		{
			//_navigationService = navigationService;

		}
		#endregion
	}
}
