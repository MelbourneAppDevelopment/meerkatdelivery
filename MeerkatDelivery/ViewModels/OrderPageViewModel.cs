﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using System.Diagnostics;
using Prism.Navigation;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MeerkatDelivery.Helpers;

namespace MeerkatDelivery.ViewModels
{
	public enum UpdateMode
	{
		Insert,
		Edit,
		Display
	}

	public class OrderPageViewModel : BindableBase, INotifyPropertyChanged, INavigatedAware
	{
		#region private Proterties
        private static Color ButtonSeletedColor = (Color)Application.Current.Resources["ComplementColor"];
		private static Color ButtonNotSeletedColor = (Color)Application.Current.Resources["BrandColor"];
		#endregion
		#region public Properties
		private Order _order = null;
		public Order Order
		{
			get
			{
				return _order;
			}
			set
			{
				SetProperty(ref _order, value);
				this.LoadData(value);
			}
		}
		#endregion
		#region UIProperties
		// Manual Setup Properties
		private string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

        private bool _allowSaveAndNew = false;
        public bool AllowSaveAndNew
        {
            get
            {
                return _allowSaveAndNew;
            }
            set { SetProperty(ref _allowSaveAndNew, value); }
        }

		private bool _canAddNotes = false;
		public bool CanAddNotes
		{
			get
			{
				return _canAddNotes;
			}
			set { SetProperty(ref _canAddNotes, value); }
		}

        private Color _deliveredButtonColor = ButtonNotSeletedColor;
		public Color DeliveredButtonColor
		{
			get
			{
				return _deliveredButtonColor;
			}
			set
			{
				SetProperty(ref _deliveredButtonColor, value);
			}
		}

        private Color _deliverFailedButtonColor = ButtonNotSeletedColor;
		public Color DeliverFailedButtonColor
		{
			get
			{
				return _deliverFailedButtonColor;
			}
			set
			{
				SetProperty(ref _deliverFailedButtonColor, value);
			}
		}


		private bool _isDeliveryMode = false;
		public bool IsDeliveryMode
		{
			get
			{
				return _isDeliveryMode;
			}
			set { 
				SetProperty(ref _isDeliveryMode, value);
				if (value)
					CanAddNotes = true;
			}
		}

		private bool _isEditMode = false;
		public bool IsEditMode
		{
			get
			{
				return _isEditMode;
			}
			set { 
				SetProperty(ref _isEditMode, value);
				CanAddNotes = value;
			}
		}

		private bool _isInsertMode = false;
		public bool IsInsertMode
		{
			get
			{
				return _isInsertMode;
			}
			set { SetProperty(ref _isInsertMode, value); }
		}

		private UpdateMode _updateMode;
		public UpdateMode UpdateMode
		{
			get
			{
				return _updateMode;
			}
			set
			{
				SetProperty(ref _updateMode, value);
				switch (value)
				{
					case UpdateMode.Insert:
						IsInsertMode = true;
						IsEditMode = true;
						break;
					case UpdateMode.Edit:
						IsInsertMode = false;
						IsEditMode = true;
						break;
					case UpdateMode.Display:
						IsInsertMode = false;
						IsEditMode = false;
						break;
				}
			}
		}

		// Generated Properties

		private double _amount;
		public double Amount
		{
			get
			{
				return _amount;
			}
			set { SetProperty(ref _amount, value); }
		}
		private string _cardText;
		public string CardText
		{
			get
			{
				return _cardText;
			}
			set { SetProperty(ref _cardText, value); }
		}
		private int _clearingNumber;
		public int ClearingNumber
		{
			get
			{
				return _clearingNumber;
			}
			set { SetProperty(ref _clearingNumber, value); }
		}

		private string _contactPhone;
		public string ContactPhone
		{
			get
			{
				return _contactPhone;
			}
			set { SetProperty(ref _contactPhone, value); }
		}

		private DateTime _deliveryDate = DateTime.Today;
		public DateTime DeliveryDate
		{
			get
			{
				return _deliveryDate;
			}
			set { SetProperty(ref _deliveryDate, value); }
		}

		private DateTime _deliveryTime;
		public DateTime DeliveryTime
		{
			get
			{
				return _deliveryTime;
			}
			set { SetProperty(ref _deliveryTime, value); }
		}
		private DeliveryTime _deliveryTimeCode;
		public DeliveryTime DeliveryTimeCode
		{
			get
			{
				return _deliveryTimeCode;
			}
			set { SetProperty(ref _deliveryTimeCode, value); }
		}

		private int _id;
		public int Id
		{
			get
			{
				return _id;
			}
			set { SetProperty(ref _id, value); }
		}

		private string _notes;
		public string Notes
		{
			get
			{
				return _notes;
			}
			set { SetProperty(ref _notes, value); }
		}
		private string _orderNumber;
		public string OrderNumber
		{
			get
			{
				return _orderNumber;
			}
			set { SetProperty(ref _orderNumber, value); }
		}

		private string _orderSummary;
		public string OrderSummary
		{
			get
			{
				return _orderSummary;
			}
			set { SetProperty(ref _orderSummary, value); }
		}

		private string _orderSystemClientId;
		public string OrderSystemClientId
		{
			get
			{
				return _orderSystemClientId;
			}
			set { SetProperty(ref _orderSystemClientId, value); }
		}

		private string _priority;
		public string Priority
		{
			get
			{
				return _priority;
			}
			set { SetProperty(ref _priority, value); }
		}
		private string _product1Code;
		public string Product1Code
		{
			get
			{
				return _product1Code;
			}
			set { SetProperty(ref _product1Code, value); }
		}
		private string _product1Description;
		public string Product1Description
		{
			get
			{
				return _product1Description;
			}
			set { SetProperty(ref _product1Description, value); }
		}

		private string _recipientCompanyName;
		public string RecipientCompanyName
		{
			get
			{
				return _recipientCompanyName;
			}
			set { SetProperty(ref _recipientCompanyName, value); }
		}

		private string _recipientName;
		public string RecipientName
		{
			get
			{
				return _recipientName;
			}
			set { SetProperty(ref _recipientName, value); }
		}

		private string _recipientState;
		public string RecipientState
		{
			get
			{
				return _recipientState;
			}
			set { SetProperty(ref _recipientState, value); }
		}
		private string _recipientStreetAddress;
		public string RecipientStreetAddress
		{
			get
			{
				return _recipientStreetAddress;
			}
			set { SetProperty(ref _recipientStreetAddress, value); }
		}
		private string _recipientSuburb;
		public string RecipientSuburb
		{
			get
			{
				return _recipientSuburb;
			}
			set { SetProperty(ref _recipientSuburb, value); }
		}
		private string _recipientZip;
		public string RecipientZip
		{
			get
			{
				return _recipientZip;
			}
			set { SetProperty(ref _recipientZip, value); }
		}

		private string _senderEmail;
		public string SenderEmail
		{
			get
			{
				return _senderEmail;
			}
			set { SetProperty(ref _senderEmail, value); }
		}
		private string _senderName;
		public string SenderName
		{
			get
			{
				return _senderName;
			}
			set { SetProperty(ref _senderName, value); }
		}

		private string _specialInstructions;
		public string SpecialInstructions
		{
			get
			{
				return _specialInstructions;
			}
			set { SetProperty(ref _specialInstructions, value); }
		}
		private OrderStatus _status;
		public OrderStatus Status
		{
			get
			{
				return _status;
			}
			set { 
                SetProperty(ref _status, value);
                switch (value)
                {
                    case OrderStatus.Delivered:
                        DeliveredButtonColor = ButtonSeletedColor;
                        DeliverFailedButtonColor = ButtonNotSeletedColor;
                        break;
                    case OrderStatus.FailedDelivery:
                        DeliveredButtonColor = ButtonNotSeletedColor;
                        DeliverFailedButtonColor = ButtonSeletedColor;
                        break;
                    default:
                        DeliveredButtonColor = ButtonNotSeletedColor;
                        DeliverFailedButtonColor = ButtonNotSeletedColor;
                        break;
                
				}
            }
		}
		#endregion
		#region Commands
		private DelegateCommand _editCommand;
		public DelegateCommand EditCommand => _editCommand != null ? _editCommand : (_editCommand = new DelegateCommand(DoEditCommand));
		private void DoEditCommand()
		{
            if (UpdateMode != UpdateMode.Insert)
            {
                IsEditMode = true;
                if (IsDeliveryMode){
                    AllowSaveAndNew = false;
                    IsDeliveryMode = false;
                } else {
                    AllowSaveAndNew = true;
                }
            }
		}

		private DelegateCommand _saveAndExitCommand;
		public DelegateCommand SaveAndExitCommand => _saveAndExitCommand != null ? _saveAndExitCommand : (_saveAndExitCommand = new DelegateCommand(async () => await DoSaveAndExitCommand()));
		private async Task DoSaveAndExitCommand()
		{
			this.LoadObject(Order);

			var isValid = await Order.Validate();
			if (!isValid)
			{
				ErrorMessage = Order.InvalidReason;
			}
			else
			{
				await Order.SaveAsync();
				await _navigationService.GoBackAsync();
			}
		}

		private DelegateCommand _saveAndNewCommand;
		public DelegateCommand SaveAndNewCommand => _saveAndNewCommand != null ? _saveAndNewCommand : (_saveAndNewCommand = new DelegateCommand(async () => await DoSaveAndNewCommand()));
		private async Task DoSaveAndNewCommand()
		{
			this.LoadObject(Order);

			var isValid = await Order.Validate();
			if (!isValid)
			{
				ErrorMessage = Order.InvalidReason;
			}
			else
			{
				await Order.SaveAsync();
				Order = new Order();
			}

		}

		private DelegateCommand _cancelCommand;
		public DelegateCommand CancelCommand => _cancelCommand != null ? _cancelCommand : (_cancelCommand = new DelegateCommand(async () => await DoCancelCommand()));
		private async Task DoCancelCommand()
		{
            Order = new Order();
			await _navigationService.GoBackAsync();
		}

		private DelegateCommand _deliveredCommand;
		public DelegateCommand DeliveredCommand => _deliveredCommand != null ? _deliveredCommand : (_deliveredCommand = new DelegateCommand(async () => await DoDeliveredCommand()));
		private async Task DoDeliveredCommand()
		{
			if (Order.Status == OrderStatus.Delivered)
				Order.Status = OrderStatus.Ordered;
			else
				Order.Status = OrderStatus.Delivered;
			
			await Order.SaveAsync();

			Debug.WriteLine("Saved - UpdateTimestamp {0}", Order.UpdateTimestamp);
			await _navigationService.GoBackAsync();
		}

		private DelegateCommand _deliverFailedCommand;
		public DelegateCommand DeliverFailedCommand => _deliverFailedCommand != null ? _deliverFailedCommand : (_deliverFailedCommand = new DelegateCommand(async () => await DoDeliverFailedCommand()));
		private async Task DoDeliverFailedCommand()
		{
			if (Order.Status == OrderStatus.FailedDelivery)
				Order.Status = OrderStatus.Ordered;
			else
				Order.Status = OrderStatus.FailedDelivery;

			await Order.SaveAsync();
			await _navigationService.GoBackAsync();
		}

		private DelegateCommand _turnByTurnCommand;
		public DelegateCommand TurnByTurnCommand => _turnByTurnCommand != null ? _turnByTurnCommand : (_turnByTurnCommand = new DelegateCommand(async () => await DoTurnByTurnCommand()));
		private async Task DoTurnByTurnCommand()
		{
			var address = String.Format("{0} {1}", Order.RecipientStreetAddress, Order.RecipientSuburb);
			address = address.Replace("  ", " ");
			address = address.Replace("  ", " ");
			address = address.Replace(" ", "+");

			var googleMapUrl = String.Format("comgooglemaps://?q={0}",address);
			Xamarin.Forms.Device.OpenUri(new Uri(googleMapUrl));
		}


		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

			var message = String.Format("{0}", AppResources.ExampleHelpText);
			string[] values = { AppResources.Help, message, AppResources.GotIt };
			MessagingCenter.Send<OrderPageViewModel, string[]>(this, "OrderPageDisplayAlert", values);
		}

		#endregion
		#region public methods
		public void OnAppear()
		{
			// This forces at least a single reference 'currentLocation'
			if (IsEditMode)
			{
				Task.Run(async () =>
				{
					var currentLocation = await MapHelper.GeoCoder.GetMyCurrentLocationAsync();
					if (currentLocation == null)
					{
						var message = String.Format("{0}", "Unable to get your current location. Do you have internet coverage?");
						string[] values = { AppResources.Error, message, AppResources.Ok };
						MessagingCenter.Send<OrderPageViewModel, string[]>(this, "DisplayErrorAndExit", values);
					}

				});

			}
		}

		public void OnDisappear()
		{
			if (Order != null)
			{
				this.LoadObject(Order);
				//Order.Save();
			}
		}
		#endregion
		#region private Methods
		private void LoadData(Order obj)
		{
			this.Amount = obj.Amount;
			this.CardText = obj.CardText;
			this.ClearingNumber = obj.ClearingNumber;
			this.ContactPhone = obj.ContactPhone;
			this.DeliveryDate = obj.DeliveryDate;
			this.DeliveryTime = obj.DeliveryTime;
			this.DeliveryTimeCode = obj.DeliveryTimeCode;
			this.Id = (int)obj.Id;
			this.Notes = obj.Notes;
			this.OrderNumber = obj.OrderNumber;
			this.OrderSystemClientId = obj.OrderSystemClientId;
			this.Priority = obj.Priority;
			this.Product1Code = obj.Product1Code;
			this.Product1Description = obj.Product1Description;
			this.RecipientCompanyName = obj.RecipientCompanyName;
			this.RecipientName = obj.RecipientName;
			this.RecipientState = obj.RecipientState;
			this.RecipientStreetAddress = obj.RecipientStreetAddress;
			this.RecipientSuburb = obj.RecipientSuburb;
			this.RecipientZip = obj.RecipientZip;
			this.SenderEmail = obj.SenderEmail;
			this.SenderName = obj.SenderName;
			this.SpecialInstructions = obj.SpecialInstructions;
			this.Status = obj.Status;

			this.OrderSummary = obj.Summary;
		}
		private void LoadObject(Order obj)
		{
			Debug.WriteLine("WARNING: Client Id Hard Coded to '{0}'", Settings.DefaultClientId);

			obj.ClientId = Settings.DefaultClientId;

			obj.Amount = this.Amount;
			obj.CardText = this.CardText;
			obj.ClearingNumber = this.ClearingNumber;
			obj.ContactPhone = this.ContactPhone;
			obj.DeliveryDate = this.DeliveryDate;
			obj.DeliveryTime = this.DeliveryTime;
			obj.DeliveryTimeCode = this.DeliveryTimeCode;
			obj.Id = (int)this.Id;
			obj.Notes = this.Notes;
			obj.OrderNumber = this.OrderNumber;
			obj.OrderSystemClientId = this.OrderSystemClientId;
			obj.Priority = this.Priority;
			obj.Product1Code = this.Product1Code;
			obj.Product1Description = this.Product1Description;
			obj.RecipientCompanyName = this.RecipientCompanyName;
			obj.RecipientName = this.RecipientName;
			obj.RecipientState = this.RecipientState;
			obj.RecipientStreetAddress = this.RecipientStreetAddress;
			obj.RecipientSuburb = this.RecipientSuburb;
			obj.RecipientZip = this.RecipientZip;
			obj.SenderEmail = this.SenderEmail;
			obj.SenderName = this.SenderName;
			obj.SpecialInstructions = this.SpecialInstructions;
			obj.Status = this.Status;
		}
		#endregion

		#region INavigateAware
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
			// left this program
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			if (parameters.ContainsKey("OrderId"))
			{
				var orderId = (int)parameters["OrderId"];
				if (orderId == 0)
				{
					Order = new Order();
				}
				else
				{
					Order = new Order(orderId);
				}
			}
			else
			{
				Order = new Order();
			}

			if (parameters.ContainsKey("UpdateMode"))
			{
				IsDeliveryMode = false;
				switch ((string)parameters["UpdateMode"])
				{
					case "Insert":
						UpdateMode = UpdateMode.Insert;
						break;
					case "Edit":
						UpdateMode = UpdateMode.Edit;
						break;
					case "Display":
						UpdateMode = UpdateMode.Display;
						break;
					case "Delivery":
						UpdateMode = UpdateMode.Display;
						IsDeliveryMode = true;
						break;
					default:
						UpdateMode = UpdateMode.Display;
						break;
				}
			}
			else
				UpdateMode = UpdateMode.Display;

		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{

		}
		#endregion

		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IExampleServices _exampleServices;

		public OrderPageViewModel(
			INavigationService navigationService
		)
		{
			_navigationService = navigationService;
		}
		#endregion
	}
}
