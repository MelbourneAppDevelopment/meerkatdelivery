﻿using System;
using Xamarin.Forms;
using Prism.Commands;
using Prism.Mvvm;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using System.Diagnostics;
using Prism.Navigation;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Resources;
using System.ComponentModel;
using Helpers;
using Microsoft.Practices.Unity;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace MeerkatDelivery.ViewModels
{
	public class OrdersPageViewModel : BindableBase, INotifyPropertyChanged, INavigatedAware
	{
		#region private Proterties
		TimerState batchTimer;

		#endregion
		#region UIProperties
		bool _isRefreshing;
		public bool IsRefreshing
		{
			get
			{
				return _isRefreshing;
			}
			set
			{
				SetProperty(ref _isRefreshing, value);
			}
		}

		string _deviceName;
		public string DeviceName
		{
			get
			{
				return _deviceName;
			}
			set
			{
				SetProperty(ref _deviceName, value);
			}
		}

		string _errorMessage = "";
		public string ErrorMessage
		{
			get
			{
				return _errorMessage;
			}
			set { SetProperty(ref _errorMessage, value); }
		}

		ObservableCollection<OrderItemViewModel> _orderItems = new ObservableCollection<OrderItemViewModel>();
		public ObservableCollection<OrderItemViewModel> OrderItems
		{
			get
			{
				return _orderItems;
			}
			set { SetProperty(ref _orderItems, value); }
		}
		#endregion
		#region ICommands
		//private DelegateCommand<AuthorisedPump> _onItemSelectedCommand;
		//public DelegateCommand<AuthorisedPump> OnItemSelectedCommand => _onItemSelectedCommand != null ? _onItemSelectedCommand : (_onItemSelectedCommand = new DelegateCommand<AuthorisedPump>(DoOnItemSelectedCommand));
		//private async void DoOnItemSelectedCommand(AuthorisedPump authorisedPump)
		//{
		//}
		private DelegateCommand<OrderItemViewModel> _onItemTappedCommand;
		public DelegateCommand<OrderItemViewModel> OnItemTappedCommand => _onItemTappedCommand != null ? _onItemTappedCommand : (_onItemTappedCommand = new DelegateCommand<OrderItemViewModel>(DoOnItemTappedCommand));
		private async void DoOnItemTappedCommand(OrderItemViewModel item)
		{
			var param = new NavigationParameters();
			param.Add("OrderId", item.Order.Id);
			param.Add("UpdateMode", "Display");

			await _navigationService.NavigateAsync("OrderPage", param, false, true);
		}

		private DelegateCommand _addCommand;
		public DelegateCommand AddCommand => _addCommand != null ? _addCommand : (_addCommand = new DelegateCommand(DoAddCommand));
		private async void DoAddCommand()
		{
			var param = new NavigationParameters();
			param.Add("UpdateMode", "Insert");
			await _navigationService.NavigateAsync("OrderPage", param, false, true);
		}

		//private DelegateCommand<OrderItemViewModel> _editCommand;
		//public DelegateCommand<OrderItemViewModel> EditCommand => _editCommand != null ? _editCommand : (_editCommand = new DelegateCommand<OrderItemViewModel>(DoEditCommand));
		//private async void DoEditCommand(OrderItemViewModel item)
		//{
		//	var param = new NavigationParameters();
		//	param.Add("OrderId", item.Order.Id);
		//	param.Add("UpdateMode", "Edit");

		//	await _navigationService.NavigateAsync("OrderPage", param, false, true);
		//}

		private DelegateCommand<OrderItemViewModel> _deleteCommand;
		public DelegateCommand<OrderItemViewModel> DeleteCommand => _deleteCommand != null ? _deleteCommand : (_deleteCommand = new DelegateCommand<OrderItemViewModel>(async (item) => await DoDeleteCommand(item)));
		private async Task DoDeleteCommand(OrderItemViewModel item)
		{
			var message = String.Format("{0} {1}?", AppResources.DeleteOrderText, item.Order.Id);
			string[] values = { AppResources.Help, message, AppResources.GotIt, AppResources.Cancel };

			Action action = () => DeleteOrder(item);
			var args = new Tuple<string[], Action>(values, action);

			MessagingCenter.Send<OrdersPageViewModel, Tuple<string[], Action>>(this, "OrdersPageDeleteOrder", args);
		}

		public void DeleteOrder(OrderItemViewModel ordervm)
		{
			ordervm.Order.Delete();
			OrderItems.Remove(ordervm);
		}


		private DelegateCommand _refreshListCommand;
		public DelegateCommand RefreshListCommand => _refreshListCommand != null ? _refreshListCommand : (_refreshListCommand = new DelegateCommand( async () => await DoRefreshListCommand() ));
		private async Task DoRefreshListCommand()
		{
			IsRefreshing = true;
			//await Order.SyncWithServerAsync();
			IsRefreshing = false;
		}

		private DelegateCommand _filterCommand;
		public DelegateCommand FilterCommand => _filterCommand != null ? _filterCommand : (_filterCommand = new DelegateCommand(DoFilterCommand));
		private async void DoFilterCommand()
		{
			await _navigationService.NavigateAsync("FilterPage", null, false,true);
		}

		private DelegateCommand _helpCommand;
		public DelegateCommand HelpCommand => _helpCommand != null ? _helpCommand : (_helpCommand = new DelegateCommand(DoHelpCommand));
		private void DoHelpCommand()
		{

			var message = String.Format("{0}", AppResources.ExampleHelpText);
			string[] values = { AppResources.Help, message, AppResources.GotIt };
			MessagingCenter.Send<OrdersPageViewModel, string[]>(this, "OrdersPageDisplayAlert", values);
		}

		#endregion
		#region public methods
		#endregion
		#region private Methods
		private async Task LoadList()
		{
			_orderItems.Clear();
			//TODO should be GetAll(clientId);
			var orders = await Order.GetAllAsync();
			Debug.WriteLine("OrdersPageViewModel.LoadOrders: count of orders = {0}", orders.Count);
			foreach (var order in orders)
			{
				var item = new OrderItemViewModel();
				item.Order = order;
				if (!item.Order.Deleted)
					_orderItems.Add(item);
			}
		}
		#endregion
		#region Batch Processing
		private void StartTimer()
		{
			batchTimer = new TimerState();
			TimerCallback timerDelegate = new TimerCallback(ThreadedBatchProcess);
			Timer timer = new Timer(timerDelegate, batchTimer, 1000, 1000);
			batchTimer.tmr = timer;  // So we have a handle to dispose
		}

		private void StopTimer()
		{
			if (batchTimer != null && batchTimer.tmr != null)
			{
				batchTimer.tmr.Cancel();
				batchTimer.tmr.Dispose();
				batchTimer = null;
			}
		}

		private void ThreadedBatchProcess(Object state)
		{
			Debug.WriteLine("Timer ran");
			// Do stuff
		}
		#endregion
		#region INavigateAware
		/// <summary>
		/// Triggered from code behind when page appears
		/// </summary>
		public void OnAppearing()
		{
            LoadList();
		}

		public void OnDisappearing()
		{
		}
		//THIS DON'T WORK when triggered by back button on NavBar or Android physical button
		public void OnNavigatedFrom(NavigationParameters parameters)
		{
			// left this program
		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{
		}
		#endregion

		#region Constructors
		private readonly INavigationService _navigationService;
		private readonly IExampleServices _exampleServices;

		public OrdersPageViewModel(
			INavigationService navigationService
		)
		{
			_navigationService = navigationService;

			// Update the Order List whenever database changes
			MessagingCenter.Subscribe<Order>(this, "OrdersUpdated", (sender) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
				  {
					  var orderObjects = await Order.GetAllAsync();
					  foreach (var obj in orderObjects)
					  {
						  //TODO a bit rough
						  var found = false;
						  OrderItemViewModel existingItem = null;
						  foreach (var oi in OrderItems)
							  if (oi.Order.Id == obj.Id)
							  {    // already in Orders
								  existingItem = oi;
								  found = true;
								  break;
							  }

						  if (found)
						  {
							existingItem.Order.LoadFromDB(obj.DbOrder); // reload with new values
						  }
						  else
						  {
							  var item = new OrderItemViewModel();
							  item.Order = obj;
							  if (!item.Order.Deleted)
								  OrderItems.Add(item);
						  }
					  
					  }
				  });

			});
		}
		#endregion
	}
}
