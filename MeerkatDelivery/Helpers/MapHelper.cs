﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;
using System.Collections.ObjectModel;

namespace MeerkatDelivery.Helpers
{
	public class MapHelper
	{
		Geocoder geoCoder = new Geocoder();
		public static int EQUATOR_RADIUS = 6378137; // meters
		public static double DEGREES_TO_RADIANS = Math.PI / 180.0;


		static private MapHelper _mapHelper = null;
		public static MapHelper GeoCoder
		{
			get
			{
				if (_mapHelper == null)
					_mapHelper = new MapHelper();
				return _mapHelper;
			}
		}


		public async Task<Position> GetOnePositionFromAddressAsync(string address, int maxDistanceFrom = 0)
		{
			var possiblePositions = await GetPositionFromAddressAsync(address, maxDistanceFrom = 0);
			if (possiblePositions.Count == 0)
				return new Position();

			if (possiblePositions.Count == 1)
				return possiblePositions[0];

			var currentLocation = await GetMyCurrentLocationAsync();
			if (currentLocation == null)
				return possiblePositions[0]; // trust google and return the first one

			double minDistance = double.MaxValue;
			Position minPosition = new Position();

			foreach (var position in possiblePositions)
			{
				var distance = DistanceBetween(currentLocation, position);
				if (distance < minDistance)
				{
					minDistance = distance;
					minPosition = position;
				}
			}
			return minPosition;
		}

		/// <summary>
		/// Gets the gps position from address string.
		/// </summary>
		/// <returns>A list of possible locations</returns>
		/// <param name="address">Address string.</param>
		/// <param name="distanceFrom">Max Km from current location to look.</param>
		public async Task<List<Position>> GetPositionFromAddressAsync(string address, int maxDistanceFrom = 0)
		{
			List<Position> result = new List<Position>();
			try
			{
				var res = await geoCoder.GetPositionsForAddressAsync(address);
				if (res != null)
				{
					result = res.ToList();

					// remove values outside range
					if (result.Count() > 0 && maxDistanceFrom != 0)
					{
						var currentLocation = await GetMyCurrentLocationAsync();
						if (currentLocation != null)            // If current position not known, assume all return values are ok
						{
							foreach (var pos in result)
							{
								// Only return positions with maxDistance from current location
								if (DistanceBetween(currentLocation, pos) / 1000 > maxDistanceFrom)
									result.Remove(pos);
							}
						}
					}
				}
			}
			catch (System.OperationCanceledException ex)
			{
				Debug.WriteLine("GPS GeoCoder failed to find a location");
			}
			return result;
		}

		/// <summary>
		/// Gets the current GPS location of the device,
		/// but check a maximum number of times based on Settings.CheckLocationMinutes.
		/// If GPS isn't availble, return the last know location.
		/// </summary>
		/// <returns>The current location.</returns>
		private DateTime _lastChecked = DateTime.MinValue;
		private Position _currentLocation;
		public async Task<Position> GetMyCurrentLocationAsync()
		{
			if (DateTime.Now - _lastChecked > TimeSpan.FromMinutes(Settings.CheckLocationMinutes))
			{
				Position result = await this.GetActualCurrentLocationAsync();
				if (result.Latitude != 0)
				{
					_currentLocation = result;
					_lastChecked = DateTime.Now;
				}
			}
			return _currentLocation;
		}
		private async Task<Position> GetActualCurrentLocationAsync()
		{
			Plugin.Geolocator.Abstractions.Position geolocatorPosition = new Plugin.Geolocator.Abstractions.Position();
			try
			{
				geolocatorPosition = await CrossGeolocator.Current.GetPositionAsync(timeoutMilliseconds: Settings.GeolocationTimeoutMilliseconds);
			}
			catch (System.OperationCanceledException ex)
			{
				Debug.WriteLine("GPS CrossGeolocator failed to find current location");
			}

			if (geolocatorPosition == null)
				return new Position();
			return new Position(geolocatorPosition.Latitude, geolocatorPosition.Longitude);
		}

		/// <summary>
		/// Distances the between two gps locations in meters.
		/// </summary>
		/// <returns>The distance in meters</returns>
		public static double DistanceBetween(Position a, Position b)
		{
			// From https://github.com/XLabs/Xamarin-Forms-Labs/blob/master/src/Platform/XLabs.Platform/Services/GeoLocation/PositionExtensions.cs

			var bLatRad = DegreesToRadians(b.Latitude);
			var bLngRad = DegreesToRadians(b.Longitude);

			var myLatRad = DegreesToRadians(a.Latitude);
			var myLngRad = DegreesToRadians(a.Longitude);

			var dLat = bLatRad - myLatRad;
			var dLng = bLngRad - myLngRad;

			var a1 = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(myLatRad) * Math.Cos(bLatRad) * Math.Sin(dLng / 2) * Math.Sin(dLng / 2);
			var distance = 2 * Math.Atan2(Math.Sqrt(a1), Math.Sqrt(1 - a1));

			var actualDistance = EQUATOR_RADIUS * distance;

			return actualDistance;
		}

		/// <summary>
		/// The central postion in a list of gpw positions.
		/// </summary>

		public static Position CenterPosition(List<Position> dots)
		{
			double totalLat = 0, totalLng = 0;
			foreach (Position p in dots)
			{
				totalLat += p.Latitude;
				totalLng += p.Longitude;
			}

			double centerLat = totalLat / dots.Count;
			double centerLng = totalLng / dots.Count;
			Position center = new Position(centerLat, centerLng);

			return center;

		}

		public static Position CenterPosition(ObservableCollection<Position> dots)
		{
			double totalLat = 0, totalLng = 0;
			foreach (Position p in dots)
			{
				totalLat += p.Latitude;
				totalLng += p.Longitude;
			}

			double centerLat = totalLat / dots.Count;
			double centerLng = totalLng / dots.Count;
			Position center = new Position(centerLat, centerLng);

			return center;
		}

		public static Position CenterPosition(ObservableCollection<Pin> dots)
		{
			double totalLat = 0, totalLng = 0;
			foreach (Pin p in dots)
			{
				totalLat += p.Position.Latitude;
				totalLng += p.Position.Longitude;
			}

			double centerLat = totalLat / dots.Count;
			double centerLng = totalLng / dots.Count;
			Position center = new Position(centerLat, centerLng);

			return center;
		}

		/// <summary>
		/// Returns the radius of a list of GPS locations from a central point
		/// </summary>
		public static double RadiusOf(Position centralPos, List<Position> dots)
		{
			double maxDistance = 0;
			foreach (var dot in dots)
			{
				var dist = Math.Abs(DistanceBetween(centralPos, dot));
				if (dist > maxDistance)
					maxDistance = dist;
			}
			if (maxDistance < Settings.MinMapRadius)
				maxDistance = Settings.MinMapRadius;
			return maxDistance;
		}

		public static double RadiusOf(Position centralPos, ObservableCollection<Position> dots)
		{
			double maxDistance = 0;
			foreach (var dot in dots)
			{
				var dist = Math.Abs(DistanceBetween(centralPos, dot));
				if (dist > maxDistance)
					maxDistance = dist;
			}
           if (maxDistance < Settings.MinMapRadius)
                maxDistance = Settings.MinMapRadius;
			return maxDistance;
		}

		public static double RadiusOf(Position centralPos, ObservableCollection<Pin> dots)
		{
			double maxDistance = 0;
			foreach (var dot in dots)
			{
				var dist = Math.Abs(DistanceBetween(centralPos, dot.Position));
				if (dist > maxDistance)
					maxDistance = dist;
			}
            if (maxDistance < Settings.MinMapRadius)
                maxDistance = Settings.MinMapRadius;
			return maxDistance;
		}

		/// <summary>
		/// Calculates bearing between start and stop.
		/// </summary>
		/// <param name="start">Start coordinates.</param>
		/// <param name="stop">Stop coordinates.</param>
		/// <returns>The <see cref="System.Double" />.</returns>
		//public static double BearingFrom(this Position start, Position stop)
		//{
		//	var deltaLon = stop.Longitude - start.Longitude;
		//	var cosStop = Math.Cos(stop.Latitude);
		//	return Math.Atan2(
		//		(Math.Cos(start.Latitude) * Math.Sin(stop.Latitude)) -
		//		(Math.Sin(start.Latitude) * cosStop * Math.Cos(deltaLon)),
		//		Math.Sin(deltaLon) * cosStop);       
		//}


		/// <summary>
		/// Calculates the end-point from a given source at a given range (meters) and bearing (degrees).
		/// This methods uses simple geometry equations to calculate the end-point.
		/// </summary>
		/// <param name="source">Point of origin</param>
		/// <param name="range">Range in meters</param>
		/// <param name="bearing">Bearing in degrees</param>
		/// <returns>End-point from the source given the desired range and bearing.</returns>
		public static Position CalculateDerivedPosition(Position source, double range, double bearing)
		{
			double latA = source.Latitude * DEGREES_TO_RADIANS;
			double lonA = source.Longitude * DEGREES_TO_RADIANS;
			double angularDistance = range / EQUATOR_RADIUS;
			double trueCourse = bearing * DEGREES_TO_RADIANS;

			double lat = Math.Asin(
				Math.Sin(latA) * Math.Cos(angularDistance) +
				Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

			double dlon = Math.Atan2(
				Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA),
				Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));

			double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

			return new Position(
				lat / DEGREES_TO_RADIANS,
				lon / DEGREES_TO_RADIANS);
			}

	
		public static double DegreesToRadians(double degrees)
		{
			return degrees * Math.PI / 180.0;
		}

		public static double RadianToDegrees(double radians)
		{
			return radians / Math.PI * 180.0;
		}


	}
}
