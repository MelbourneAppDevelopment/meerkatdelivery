﻿using System;
namespace MeerkatDelivery.Helpers
{
	public static class DateTimeUtils
	{
		public static TimeSpan DateTimeToTimeSpan(DateTime? ts)
		{
			if (!ts.HasValue) return TimeSpan.Zero;
			else return new TimeSpan(ts.Value.Day, ts.Value.Hour, ts.Value.Minute, ts.Value.Second, ts.Value.Millisecond);
		}
	}
}
