﻿using System;
namespace MeerkatDelivery
{
	public enum RangeCriteria
	{
		Any, EQ, NE, LT, LE, GT, GE
	}

	public class OrderStatusRange 
	{
		public OrderStatus? OrderStatus { get; set; }
		public RangeCriteria RangeCriteria { get; set; }

		public OrderStatusRange(RangeCriteria rangeCriteria = RangeCriteria.Any, OrderStatus? orderStatus = null)
		{
			RangeCriteria = rangeCriteria;
			OrderStatus = orderStatus;
			if (orderStatus == null) 
				RangeCriteria = RangeCriteria.Any;
		}
	}
}
