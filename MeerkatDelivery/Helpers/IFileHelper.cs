﻿using System;
namespace MeerkatDelivery.Helpers
{
	public interface IFileHelper
	{
		string GetLocalFilePath(string filename);
	}
}
