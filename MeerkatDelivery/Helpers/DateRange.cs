﻿using System;
namespace MeerkatDelivery
{
	public class DateRange 
	{
		DateTime _startDate = DateTime.MinValue;
		public DateTime StartDate
		{
			get { return _startDate; }
			set { _startDate = value; }
		}
		DateTime _endDate = DateTime.MinValue;
		public DateTime EndDate
		{
			get { return _endDate; }
			set { 
				_endDate = value;

				// Set to end of day
				if (_endDate.Hour == 0 && _endDate.Minute == 0 && _endDate.Second == 0 && _endDate.Millisecond == 0)
				{
					_endDate.AddDays(1);
					_endDate.AddMilliseconds(-1);
				}
			}
		}

		public DateRange(DateTime? startDate = null, DateTime? endDate = null)
		{
			if (startDate == null)
				StartDate = DateTime.MinValue;
			else
				StartDate = (DateTime)startDate;
			
			if (endDate == null)
				EndDate = DateTime.MinValue;
			else
				EndDate = (DateTime)endDate;
		}
	}
}
