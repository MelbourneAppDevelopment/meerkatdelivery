﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MeerkatDelivery.CustomControls
{
	public class CustomMap : Map
	{
		#region Bindings
		public static BindableProperty PinsItemsSourceProperty =
		BindableProperty.Create(
		"PinsItemsSource",
		typeof(IEnumerable<Pin>),
		typeof(CustomMap),
		default(IEnumerable<Pin>),
		propertyChanged: OnPinsItemsSourcePropertyChanged);

		public static BindableProperty SelectedItemProperty =
			BindableProperty.Create(
				"SelectedItem",
				typeof(Pin),
				typeof(CustomMap),
				null,
				defaultBindingMode: BindingMode.TwoWay,
				propertyChanged: OnSelectedItemChanged
				);
		#endregion
		#region Properties
		public Pin SelectedItem
		{
			get { return (Pin)GetValue(SelectedItemProperty); }
			set { SetValue(SelectedItemProperty, value); }
		}

		public IEnumerable<Pin> PinsItemsSource
		{
			get { return (IEnumerable<Pin>)GetValue(PinsItemsSourceProperty); }
			set { SetValue(PinsItemsSourceProperty, value); }
		}

		private ObservableCollection<CustomPin> _customPins = new ObservableCollection<CustomPin>();
		public ObservableCollection<CustomPin> CustomPins {
			get { return _customPins; }
			set { _customPins = value; }
		}

		private ObservableCollection<Pin> _pinsCollection;
		protected ObservableCollection<Pin> PinsCollection
		{
			get { return _pinsCollection; }
			set
			{
				if (_pinsCollection != null)
				{
					_pinsCollection.CollectionChanged -= OnObservableCollectionChanged;
				}

				_pinsCollection = value;

				if (_pinsCollection != null)
				{
					_pinsCollection.CollectionChanged += OnObservableCollectionChanged;
				}
			}
		}
		#endregion
		#region delegates
		private static void OnPinsItemsSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var control = (CustomMap)bindable;

			control.PinsCollection = newValue as ObservableCollection<Pin>;
		}


		private void OnObservableCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (Pin pin in e.NewItems)
				{
					pin.Clicked += OnPinClicked;
					Pins.Add(pin);
				}
			}

			if (e.Action == NotifyCollectionChangedAction.Remove)
			{
				foreach (Pin pin in e.OldItems)
				{
					pin.Clicked -= OnPinClicked;
					Pins.Remove(pin);
				}
			}
		}

		private void OnPinClicked(object sender, EventArgs e)
		{
			SelectedItem = (Pin)sender;
		}

		private static void OnSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var map = (CustomMap)bindable;
			var pin = newValue as Pin;
			if (pin != null)
			{
				Distance distance = map.VisibleRegion.Radius;
				MapSpan region = MapSpan.FromCenterAndRadius(pin.Position, distance);
				map.MoveToRegion(region);
			}
		}
		#endregion
	}
}
