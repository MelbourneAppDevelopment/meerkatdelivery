﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using MeerkatDelivery.CustomControls;
using Xamarin.Forms;

namespace MeerkatDelivery.CustomControls
{
	/// <summary>
	/// A XAML layout container that controls the expandability of the layout it contains.
	/// See also 
	/// </summary>
	public partial class ExpandablePanContainer : ContentView
	{
		double x, y, width, height;
		double h, tempHeight;

		static double minHeightFactor = 20.0;
		double minHeight, maxHeight;

		bool firstTime = true;

		public ExpandablePanContainer()
		{
			// Set PanGestureRecognizer.TouchPoints to control the
			// number of touch points needed to pan
			var panGesture = new PanGestureRecognizer();
			panGesture.PanUpdated += OnPanUpdated;
			GestureRecognizers.Add(panGesture);

		}

		void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			if (firstTime)
			{
				minHeight = App.ScreenHeight / minHeightFactor;
				maxHeight = App.ScreenHeight - minHeight;
				width = App.ScreenWidth;
				firstTime = false;
				height = Content.Height;
				//Debug.WriteLine("minHeight:{0}, maxHeight:{1} width:{2}, height:{3} ", minHeight, maxHeight, width, height);
			}

			switch (e.StatusType)
			{
				case GestureStatus.Running:
					//Content.HeightRequest = Content.Height + e.TotalY;

					tempHeight = height + e.TotalY;
					h = tempHeight < minHeight ? minHeight : tempHeight;
					h = h > maxHeight ? maxHeight : h;

					this.LayoutTo(new Rectangle(x, y, width, h), 150, Easing.Linear);

					//Debug.WriteLine("{0}, e.X:{1} e.Y:{2}, HR:{3} H:{4} x:{5} y:{6} w:{7} h:{8} ({9})", e.StatusType, e.TotalX, e.TotalY, Content.HeightRequest, Content.Height, x,y,width, height,e.GestureId);

					break;

				case GestureStatus.Completed:
					// Store the translation applied during the pan
					x = Content.TranslationX;
					y = Content.TranslationY;
					//width = Content.WidthRequest;
					height = h;

					break;
			}
		}
	}
}
