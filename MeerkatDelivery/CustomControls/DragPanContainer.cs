﻿using System;
using Xamarin.Forms;

namespace MeerkatDelivery.CustomControls
{
	/// <summary>
	/// Pan container.
	/// 
	/// </summary>
	public class DragPanContainer : ContentView
	{
		protected double x, y;

		private string _option = "LimitToBoundsY";
		public string Option
		{
			get
			{
				return _option;
			}
			set
			{
				_option = value;
			}
		}

		public DragPanContainer()
		{
			// Set PanGestureRecognizer.TouchPoints to control the
			// number of touch points needed to pan
			var panGesture = new PanGestureRecognizer();
			panGesture.PanUpdated += OnPanUpdated;
			GestureRecognizers.Add(panGesture);
		}

		private double startScale, currentScale, xOffset, yOffset, startX, startY;
		void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType)
			{
				case GestureStatus.Started:
					startX = e.TotalX;
					startY = e.TotalY;
					Content.AnchorX = 0;
					Content.AnchorY = 0;
					break;

				case GestureStatus.Running:
					var maxTranslationX = Content.Scale * Content.Width - Content.Width;
					Content.TranslationX = Math.Min(0, Math.Max(-maxTranslationX, xOffset + e.TotalX - startX));

					var maxTranslationY = Content.Scale * Content.Height - Content.Height;
					//Content.TranslationY = Math.Min(0, Math.Max(-maxTranslationY, yOffset + e.TotalY - startY));
					Content.TranslationY = Math.Max(Math.Min(0, yOffset + e.TotalY), -Math.Abs(Content.Height - App.ScreenHeight));
					break;

				case GestureStatus.Completed:
					Content.Layout(new Rectangle(Content.X, Content.Y + yOffset, Content.Width, Content.Height));
					xOffset = Content.TranslationX;
					yOffset = Content.TranslationY;
					break;
			}
		}

		//void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		//{
		//	switch (e.StatusType)
		//	{
		//		case GestureStatus.Running:
		//			// Translate and ensure we don't pan beyond the wrapped user interface element bounds.
		//			Content.TranslationX =
		//			  Math.Max(Math.Min(0, x + e.TotalX), -Math.Abs(Content.Width - App.ScreenWidth));

		//			switch (Option)
		//			{
		//				case "FullRangeY":			// Allow pan to go below the bottom of the Pan border
		//					Content.TranslationY =
		//					   Math.Max(y + e.TotalY, -Math.Abs(Content.Height - App.ScreenHeight));
		//					break;
		//				default:
		//					Content.TranslationY =
		//					   Math.Max(Math.Min(0, y + e.TotalY), -Math.Abs(Content.Height - App.ScreenHeight));
		//					break;
		//			}
		//			break;

		//		case GestureStatus.Completed:
		//			// Store the translation applied during the pan
		//			x = Content.TranslationX;
		//			y = Content.TranslationY;

		//			this.Layout( new Rectangle(this.X + x, this.Y + y  , this.Width, this.Height));
		//			//this.Layout( new Rectangle(Content.TranslationX, Content.TranslationY, this.Width, this.Height));
  //                  //Content.Layout( new Rectangle(Content.X + x, Content.Y - y  , Content.Width, Content.Height));
		//			break;
		//	}
		//}
	}
}