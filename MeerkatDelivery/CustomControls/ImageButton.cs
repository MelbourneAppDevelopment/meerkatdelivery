﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Windows.Input;

namespace MeerkatDelivery.CustomControls
{
	public class ImageButton : Image
	{
		//Bindable property for the command
		public static readonly BindableProperty CommandProperty =
		  BindableProperty.Create<ImageButton, ICommand>(p => p.Command, null);

		//Gets or sets the color of the command
		public ICommand Command
		{
			get { return (ICommand)GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}

		//public string Text
		//{
		//	get
		//	{

		//	}
		//	set
		//	{
		//		var a = this.
		//	}
		//}

		public ImageButton()
		{
			this.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async () =>
			   {
				   this.Opacity = 0.5;

				   if (Command != null)
				   {
					   Command.Execute(null);
				   }

				   await Task.Delay(100);
				   this.Opacity = 1.0;
			   }),
				NumberOfTapsRequired = 1
			});
		}
	}
}
