﻿using System;
using Xamarin.Forms.Maps;

namespace MeerkatDelivery.CustomControls
{
	public class CustomPin
	{
		public Pin Pin { get; set; }
		public int OrderId { get; set; }
		public string Url { get; set; }
		public string PinUrl = "RedPin.png";
	}
}
