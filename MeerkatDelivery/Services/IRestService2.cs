﻿using System.Threading.Tasks;
using System.Net.Http;

namespace MeerkatDelivery.Services
{
	public interface IRestService2
	{
		/// <summary>
		/// Makes the REST request.
		/// </summary>
		/// <returns>RestRequest which encapsulates
		/// 			T object
		///     and		HttpResponseMessage</returns>
		/// <param name="resutesUrl">REST URL.</param>
		/// <param name="method">HttpMethod.</param>
		/// <param name="body">Object to be sent</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		Task<RestResponse> MakeRequestAsync<T>(string requestUri, HttpMethod method, object body);
	}
}