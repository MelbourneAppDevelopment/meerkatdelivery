﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MeerkatDelivery.Services
{
    public class RestService
        : IRestService
    {
		#region Public Properties
		public string AccessToken { get; set; }

		private string _redirectUrl = "";
		public string RedirectUrl { 
			get{ 
				if (_redirectUrl == "")
					return Settings.AuthoriseUrl;
				return _redirectUrl;
			} 
			set{
				if (_redirectUrl != value)
					_redirectUrl = value;
			} 
		}

		#endregion
		#region Asynchronous 

		public async Task MakeRequestAsync<T>(string requestUri, HttpMethod method, object body, Action<T> validStatusAction, Action<HttpResponseMessage> invalidStatusAction, Action<Exception> errorAction)
		{
			//try
			//{
			//	if (!_reachability.IsHostReachable())
			//	{
			//		Debug.WriteLine("RESTSERVICE: (REST01) _reachability.IsHostReachable Not Connected");
			//		var http = new HttpResponseMessage();
			//		http.StatusCode = HttpStatusCode.ServiceUnavailable;
			//		invalidStatusAction(http);
			//		return;
			//	}
			//}
			//catch (Exception e)
			//{
			//	var message = String.Format("REST SERVICE: (REST02) Exception {0}", e);
			//	Debug.WriteLine(message);
			//	UserLog.Create(UserLogActions.Exception, message);
			//	errorAction(e);
			//	return;
			//}


			//try
			//{
			//	HttpClientHandler handler = new HttpClientHandler();
			//	var httpClient = new HttpClient(handler);

			//	// Create the REST request details
			//	string bodyString = this.SerializeBody(body);

			//	// TODO Done this way because the Yii server is not fully REST yet

			//	HttpResponseMessage response;
			//	if (method == HttpMethod.Get)
			//	{
			//		if (requestUri.IndexOf("api/useraccess") == -1)
			//		{   // Only string access token if not an auth request
			//			if (bodyString != "")
			//				bodyString += "&";
			//			bodyString += "AccessToken=" + AccessToken;
			//		}

			//		bodyString = "json=" + bodyString;
			//		requestUri += "?" + bodyString;

			//		// Setup the request
			//		HttpRequestMessage request = new HttpRequestMessage(method, requestUri);
			//		request.Headers.Add("Accept", "application/json");
			//		httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

			//		// set transfer-enconding 
			//		if (handler.SupportsTransferEncodingChunked())
			//		{
			//			bool chuncked = false;
			//			request.Headers.TransferEncodingChunked = chuncked;
			//			httpClient.DefaultRequestHeaders.TransferEncodingChunked = chuncked;
			//		}

			//		// await and return response of type T
			//		response = await httpClient.SendAsync(request);

			//	} else {  // POST or PUT or DELETE

			//		// Setup the request
			//		HttpRequestMessage request = new HttpRequestMessage(method, requestUri);
			//		request.Headers.Add("Accept", "application/json");
			//		httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

			//		// Add bodystring to body
			//		FormUrlEncodedContent formContent;

			//		if (requestUri.IndexOf("api/useraccess") == -1)
			//		{   // Only string access token if not an auth request
			//			formContent = new FormUrlEncodedContent(new[]
			//					   {
			//						   new KeyValuePair<string,string>("json", bodyString),
			//						   new KeyValuePair<string,string>("AccessToken", AccessToken)
			//				   });
			//		}


			//		formContent = new FormUrlEncodedContent(new[]
			//				   {
			//						   new KeyValuePair<string,string>("json", bodyString)
			//				   });

			//		// set transfer-enconding 
			//		if (handler.SupportsTransferEncodingChunked())
			//		{
			//			bool chuncked = false;
			//			request.Headers.TransferEncodingChunked = chuncked;
			//			httpClient.DefaultRequestHeaders.TransferEncodingChunked = chuncked;
			//		}

			//		// await and return response of type T
			//		response = await httpClient.PostAsync(requestUri,formContent);

			//	}

			//	var responseString = await response.Content.ReadAsStringAsync ();

			//	// http://stackoverflow.com/questions/31129873/make-http-client-synchronous-wait-for-response
			//	// calling .Result forces the call to be Synchronous and on the UI thread
			//	//				var responseString = response.Content.ReadAsStringAsync ().Result;


			//	// TEST STRING: does not parse values 'InspectionResultID' but does parse 'InspectionResultIDs'. Ones 'int' second 'string'.??
			//	//				responseString = "[{\"InspectionResultIDs\":\"1\",\"InspectionResultIDs\":\"1\",\"InspectionR"<"esultDescription\":\"Approved\",\"InspectionResultColour\":\"Lime\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultIDs\":\"2\",\"InspectionResultDescription\":\"Not Approved\",\"InspectionResultColour\":\"Tomato\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":3,\"InspectionResultDescription\":\"Not Ready\",\"InspectionResultColour\":\"Tomato\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":\"4\",\"InspectionResultDescription\":\"<not known>\",\"InspectionResultColour\":\"White\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":5,\"InspectionResultDescription\":\"Cancelled\",\"InspectionResultColour\":\"LightGray\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultIDs\":6,\"InspectionResultDescription\":\"Approved subject To\",\"InspectionResultColour\":\"LimeGreen\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":7,\"InspectionResultDescription\":\"Further works required\",\"InspectionResultColour\":\"Tomato\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":8,\"InspectionResultDescription\":\"Pending Result\",\"InspectionResultColour\":\"Magenta\",\"InspectionRsltLastChangedTimestamp\":\"2009-10-14T14:07:02.34\"}]";

			//	// The PHP send a miscellaneous "<" at the start - don't know why but strip it off.
			//	if (responseString.Length > 0 && responseString.Substring(0, 1) == "<")
			//		responseString = responseString.Substring(1);


			//	if (!response.IsSuccessStatusCode)
			//	{
			//		// Validate status codes
			//		foreach (var s in Settings.HandledHttpStatusCodes){
			//			if (response.StatusCode == s){
			//				invalidStatusAction(response);

			//				if (Settings.TestingMode)
			//					Debug.WriteLine("REST SERVICE: Success {0}", response);

			//				return;
			//			}
			//		}

			//		var ex = new Exception(String.Format("Status Code: {0}, {1}, {2}",response.StatusCode,response.ReasonPhrase, response.RequestMessage));
			//		if (Settings.TestingMode) 
			//			Debug.WriteLine("REST SERVICE: (REST03) Trapped Exception {0}", ex);

			//		errorAction (ex);
			//		return;
			//	}

			//	if (validStatusAction != null) {
			//		T toReturn;
			//		try {
			//			//toReturn = await DeserializeAsync<T>(responseString);
			//			toReturn = Deserialize<T>(responseString);
			//			//var toReturnObj = Deserialize(responseString);
			//			//toReturn = toReturnObj as T;
			//			validStatusAction(toReturn);
			//		} catch (Exception e){
			//			errorAction (e);
			//			return;
			//		}
			//		return;
			//	}

			//} catch (Exception e){

			//	if (errorAction != null) {
			//		errorAction (e);
			//		return;
			//	} else {
			//		var message = String.Format("EXCEPTION: (REST05) {0}", e);
			//		Debug.WriteLine(message);
			//		UserLog.Create(UserLogActions.Exception, message);
			//		errorAction(e);
			//	}
			//}

		}
		#endregion
		#region Private Methods
		private string SerializeBody(object body)
		{
			return "";
			//string bodyString = "";

			//var bodyType = body.GetType ().Name;
			//if (bodyType == "String" && (string)body == "")
			//	return bodyString;

			//if (Settings.RestUserJsonFormat)
			//{
			//	return JsonConvert.SerializeObject(body);
			//	//return _jsonConverter.SerializeObject (body);
			//}

			//try
			//{

			//	var firstTime = true;
			//	foreach (var _objProperty in body.GetType().GetProperties(BindingFlags.Public))
			//	{
			//		var name = _objProperty.Name;

			//		object value;
			//		if (name == "Chars")
			//		{
			//			value = body;
			//		}
			//		else {
			//			value = _objProperty.GetValue(body);
			//		}

			//		if (value == null)
			//			continue; // ignore properties with null values

			//		var typeOf = value.GetType().Name;

			//		if (typeOf == "DateTime")
			//		{
			//			var dtvalue = (DateTime)value;
			//			value = dtvalue.ToString("s");
			//		}
			//		if (typeOf == "String")
			//		{
			//			var svalue = (string)value;
			//			if (svalue == "")
			//				continue;       // ignore blank string fields
			//		}

			//		value = Uri.EscapeDataString(value.ToString());

			//		if (!firstTime)
			//		{
			//			bodyString += "&";
			//		}
			//		else {
			//			firstTime = false;
			//		}
			//		bodyString += String.Format("{0}={1}", name, value);
			//	}

			//}
			//catch (Exception e)
			//{
			//	var message = String.Format("REST SERVICE: (REST04) Exception {0}", e);
			//	Debug.WriteLine(message);
			//	UserLog.Create(UserLogActions.Exception, message);
			//	throw(e);
			//}
			//return bodyString;
		}

		private T Deserialize<T>(string responseBody)
		{
			return JsonConvert.DeserializeObject<T>(responseBody);

			//return Task.Factory.StartNew(() => _jsonConverter.DeserializeObject<T>(responseBody));
		}

		//private  Task<T> DeserializeAsync<T>(string responseBody)
		//{
		//	return Task.Factory.StartNew(() => _jsonConverter.DeserializeObject<T>(responseBody));
		//}

		#endregion
		#region Constructors
		//private readonly IMvxJsonConverter _jsonConverter;
		private readonly IReachability _reachability;
		public RestService(
			//IMvxJsonConverter jsonConverter
			IReachability reachability
		)
		{
			//_jsonConverter = Mvx.Resolve<IMvxJsonConverter> ();
			//_reachability = Mvx.Resolve<IReachability> ();
			//_jsonConverter = jsonConverter;
			_reachability = reachability;
		}
		#endregion
	}
}
