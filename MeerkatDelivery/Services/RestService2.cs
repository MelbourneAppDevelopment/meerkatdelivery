﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;

namespace MeerkatDelivery.Services
{
	public class RestResponse
	{
		public object Result = null;
		public HttpResponseMessage HttpResponseMessage = null;
	}

    public class RestService2 : IRestService2
    {
		#region Public Properties
		public string AccessToken { 
			get
			{
				return MyCookie.Cookie.AccessToken;
			}
		}
		#endregion
		#region Asynchronous 



		public async Task<RestResponse> MakeRequestAsync<T>(string requestUri, HttpMethod method, object body)
		{
			RestResponse restReponse = new RestResponse();

			try
			{
				//if (!_reachability.IsHostReachable())
				//{
					//Debug.WriteLine("RESTSERVICE: (REST01) _reachability.IsHostReachable Not Connected");
					//restReponse.HttpResponseMessage = new HttpResponseMessage();
					//restReponse.HttpResponseMessage.StatusCode = HttpStatusCode.ServiceUnavailable;
					//restReponse.HttpResponseMessage.ReasonPhrase = "Host is not reachable";
					//return restReponse;
				//}
			}
			catch (Exception e)
			{
				var message = String.Format("(REST02) Exception {0}", e);
				Debug.WriteLine(message);
				//UserLog.Create(UserLogActions.Exception, message);
				//errorAction(e);

				restReponse.HttpResponseMessage = new HttpResponseMessage();
				restReponse.HttpResponseMessage.StatusCode = HttpStatusCode.ServiceUnavailable;
				restReponse.HttpResponseMessage.ReasonPhrase = message;
				return restReponse;
			}


			try
			{
				HttpClientHandler httpClientHandler = new HttpClientHandler();
				var httpClient = new HttpClient(httpClientHandler);
				HttpRequestMessage request = new HttpRequestMessage(method, requestUri);
				HttpResponseMessage httpResponse;

				//if (requestUri.IndexOf("api/useraccess") == -1)		// Only string access token if not an auth request
				//	request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(AccessToken);

				if (method == HttpMethod.Get)
				{
					httpResponse = await httpClient.SendAsync(request);

				} else {  // POST or PUT or DELETE

					// Create the REST request details
					var jsonSettings = new JsonSerializerSettings
					{
						NullValueHandling = NullValueHandling.Ignore,
						MissingMemberHandling = MissingMemberHandling.Ignore        
					};
					
					var a = JsonConvert.SerializeObject(body, jsonSettings);
					FormUrlEncodedContent formContent = new FormUrlEncodedContent(new[]
				   {
						new KeyValuePair<string, string>("data", JsonConvert.SerializeObject(body, jsonSettings)),
						new KeyValuePair<string, string>("userId", MyCookie.Cookie.UserId.ToString())
				   });

					request.Headers.Add("Accept", "application/json");
					httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

					switch (method.ToString())
					{
						case "POST":
							httpResponse = await httpClient.PostAsync(requestUri, formContent);
							break;
						case "PUT":
							httpResponse = await httpClient.PutAsync(requestUri, formContent);
							break;
						case "DELETE":
							httpResponse = await httpClient.DeleteAsync(requestUri);
							break;
						default:
							throw new Exception("Invalid httpMethod");
					}
				}

				if (!httpResponse.IsSuccessStatusCode)
				{
					restReponse.HttpResponseMessage = httpResponse;
					return restReponse;
				}

				var responseString = await httpResponse.Content.ReadAsStringAsync ();

				// TEST STRING: does not parse values 'InspectionResultID' but does parse 'InspectionResultIDs'. Ones 'int' second 'string'.??
				//				responseString = "[{\"InspectionResultIDs\":\"1\",\"InspectionResultIDs\":\"1\",\"InspectionR"<"esultDescription\":\"Approved\",\"InspectionResultColour\":\"Lime\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultIDs\":\"2\",\"InspectionResultDescription\":\"Not Approved\",\"InspectionResultColour\":\"Tomato\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":3,\"InspectionResultDescription\":\"Not Ready\",\"InspectionResultColour\":\"Tomato\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":\"4\",\"InspectionResultDescription\":\"<not known>\",\"InspectionResultColour\":\"White\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":5,\"InspectionResultDescription\":\"Cancelled\",\"InspectionResultColour\":\"LightGray\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultIDs\":6,\"InspectionResultDescription\":\"Approved subject To\",\"InspectionResultColour\":\"LimeGreen\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":7,\"InspectionResultDescription\":\"Further works required\",\"InspectionResultColour\":\"Tomato\",\"InspectionRsltLastChangedTimestamp\":\"2008-06-15T17:24:29.607\"},{\"InspectionResultID\":8,\"InspectionResultDescription\":\"Pending Result\",\"InspectionResultColour\":\"Magenta\",\"InspectionRsltLastChangedTimestamp\":\"2009-10-14T14:07:02.34\"}]";

				if (responseString == "")
				{
					restReponse.HttpResponseMessage = new HttpResponseMessage();
					restReponse.HttpResponseMessage.StatusCode = HttpStatusCode.NoContent;
					restReponse.HttpResponseMessage.ReasonPhrase = "Empty content returned from the server";
					return restReponse;
				}
				else
				{
					T toReturn = default(T);
					try
					{
						//responseString="[{\"Id\":10,\"OrderSystem\":\"order system\",\"OrderNumber\":\"1928s\",\"ClientId\":2,\"Deleted\":0,\"OrderSystemClientId\":\"N1234\",\"SenderName\":\"sender name\",\"SenderCompanyName\":\"sender company name\",\"SenderAddress\":\"sender address\",\"SenderSuburb\":\"sender suburb\",\"SenderState\":\"sender state\",\"SenderZip\":\"sender zip\",\"SenderPhone\":\"sender phone\",\"SenderEmail\":\"sender email\",\"RecipientName\":\"recipient name\",\"RecipientCompanyName\":\"recipient company name\",\"RecipientStreetAddress\":\"600B Geelong Rd\",\"RecipientSuburb\":\"Camberwell\",\"RecipientState\":\"recipient state\",\"RecipientZip\":\"recipient zip\",\"RecipientPhone\":\"recipient phone\",\"RecipientEmail\":\"recipient email\",\"ContactPhone\":\"contact phone\",\"DeliveryDate\":1493107815,\"DeliveryTime\":1493107815,\"DeliveryTimeCode\":\"Anytime\",\"SpecialInstructions\":\"Leave behind chimney\",\"Priority\":\"High\",\"Product1Code\":\"product1 code\",\"Product1Description\":\"Bouquet Nbr 17, 20 long stem roses, with fruit basket and chocolate box 17\",\"Product2Code\":\"product2 code\",\"Product2Description\":\"product2 description\",\"Product3Code\":\"product3 code\",\"Product3Description\":\"product3 description\",\"Notes\":\"These are our notes\",\"Amount\":\"1.00\",\"CardText\":\"card text\",\"ClearingNumber\":3,\"GpsLatitude\":15,\"GpsLongitude\":16,\"DeliverySequence\":11,\"Image1\":null,\"Image2\":null,\"PickingUserId\":27,\"PickingTime\":1493107815,\"PickedTime\":1493107815,\"DeliveryUserId\":14,\"LoadedTime\":1493107815,\"DeliveringTime\":null,\"DeliveringGpsLatitude\":null,\"DeliveringGpsLongitude\":null,\"DeliveredImage\":null,\"DeliveredTime\":1493107815,\"DeliveredGpsLatitude\":6,\"DeliveredGpsLongitude\":7,\"Status\":\"Ordered\",\"DeviceTimeZone\":null,\"DeviceCreateTimestamp\":0,\"DeviceUpdateTimestamp\":0,\"CreateUserId\":0,\"UpdateUserId\":0,\"UpdateTimestamp\":1493104215}]";
						//responseString="[{\"Id\":10,\"OrderSystem\":\"order system\",\"OrderNumber\":\"1928s\",\"ClientId\":2,\"Deleted\":0,\"OrderSystemClientId\":\"N1234\",\"SenderName\":\"sender name\",\"SenderCompanyName\":\"sender company name\",\"SenderAddress\":\"sender address\",\"SenderSuburb\":\"sender suburb\",\"SenderState\":\"sender state\",\"SenderZip\":\"sender zip\",\"SenderPhone\":\"sender phone\",\"SenderEmail\":\"sender email\",\"RecipientName\":\"recipient name\",\"RecipientCompanyName\":\"recipient company name\",\"RecipientStreetAddress\":\"600B Geelong Rd\",\"RecipientSuburb\":\"Camberwell\",\"RecipientState\":\"recipient state\",\"RecipientZip\":\"recipient zip\",\"RecipientPhone\":\"recipient phone\",\"RecipientEmail\":\"recipient email\",\"ContactPhone\":\"contact phone\",\"DeliveryDate\":1493107815,\"DeliveryTime\":1493107815,\"DeliveryTimeCode\":\"Anytime\",\"SpecialInstructions\":\"Leave behind chimney\",\"Priority\":\"High\",\"Product1Code\":\"product1 code\",\"Product1Description\":\"Bouquet Nbr 17, 20 long stem roses, with fruit basket and chocolate box 17\",\"Product2Code\":\"product2 code\",\"Product2Description\":\"product2 description\",\"Product3Code\":\"product3 code\",\"Product3Description\":\"product3 description\",\"Notes\":\"These are our notes\",\"Amount\":\"1.00\",\"CardText\":\"card text\",\"ClearingNumber\":3,\"GpsLatitude\":15,\"GpsLongitude\":16,\"DeliverySequence\":11,\"PickingUserId\":27,\"PickingTime\":1493107815,\"PickedTime\":1493107815,\"DeliveryUserId\":14,\"LoadedTime\":1493107815,\"DeliveredTime\":1493107815,\"DeliveredGpsLatitude\":6,\"DeliveredGpsLongitude\":7,\"Status\":\"Ordered\",\"DeviceCreateTimestamp\":0,\"DeviceUpdateTimestamp\":0,\"CreateUserId\":0,\"UpdateUserId\":0,\"UpdateTimestamp\":1493104215}]";
						//List<Database.Order> result = JsonConvert.DeserializeObject<List<Database.Order>>(responseString);

						toReturn = JsonConvert.DeserializeObject<T>(responseString);

					}
					catch (Exception e)
					{
						var typeOfT = toReturn.GetType().Name;

						var message = String.Format("Failed to deserialize [{0}]: {1}", typeOfT, e);
						Debug.WriteLine(message);
						//UserLog.Create(UserLogActions.Exception, message);
						//errorAction(e);

						restReponse.HttpResponseMessage = new HttpResponseMessage();
						restReponse.HttpResponseMessage.StatusCode = HttpStatusCode.PartialContent;
						restReponse.HttpResponseMessage.ReasonPhrase = message;
						return restReponse;
					}
					restReponse.Result = (object)toReturn;
					if (restReponse.HttpResponseMessage == null)
					{
						restReponse.HttpResponseMessage = new HttpResponseMessage();
						restReponse.HttpResponseMessage.StatusCode = HttpStatusCode.OK;
					}
					return restReponse;
				}

			} catch (Exception e){

				var message = String.Format("Http request failed: {0}", e);
				Debug.WriteLine(message);
				//UserLog.Create(UserLogActions.Exception, message);
				//errorAction(e);

				restReponse.HttpResponseMessage = new HttpResponseMessage();
				restReponse.HttpResponseMessage.StatusCode = HttpStatusCode.BadRequest;
				restReponse.HttpResponseMessage.ReasonPhrase = message;
				return restReponse;
			}

		}
		#endregion
		#region Private Methods
		#endregion
		#region Constructors
		private readonly IReachability _reachability;
		public RestService2(
			//IReachability reachability
		)
		{
			//_reachability = reachability;
		}
		#endregion
	}
}
