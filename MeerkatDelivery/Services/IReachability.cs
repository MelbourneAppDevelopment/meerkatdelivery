﻿namespace MeerkatDelivery.Services
{
	public enum NetworkStatus
	{
		Unknown,
		Disconnected,
		ConnectedViaWifi,
		ConnectedViaMobile
	}

	public interface IReachability
	{
		bool IsHostReachable(string hostName = "www.google.com");
		NetworkStatus RemoteHostStatus ();
		NetworkStatus InternetConnectionStatus ();
		NetworkStatus LocalWifiConnectionStatus ();
		string GetIPAddress();
	}
}

