﻿using System;
namespace MeerkatDelivery.Services
{
	public interface IDeviceService
	{
		string Manufacturer { get; }
		string Model { get; }
		string DeviceOrSimulator { get; }
		string Serial { get; }
	}
}

