﻿using System;
using System.Threading.Tasks;
using System.Net.Http;

namespace MeerkatDelivery.Services
{
    public interface IRestService
    {
		string AccessToken { get; set; }
		string RedirectUrl { get; set; }

		Task MakeRequestAsync<T>(
			string requestUri, 
			HttpMethod method, 
			object body, 
			Action<T> validStatusAction, 
			Action<HttpResponseMessage> 
			invalidStatusAction, 
			Action<Exception> errorAction
		);
    }
}