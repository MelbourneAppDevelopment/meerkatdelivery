﻿using System;
using System.Collections.Generic;
using MeerkatDelivery.ViewModels;
using Xamarin.Forms;

namespace MeerkatDelivery.Views
{
	public partial class OrderPage : ContentPage
	{
		private OrderPageViewModel ViewModel;
		public OrderPage()
		{
			InitializeComponent();
			ViewModel = (OrderPageViewModel)BindingContext;

			ToolbarItems.Add(new ToolbarItem("Edit", "edit.png", () =>
			{
				ViewModel.EditCommand.Execute();
			}));
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			MessagingCenter.Subscribe<OrderPageViewModel, string[]>(this, "OrderPageViewModel", (sender, values) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
					  {
						  await DisplayAlert(values[0], values[1], values[2]);
					  });
			});

			MessagingCenter.Subscribe<OrderPageViewModel, string[]>(this, "DisplayErrorAndExit", (sender, values) =>
			{
				Device.BeginInvokeOnMainThread(async () =>
							 {
								 MessagingCenter.Unsubscribe<OrderPageViewModel, string[]>(this, "DisplayErrorAndExit");
								 await DisplayAlert(values[0], values[1], values[2]);
								 if (this.Navigation.NavigationStack.Count != 0)
								 {
									 await this.Navigation.PopAsync();
								 }
							 });
			});

		}
	
		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			MessagingCenter.Unsubscribe<OrderPageViewModel, string[]>(this, "OrderPageViewModel");
			 MessagingCenter.Unsubscribe<OrderPageViewModel, string[]>(this, "DisplayErrorAndExit");
		}

	}
}
