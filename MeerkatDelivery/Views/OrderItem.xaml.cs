﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using MeerkatDelivery;
using MeerkatDelivery.ViewModels;
using Xamarin.Forms;

namespace MeerkatDelivery.Views
{
	public partial class OrderItem : ViewCell
	{
		public OrderItem()
		{
			InitializeComponent();

			Debug.WriteLine("Selected Picker = '{0}'", PickerL.SelectedIndex);


			//MessagingCenter.Subscribe<OrderItemViewModel, Tuple<string[], Action>>(this, "OrderItemDeleteOrder", (sender, tuple) =>
			//{
			//	Device.BeginInvokeOnMainThread(async () =>
			//		  {
			//			  var values = tuple.Item1;
			//			  var action = tuple.Item2;

			//			  if (await DisplayAlert(values[0], values[1], values[2], values[3]))
			//			  {
			//				  try
			//				  {
			//					  action.Invoke();
			//				  }
			//				  catch (Exception e)
			//				  {
			//					  await DisplayAlert(values[0], "Failed to delete order", values[2]);
			//				  }
			//			  }
			//		  });		
			//});

		}

		public void OnEdit(object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);
			//await((MainPageViewModel)this.BindingContext).EditPumpCommand.Execute((AuthorisedPump)mi.CommandParameter);
			((OrderItemViewModel)this.BindingContext).EditCommand.Execute((OrderItemViewModel)mi.CommandParameter);
		}

		public void OnDelete(object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);

			//await((MainPageViewModel)this.BindingContext).DeletePumpCommand.Execute((AuthorisedPump)mi.CommandParameter);
			((OrderItemViewModel)this.BindingContext).DeleteCommand.Execute((OrderItemViewModel)mi.CommandParameter);	}


			}
}
