﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace MeerkatDelivery.Views
{
	public partial class TestPage : ContentPage
	{
		double combinedHeight, spacing;
		public TestPage()
		{
			InitializeComponent();
			TopStack.SizeChanged += OnSizeChanged;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			combinedHeight = TopStack.Height + BottomStack.Height;

			spacing = TopStack.Margin.Bottom + BottomStack.Margin.Top;
			if (BottomStack.Parent.GetType().Name == "StackLayout")
			{
				var parent = (StackLayout)BottomStack.Parent;
				spacing =+ parent.Spacing;
			}
		}

		void OnSizeChanged(object sender, EventArgs e)
		{
			if (combinedHeight > 0)
			{
				BottomStack.LayoutTo(new Rectangle(BottomStack.X, TopStack.Y + TopStack.Height + spacing, BottomStack.Width, combinedHeight - TopStack.Height), 150, Easing.Linear);
			}
		}
	}
}
