﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using MeerkatDelivery.BusinessObjects;
using MeerkatDelivery.CustomControls;
using MeerkatDelivery.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace MeerkatDelivery.Views
{
	public partial class DeliveriesPage : ContentPage
	{
		private double xOffset, yOffset;
		private double listViewOriginalHeight = -1;

		private DeliveriesPageViewModel ViewModel;
		public DeliveriesPage()
		{
			InitializeComponent();
			ViewModel = (DeliveriesPageViewModel)BindingContext;

			ToolbarItems.Add(new ToolbarItem("Route", "CreateRoute.png",  () =>
			{
				ViewModel.CreateRouteCommand.Execute();
			}));


			OrdersListView.ItemAppearing += OnItemAppearing;
			OrdersListView.ItemDisappearing += OnItemDisappearing;

			ViewModel.Map = DeliveryMap;
		}

		public void OnDelete(object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);
		}

		// Handle the panning of the list view up and down
		void OnPanUpdated(object sender, PanUpdatedEventArgs e)
		{
			var handle = (StackLayout)sender;
			switch (e.StatusType)
			{
				case GestureStatus.Started:
					handle.AnchorX = 0;
					handle.AnchorY = 0;
					break;

				case GestureStatus.Running:
					// 30 is 30 pixels from the bottom, 0 is stopping sliding up past original top point.
					handle.TranslationY = Math.Max(Math.Min(handle.Height - 30, yOffset + e.TotalY), 0);
					break;

				case GestureStatus.Completed:
					yOffset = handle.TranslationY;
					OrdersListView.HeightRequest = listViewOriginalHeight - yOffset;
					break;
			}
		}


		public void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e.Item == null)
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null

			ViewModel.OnItemTappedCommand.Execute((DeliveryItemViewModel)e.Item);
			((ListView)sender).SelectedItem = null;
		}


		private int lastItemAtTop = -1;
		private int _indexOfItemAtTop = 0;
		private int IndexOfItemAtTop
		{
			get
			{
				return _indexOfItemAtTop;
			}
			set
			{
				if (value < 0)
					_indexOfItemAtTop = 0;
				else if (value > ViewModel.Orders.Count - 1)
					_indexOfItemAtTop = ViewModel.Orders.Count - 1;
				else
					_indexOfItemAtTop = value;
			}
		}


		private List<int> OnScreen = new List<int>();
		private string lastWas = "";
		private int lastItemCount = -1;

		private DeliveryItemViewModel ItemAppearing;
		/// <summary>
		/// On the item appearing reset the order that is at the top of the page.
		/// </summary>
		void OnItemAppearing(object sender, ItemVisibilityEventArgs e)
		{
			return;

			// ItemDisappearing is not reliable in the case where PAN has changed the listview size (Forms still thinks its the old size)
			// But if the OnAppearing item is above  (Less than) the current AtTop then Scrolling down and AtTop is -1
			// If OnAppearing is greater than then scrolling up and AtTop +1
			// If OnDisappearing is less than or equal to AtTop scrolling up and AtTop +1
			// 
			// So trick is to get the first AtTop in the case of items being added to the screen rather than scrolling
			// We use the LastIs field - relying on appearing and disappearing alternating (UnevenRows = false) 

			ItemAppearing = (DeliveryItemViewModel)e.Item;
			var appearingIndex = ViewModel.Orders.IndexOf(ItemAppearing);

			var message = String.Format("---- Appearing    {0} [{1}]    ", ItemAppearing.OrderNumber, appearingIndex);

			if (OnScreen.IndexOf(appearingIndex) == -1)
				OnScreen.Add(appearingIndex);

			lastItemAtTop = IndexOfItemAtTop;

			//TODO this doesn't take into account an insert that doesn't cause an "OnAppearing" and was after a previous "OnAppearing"
			if (lastItemCount != ViewModel.Orders.Count && lastWas != "disappearing")
				lastWas = "insertNewItem";
			lastItemCount = ViewModel.Orders.Count;

			switch (lastWas)
			{
				case "":                // starting
					IndexOfItemAtTop = appearingIndex;
					break;

				case "insertNewItem":	// new row added to screen - AtTop hasnt changed (assume added under the top)
					if (IndexOfItemAtTop == -1)
						IndexOfItemAtTop = 0;
					break;

				case "appearing":       // two appearings in a row and count hasn't change - this is the first appearing after an insert
					if (IndexOfItemAtTop == -1)
						IndexOfItemAtTop = 0;
					break;

				case "disappearing":
					if (appearingIndex < IndexOfItemAtTop)
						IndexOfItemAtTop--;
					//else if (appearingIndex > IndexOfItemAtTop)
					//	IndexOfItemAtTop++;
					break;
			}
			lastWas = "appearing";

			message = message + String.Format("TOP = {0}|  ", IndexOfItemAtTop);
			foreach (var a in OnScreen)
			{
				message = message + String.Format("{0}, ", a);
			}
			Debug.WriteLine(message);

			this.ItemAppeared(ViewModel.Orders[IndexOfItemAtTop]);
		}

		private DeliveryItemViewModel ItemDisappearing;
		/// <summary>
		/// On the item disappearing reset the order that is at the top of the page.
		/// </summary>
		void OnItemDisappearing(object sender, ItemVisibilityEventArgs e)
		{
			return;

			ItemDisappearing = (DeliveryItemViewModel)e.Item;
			var disappearingIndex = ViewModel.Orders.IndexOf(ItemDisappearing);
			var message = String.Format("---- Disappearing {0} [{1}]", ItemDisappearing.OrderNumber, disappearingIndex);

			OnScreen.Remove(disappearingIndex);

			lastItemAtTop = IndexOfItemAtTop;

			switch (lastWas)
			{
				case "":                // starting
					break;

				case "insertNewItem":	// first disappearing after an insert
				case "appearing":
					if (disappearingIndex <= IndexOfItemAtTop)
						IndexOfItemAtTop++;
					else if (IndexOfItemAtTop == 0)
						IndexOfItemAtTop++;
					break;
			}
			lastWas = "disappearing";

			message = message + String.Format("TOP = {0}|  ", IndexOfItemAtTop);
			foreach (var a in OnScreen)
			{
				message = message + String.Format("{0}, ", a);
			}
			Debug.WriteLine(message);

			this.ItemAppeared(ViewModel.Orders[IndexOfItemAtTop]);
		}
	
		private DeliveryItemViewModel lastItemAppeared = null;
		private void ItemAppeared(DeliveryItemViewModel item)
		{
			if (lastItemAtTop == IndexOfItemAtTop)
				return;

			Debug.WriteLine("                               ItemAppeared this:{0} last:{1}", IndexOfItemAtTop, lastItemAtTop);

			ViewModel.SetFocus(IndexOfItemAtTop);
			//item.StatusBackgroundColor = "Blue";
			//if (lastItemAppeared != null)
			//	lastItemAppeared.StatusBackgroundColor = "White";	//TODO make map pin chang		
			//lastItemAppeared = item;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.OnAppearing();

			if (listViewOriginalHeight == -1)
				listViewOriginalHeight = OrdersListView.Height;

			OnScreen.Clear();
			lastWas = "";
			IndexOfItemAtTop = -1;
			if ( ViewModel.Orders.Count > 0)
				IndexOfItemAtTop = 1;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			ViewModel.OnDisappearing();
		}

	}
}
