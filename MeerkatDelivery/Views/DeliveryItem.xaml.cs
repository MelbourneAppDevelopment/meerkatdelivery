﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using MeerkatDelivery.ViewModels;
using Xamarin.Forms;

namespace MeerkatDelivery.Views
{
	public partial class DeliveryItem : ViewCell
	{
		DeliveryItemViewModel ViewModel;

		
		public DeliveryItem()
		{
			InitializeComponent();
			ViewModel = (DeliveryItemViewModel)BindingContext;
		}
	}
}
