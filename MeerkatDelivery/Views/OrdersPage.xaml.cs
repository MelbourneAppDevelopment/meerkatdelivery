﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using MeerkatDelivery.BusinessObjects;
using MeerkatDelivery.ViewModels;
using Xamarin.Forms;

namespace MeerkatDelivery.Views
{
	public partial class OrdersPage : ContentPage
	{
		private OrdersPageViewModel ViewModel;
		public OrdersPage()
		{
			InitializeComponent();
			ViewModel = (OrdersPageViewModel)BindingContext;


			ToolbarItems.Add(new ToolbarItem("Add", "add.png", async () =>
			{
				ViewModel.AddCommand.Execute();
			}));
		}

		public void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e.Item == null)
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null

			ViewModel.OnItemTappedCommand.Execute((OrderItemViewModel)e.Item);
			((ListView)sender).SelectedItem = null;	
		}

		//public void OnEdit(object sender, EventArgs e)
		//{
		//	var mi = ((MenuItem)sender);
		//	ViewModel.EditCommand.Execute((OrderItemViewModel)mi.CommandParameter);
		//}

		public void OnDelete(object sender, EventArgs e)
		{
			var mi = ((MenuItem)sender);
			ViewModel.DeleteCommand.Execute((OrderItemViewModel)mi.CommandParameter);
		}


		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.OnAppearing();
			//MessagingCenter.Subscribe<OrderItemViewModel, Tuple<string[], OrderItemViewModel>>(this, "OrderItemDeleteOrder", (sender, tuple) =>
			//{
			//	Device.BeginInvokeOnMainThread(async () =>
			//		  {
			//			  var values = tuple.Item1;
			//			  var ordervm = tuple.Item2;

			//			  if (await DisplayAlert(values[0], values[1], values[2], values[3]))
			//			  {
			//				  ViewModel.DeleteOrder(ordervm);
			//			  }
			//		  });
			//});
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			ViewModel.OnDisappearing();

			MessagingCenter.Unsubscribe<OrderItemViewModel, Tuple<string[], Action>>(this, "OrderItemDeleteOrder");
		}

	}
}
