using System;
using System.Diagnostics;
using MeerkatDelivery.BusinessObjects;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.ViewModels;
using Xamarin.Forms;

namespace MeerkatDelivery.Views
{
	public partial class MainPage : ContentPage
	{
		private MainPageViewModel ViewModel;


		public MainPage()
		{
			//NavigationPage.SetHasNavigationBar(this, false);

			InitializeComponent();
			ViewModel = (MainPageViewModel)BindingContext;

			MessagingCenter.Subscribe<MainPageViewModel, string[]>(this, "MainPageDisplayAlert", async (sender, values) =>
			{
			   await DisplayAlert(values[0], values[1], values[2]);
			});

		}

		/// <summary>
		/// Simple way of sending ListView OnTap event to viewmodel
		/// </summary>
		public void OnItemTapped(object sender, ItemTappedEventArgs e)
		{
			if (e.Item == null)
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null

			//((MainPageViewModel)this.BindingContext).OnItemSelectedCommand.Execute((-put selected object here-)e.Item);

			((ListView)sender).SelectedItem = null;

		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.OnAppearing(); 		// Until Prism iNavigateAware works for android back button and navbar Back
			HockeyApp.MetricsManager.TrackEvent("MainPage Appearing");
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
			ViewModel.OnDisappearing(); 	// Until Prism iNavigateAware works for android back button and navbar Back
			HockeyApp.MetricsManager.TrackEvent("MainPage Disappearing");
		}

	}
}
