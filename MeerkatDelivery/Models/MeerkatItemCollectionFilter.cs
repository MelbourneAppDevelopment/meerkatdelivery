﻿using System;
namespace MeerkatDelivery
{
	public class MeerkatItemCollectionFilter
	{
		public DateRange OrderDateRange = new DateRange(DateTime.Today, DateTime.Today);
		public DateRange DeliveryDateRange = new DateRange(DateTime.Today, DateTime.Today);
		public int? PickById = null;
		public int? DeliverById = null;
		public OrderStatusRange OrderStatusRange = new OrderStatusRange();

	}
}
