﻿using System;
namespace MeerkatDelivery
{
	public enum OrderStatus
	{
		// WARNING: Change these, also change in constructor of FilterPageViewModel
		Initialised, 
		Ordered, 
		Cancelled, 
		Picked, 
		Loaded, 
		Delivering, 
		Delivered, 
		FailedDelivery
	}
}
