﻿using System;
namespace MeerkatDelivery.BusinessObjects
{
	public enum UserType
	{
		admin, picker, courier, unknown
	}
}
