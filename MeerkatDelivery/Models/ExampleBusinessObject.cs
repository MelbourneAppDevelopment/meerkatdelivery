﻿//using System;
//using System.Collections.Generic;
//using MeerkatDelivery.BusinessObjectServices;
//using Microsoft.Practices.Unity;
//using System.Threading.Tasks;

//namespace MeerkatDelivery.BusinessObjects
//{

//	#region EventArgs
//	public class ExampleBusinessObjectStateChangedEventArgs : EventArgs
//	{
//		public string State { get; set; }
//		public ExampleBusinessObjectStateChangedEventArgs(string state)
//		{
//			State = state;
//		}
//	}
//	#endregion

//	public class ExampleBusinessObject : BaseBusinessObject
//	{
//		#region Constants
//		#endregion
//		#region private Properties
//		#endregion

//		#region Properties
//		public string InstanceId { get; set; } // Only for testing

//		private int _id;
//		public int Id
//		{
//			get
//			{
//				return _id;
//			}
//			set
//			{
//				if (_id != value)
//					_id = value;
//			}
//		}
//		private string _deviceName = "Example device name";
//		public string DeviceName
//		{
//			get
//			{
//				return _deviceName;
//			}
//			set
//			{
//				if (_deviceName != value)
//					_deviceName = value;
//			}
//		}
//		private ExampleEnum _exampleEnum;
//		public ExampleEnum ExampleEnum
//		{
//			get
//			{
//				return _exampleEnum;
//			}
//			set
//			{
//				if (_exampleEnum != value)
//					_exampleEnum = value;
//			}
//		}

//		#endregion
//		#region Events
//		public event EventHandler<ExampleBusinessObjectStateChangedEventArgs> OnStateChanged;

//		#endregion
//		#region Methods
//		#endregion
//		#region DatabaseMethods
//		public async Task SaveAsync()
//		{
//			var dbObj = new Database.ExampleBusinessObject();
//			this.SetFields(ref dbObj);
//			if (dbObj.Id != 0)
//				await _dataService.UpdateAsync(dbObj);
//			else
//				await _dataService.InsertAsync(dbObj);
//		}

//		public void Save()
//		{
//			var dbObj = new Database.ExampleBusinessObject();
//			this.SetFields(ref dbObj);
//			if (dbObj.Id != 0)
//				_dataService.Update(dbObj);
//			else
//				_dataService.Insert(dbObj);
//		}

//		private void SetFields(ref Database.ExampleBusinessObject dbObj)
//		{

//			dbObj.Id = (int)this.Id;
//			dbObj.DeviceName = this.DeviceName;
//			dbObj.ExampleEnum = this.ExampleEnum.ToString();

//			base.SetFields((Database.BaseDatabaseObject)dbObj);
//		}

//		public object LoadFromDB(object databaseRecord, bool getChildren = false)
//		{
//			var dbObj = databaseRecord as Database.ExampleBusinessObject;

//			this.Id = (int)dbObj.Id;
//			try
//			{
//				this.ExampleEnum = (ExampleEnum)Enum.Parse(typeof(ExampleEnum), dbObj.ExampleEnum);
//			}
//			catch
//			{
//				throw new Exception("Hint: invalid DB Type");
//			}
//			this.DeviceName = dbObj.DeviceName;

//			if (getChildren)
//			{
//				// Add consequential childred processing
//			}
//			base.LoadFromDB(dbObj, getChildren);

//			return this;
//		}

//		#endregion
//		#region Constructors
//		private readonly IExampleServices _exampleServices;
//		static private MeerkatDelivery.Database.IDataService _dataService;
//		public ExampleBusinessObject() : base()
//		{
//			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

//			_exampleServices = Settings.Container.Resolve<IExampleServices>();
//			_dataService = Settings.Container.Resolve<Database.IDataService>();

//		}

//		public ExampleBusinessObject(int id) : base()
//		{
//			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

//			_exampleServices = Settings.Container.Resolve<IExampleServices>();
//			_dataService = Settings.Container.Resolve<Database.IDataService>();

//			var dbObj = _dataService.GetExampleBusinessObject(id);
//			if (dbObj != null)
//				this.LoadFromDB(dbObj);
//		}

//		public ExampleBusinessObject(Database.ExampleBusinessObject dbObj) : base()
//		{
//			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

//			_exampleServices = Settings.Container.Resolve<IExampleServices>();
//			_dataService = Settings.Container.Resolve<Database.IDataService>();


//			this.LoadFromDB(dbObj);
//		}

//		#endregion
//		#region IDisposable Support
//		private bool disposedValue = false; // To detect redundant calls

//		protected virtual void Dispose(bool disposing)
//		{
//			if (!disposedValue)
//			{
//				if (disposing)
//				{
//					// TODO: dispose managed state (managed objects).
//				}

//				//TODO: unmanaged objects

//				disposedValue = true;
//			}
//		}

//		// override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
//		 ~ExampleBusinessObject() {
//		   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.

//		   Dispose(false);
//		 }

//		// This code added to correctly implement the disposable pattern.
//		public void Dispose()
//		{
//			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
//			Dispose(true);
//			// uncomment the following line if the finalizer is overridden above.
//			 GC.SuppressFinalize(this);
//		}

//		#endregion
//	}
//}
