﻿using System;
namespace MeerkatDelivery.BusinessObjects
{
	public enum DeliveryTime
	{
		Morning,
		Afternoon,
		AfterWork,
		Anytime,
		Midday
	}
}
