﻿using System;
using System.Reflection;

namespace MeerkatDelivery.BusinessObjects
{
	public abstract class BaseBusinessObject : IDisposable
	{
		#region Properties
		public string DeviceTimeZone { get; set; }
		public DateTime DeviceCreateTimestamp { get; set; }
		public DateTime DeviceUpdateTimestamp { get; set; }
		public int CreateUserId { get; set; }
		public int UpdateUserId { get; set; }
		public DateTime UpdateTimestamp { get; set; } // Ask the server for changes after this time.

		private bool _changed = false;
		public bool Changed             // The UI sets this to false and uses it to trigger UI updates
		{
			get { return _changed; }
			set
			{
				if (_changed != value)
				{
					_changed = value;
				}
			}
		}

		private object _databaseObj = null;
		public object DatabaseObj
		{
			get { return _databaseObj; }
			set
			{
				if (_databaseObj != value)
				{
					_databaseObj = value;
				}
			}
		}

		public BaseBusinessObject()
		{
			//_dataService = Mvx.Resolve<ClimatarianChallenge.Core.Database.IDataService> ();
		}
		#endregion
		#region Methods
		protected void SetFields(Database.BaseDatabaseObject databaseObj)
		{
			databaseObj.DeviceTimeZone = this.DeviceTimeZone;
			databaseObj.DeviceCreateTimestamp = this.DeviceCreateTimestamp;
			databaseObj.DeviceUpdateTimestamp = DateTime.Now;
			databaseObj.CreateUserId = this.CreateUserId;
			databaseObj.UpdateUserId = this.UpdateUserId;
			databaseObj.UpdateTimestamp = this.UpdateTimestamp;

		}
		protected void LoadFromDB(object databaseObj, bool getChildren = true)
		{
			var dbObj = databaseObj as Database.BaseDatabaseObject;
			this.DeviceTimeZone = dbObj.DeviceTimeZone;
			this.DeviceCreateTimestamp = dbObj.DeviceCreateTimestamp;
			this.DeviceUpdateTimestamp = dbObj.DeviceUpdateTimestamp;
			this.CreateUserId = dbObj.CreateUserId;
			this.UpdateUserId = dbObj.UpdateUserId;
			this.UpdateTimestamp = dbObj.UpdateTimestamp;
		}

		public void Copy(object copyFrom)
		{
			var fromProperties = copyFrom.GetType().GetRuntimeProperties();
			foreach (var fromProperty in fromProperties)
			{
				var name = fromProperty.Name;
				var value = fromProperty.GetValue(copyFrom, null);
				var toProperty = this.GetType().GetRuntimeProperty(name);

				toProperty.SetValue(this, value);
			}
		}
		#endregion
		#region IDisposable implementation
		private bool _disposed;

		~BaseBusinessObject()
		{
			Dispose(disposing: false);
		}

		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			//			Debug.WriteLine ("Disposing {0}: (disposing: {1}) {3}",this.GetType().ToString(),disposing, this.ToString());

			if (!_disposed)
			{
				_disposed = true;

				DisposeUnmanagedResource();

				if (disposing)
				{
					DisposeOtherManagedResource();
				}
			}
		}

		protected virtual void DisposeUnmanagedResource()
		{
		}
		protected virtual void DisposeOtherManagedResource()
		{
		}

		#endregion
	}

}

