﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using MeerkatDelivery.Services;
using Microsoft.Practices.Unity;
using MeerkatDelivery.Database;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Net.Http;
using Xamarin.Forms.Maps;
using MeerkatDelivery.Helpers;

namespace MeerkatDelivery.BusinessObjects
{
	public class User : BaseBusinessObject
	{
		#region Properties
		// --- NOT GENERATED ---
		public string InstanceId { get; set; } // Only for testing


		// --- GENERATED ---

		public int Id { get; set; }
		public UserType Type { get; set; }        // admin, picker, courier, unknown
		public int ClientId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string ProposedPassword { get; set; }
		public string AccessToken { get; set; }
		public string FacebookId { get; set; }
		public string FacebookToken { get; set; }
		public string FacebookDetailsJson { get; set; }
		public string Token { get; set; }
		public UserStatus Status { get; set; }        // pending, ok, loggedin
		public string Manufacturer { get; set; }
		public string Model { get; set; }
		public string Serial { get; set; }
		public string AppVersion { get; set; }



		#endregion
		#region Private properties
		private MyCookie _cookie = null;
		private MyCookie Cookie
		{
			get
			{
				if (_cookie == null)
					_cookie = MyCookie.Cookie;
				return _cookie;
			}
		}

		#endregion
		#region General Methods
		public override string ToString()
		{
			return String.Format("Id {0}|Type {1}|ClientId {2}|FirstName {3}|LastName {4}|Email {5}|Password {6}|ProposedPassword {7}|AccessToken {8}|FacebookId {9}|FacebookToken {10}|FacebookDetailsJson {11}|Token {12}|Status {13}|Manufacturer {14}|Model {15}|Serial {16}|AppVersion {17}",

				this.Id,
				this.Type,
				this.ClientId,
				this.FirstName,
				this.LastName,
				this.Email,
				this.Password,
				this.ProposedPassword,
				this.AccessToken,
				this.FacebookId,
				this.FacebookToken,
				this.FacebookDetailsJson,
				this.Token,
				this.Status,
				this.Manufacturer,
				this.Model,
				this.Serial,
				this.AppVersion);
		}
		#endregion
		#region DatabaseMethods
		public async Task SaveAsync()
		{
			var dbObj = new Database.User();
			this.SetFields(ref dbObj);
			if (dbObj.Id != 0)
				await _dataService.UpdateAsync(dbObj);
			else
				await _dataService.InsertAsync(dbObj);
		}

		public void Save()
		{
			var dbObj = new Database.User();
			this.SetFields(ref dbObj);
			if (dbObj.Id != 0)
				_dataService.Update(dbObj);
			else
				_dataService.Insert(dbObj);
		}

		private void SetFields(ref Database.User dbObj)
		{

			dbObj.Id = (int)this.Id;
			dbObj.Type = this.Type.ToString();
			dbObj.ClientId = this.ClientId;
			dbObj.FirstName = this.FirstName;
			dbObj.LastName = this.LastName;
			dbObj.Email = this.Email;
			dbObj.Password = this.Password;
			dbObj.ProposedPassword = this.ProposedPassword;
			dbObj.AccessToken = this.AccessToken;
			dbObj.FacebookId = this.FacebookId;
			dbObj.FacebookToken = this.FacebookToken;
			dbObj.FacebookDetailsJson = this.FacebookDetailsJson;
			dbObj.Token = this.Token;
			dbObj.Status = this.Status.ToString();
			dbObj.Manufacturer = this.Manufacturer;
			dbObj.Model = this.Model;
			dbObj.Serial = this.Serial;
			dbObj.AppVersion = this.AppVersion;
			base.SetFields((Database.BaseDatabaseObject)dbObj);
		}

		public object LoadFromDB(object databaseRecord, bool getChildren = false)
		{
			var dbObj = databaseRecord as Database.User;

			this.Id = (int)dbObj.Id;
			try
			{
				this.Type = (UserType)Enum.Parse(typeof(UserType), dbObj.Type);
			}
			catch
			{
				throw new Exception("invalid DB Type");
			}
			this.ClientId = dbObj.ClientId;
			this.FirstName = dbObj.FirstName;
			this.LastName = dbObj.LastName;
			this.Email = dbObj.Email;
			this.Password = dbObj.Password;
			this.ProposedPassword = dbObj.ProposedPassword;
			this.AccessToken = dbObj.AccessToken;
			this.FacebookId = dbObj.FacebookId;
			this.FacebookToken = dbObj.FacebookToken;
			this.FacebookDetailsJson = dbObj.FacebookDetailsJson;
			this.Token = dbObj.Token;
			try
			{
				this.Status = (UserStatus)Enum.Parse(typeof(UserStatus), dbObj.Status);
			}
			catch
			{
				throw new Exception("invalid DB Status");
			}
			this.Manufacturer = dbObj.Manufacturer;
			this.Model = dbObj.Model;
			this.Serial = dbObj.Serial;
			this.AppVersion = dbObj.AppVersion;


			if (getChildren)
			{
				// Add consequential childred processing
			}
			base.LoadFromDB(dbObj, getChildren);

			return this;
		}

		#endregion
		#region Static Database Methods
		public async static Task<ObservableCollection<User>> GetAllAsync(int clientId)
		{
			var result = new ObservableCollection<User>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();
			var dbObjs = await _dataService.AllUsersAsync(clientId);
			foreach (var dbObj in dbObjs)
			{
				var obj = new User(dbObj);
				result.Add(obj);
			}
			return result;
		}

		public static ObservableCollection<User> GetAll(int clientId)
		{
			var result = new ObservableCollection<User>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();
			var dbObjs = _dataService.AllUsers(clientId);
			foreach (var dbObj in dbObjs)
			{
				var obj = new User(dbObj);
				result.Add(obj);
			}
			return result;
		}

		public async static Task<ObservableCollection<User>> GetPickersAsync(int clientId)
		{
			var all = await User.GetAllAsync(clientId);
			return GetTypes(UserType.picker, all);
		}

		public async static Task<ObservableCollection<User>> GetCouriersAsync(int clientId)
		{
			var all = await User.GetAllAsync(clientId);
			return GetTypes(UserType.courier, all);
		}

		public static ObservableCollection<User> GetPickers(int clientId)
		{
			var all = User.GetAll(clientId);
			return GetTypes(UserType.picker, all);
		}

		public static ObservableCollection<User> GetCouriers(int clientId)
		{
			var all = User.GetAll(clientId);
					return GetTypes(UserType.courier, all);
		}


		private static ObservableCollection<User> GetTypes(UserType type, ObservableCollection<User> all)
		{
			var result = new ObservableCollection<User>();
			foreach (var user in all)
			{
				switch (type)
				{
					case UserType.admin:
						result.Add(user);
						break;
					case UserType.courier:
						if (user.Type == UserType.admin)
							result.Add(user);
						if (user.Type == UserType.courier)
							result.Add(user);
						break;
					case UserType.picker:
						if (user.Type == UserType.admin)
							result.Add(user);
						if (user.Type == UserType.picker)
							result.Add(user);
						break;
				}
			}
			return result;
		}

		public async static Task<DateTime> GetLastUpdateDate()
		{
			return await _dataService.MaxUserUpdateTimestampAsync(MyCookie.Cookie.ClientId);
		}

		public async static Task SyncWithServerAsync()
		{
			// Handle multiple simultaneous updates in a similar manner to GIT:
			// a) pull changes from the server since last update, not getting updates made by me
			// b) merge:
			//		- determine fields changed on the server, by comparing to my 'server image'
			//		- determine fields that I have changed, by comparing to my 'server image'
			//		- if no conflict, change field
			//		- if conflict
			//			- if there is a natural order apply this (e.g. status of 'cancelled' override status of 'picked')
			//			- if there isn't a natural order send event and prompt user to fix problem.
			// c) update local
			// d) push changes
			// NOTE: the UpdateUserId and UpdateTimestamp are only updated by the server and used to keep track of record level changes

			await ReceiveUsers();
			await SendUsers();

		}

		private async static Task ReceiveUsers()
		{
			_dataService = Settings.Container.Resolve<Database.IDataService>();
			_restService = Settings.Container.Resolve<IRestService2>();

			// Get all orders for client since last update date
			var lastUpdateDate = await User.GetLastUpdateDate();

			//TODO
			var abclastUpdateDate = "2017-04-25T06:10:16";
			//var url = String.Format("{0}/clients/{1}/users/{2}", Settings.BaseRestApiUrl, MyCookie.Cookie.ClientId, lastUpdateDate.ToString("s"));
			var url = String.Format("{0}/clients/{1}/users/{2}", Settings.BaseRestApiUrl, MyCookie.Cookie.ClientId, abclastUpdateDate);

			RestResponse response = await _restService.MakeRequestAsync<List<Database.User>>(url, HttpMethod.Get, null);
			if (!response.HttpResponseMessage.IsSuccessStatusCode)
			{
				//TODO better fix this
				var message = String.Format("Failed to get orders from server: {0}", response.HttpResponseMessage);
				Debug.WriteLine(message);
				//var ev = OnError;
				//	if (ev != null)
				//		ev(this, new ErrorMessageEventArgs(message));
				return;
			}

			if (response.Result != null)
			{
				var users = (List<Database.User>)response.Result;
				if (users != null)
				{
					foreach (var user in users)
					{
						var localUser = await _dataService.GetUserAsync((int)user.Id);
						await _dataService.SaveAsync(user);
					}
				}
				MessagingCenter.Send(new User(), "UsersUpdated");

			}
			else
			{
				//TODO something (or maybe not?? If nothing changed do I get empty list or null??)
				Debug.WriteLine("ERROR: Result from server is null");
			}
		}


		private async static Task SendUsers()
		{
			// Only send fields that have changed of records that have changed

			_dataService = Settings.Container.Resolve<Database.IDataService>();
			_restService = Settings.Container.Resolve<IRestService2>();

			var dbObjs = await _dataService.AllChangedUsersAsync();

			foreach (var user in dbObjs)
			{
				var url = String.Format("{0}/orders/{1}", Settings.BaseRestApiUrl, user.Id);
				RestResponse response = await _restService.MakeRequestAsync<int>(url, HttpMethod.Put, user);
				if (!response.HttpResponseMessage.IsSuccessStatusCode)
				{
					//TODO something
					Debug.WriteLine("ERROR: Failed to update order to server: {0}", response.HttpResponseMessage);
				}
				else
				{
					user.HasChanged = false;
					await _dataService.SaveAsync(user);
				}
			}
		}


		#endregion
		#region Constructors
		//static private IParserService _parserService;
		static private IDataService _dataService;
		static private IRestService2 _restService;
		static private IDeviceService _deviceService;
		public User(
		) : base()
		{
			_deviceService = Settings.Container.Resolve<IDeviceService>();
			_dataService = Settings.Container.Resolve<IDataService>();

			Email = "";
			Password = "";
			FirstName = "";
			LastName = "";
			FacebookToken = "";
			FacebookDetailsJson = "";
			DeviceUpdateTimestamp = DateTime.MinValue;

			this.DeviceCreateTimestamp = DateTime.Now;
			this.DeviceTimeZone = Cookie.DeviceTimeZone;
			this.UpdateTimestamp = DateTime.Now;
			this.Model = _deviceService.Model;
			this.Manufacturer = _deviceService.Manufacturer;
			this.Serial = _deviceService.Serial;    //WARNING: This changes on iOS
			this.AppVersion = Settings.AppVersion;

		}

		public User(int id) : base()
		{
			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

			_dataService = Settings.Container.Resolve<Database.IDataService>();

			var dbObj = _dataService.GetUser(id);
			if (dbObj != null)
				this.LoadFromDB(dbObj);
		}

		public User(Database.User dbObj) : base()
		{
			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

			_dataService = Settings.Container.Resolve<Database.IDataService>();


			this.LoadFromDB(dbObj);
		}
	
		~User()
		{
			Dispose(disposing: false);
		}
		#endregion
	}
}
