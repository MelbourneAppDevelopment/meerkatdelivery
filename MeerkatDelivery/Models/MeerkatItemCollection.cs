﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MeerkatDelivery.ViewModels;

namespace MeerkatDelivery.BusinessObjects
{
	public class MeerkatItemCollection<VM> : ObservableCollection<VM> where VM : MeerkatListItemViewModel
	{
		MeerkatItemCollectionFilter MeerkatItemCollectionFilter { get; set; }
		public MeerkatItemCollection(MeerkatItemCollectionFilter meerkatCollectionFilter)
		{
			MeerkatItemCollectionFilter = meerkatCollectionFilter;
		}

		public bool Contains(Order item)
		{
			//TODO a bit rough
			foreach (var i in this)
			{
				if (i.Order.Id == item.Id)
					return true;
			}
			return false;
		}

		public async Task RefreshAsync()
		{
			if (MeerkatItemCollectionFilter == null)
				MeerkatItemCollectionFilter = new MeerkatItemCollectionFilter();

			var orders = await Order.GetAllAsync();
			Filter(ref orders);

			foreach (var orderItem in this)
			{
				//TODO Test this - most likely won't work cause they will be different. Check on Id??
				if (orders.Contains(orderItem.Order))
				{
					var order = orders[orders.IndexOf(orderItem.Order)];
					orderItem.Order.LoadFromDB(order.DbOrder); // reload with new values
					orders.Remove(order);
				}
			}

			foreach (var order in orders)
			{
				if (!order.Deleted)
				{
					var item = (VM)new MeerkatListItemViewModel();
					item.Order = order;
					this.Add(item);
				}
			}
		}


		#region Private Methods
		private void Filter(ref ObservableCollection<Order> orders)
		{
			foreach (var order in orders)
			{
				var ok = true;
				if (order.OrderDate < MeerkatItemCollectionFilter.OrderDateRange.StartDate
					|| order.OrderDate > MeerkatItemCollectionFilter.OrderDateRange.EndDate)
					ok = false;

				else if (order.DeliveryDate < MeerkatItemCollectionFilter.DeliveryDateRange.StartDate
					|| order.DeliveryDate > MeerkatItemCollectionFilter.DeliveryDateRange.EndDate)
					ok = false;


				else if (MeerkatItemCollectionFilter.PickById != null)
				{
					if (order.PickingUserId != MeerkatItemCollectionFilter.PickById)
						ok = false;
				}

				else if (MeerkatItemCollectionFilter.DeliverById != null)
				{
					if (order.CourierUserId != MeerkatItemCollectionFilter.DeliverById)
						ok = false;
				}

				else if (MeerkatItemCollectionFilter.OrderStatusRange.RangeCriteria != RangeCriteria.Any)
				{
					switch (MeerkatItemCollectionFilter.OrderStatusRange.RangeCriteria)
					{
						case RangeCriteria.EQ:
							if (order.Status != MeerkatItemCollectionFilter.OrderStatusRange.OrderStatus)
								ok = false;
							break;
						default:
							throw new ArgumentOutOfRangeException(String.Format("OrderStatus range criteria {0} not implemented", MeerkatItemCollectionFilter.OrderStatusRange.RangeCriteria));
					}
				}

				if (!ok)
				{
					orders.Remove(order);
				}
			}
			#endregion
		}
	}
}

