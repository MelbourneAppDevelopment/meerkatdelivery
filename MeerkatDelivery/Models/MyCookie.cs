﻿using System;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace MeerkatDelivery.BusinessObjects
{
	public class MyCookie : BaseBusinessObject
	{
		static private MyCookie _cookie = null;
		#region Properties
		public int Id { get; set; }  // NOTE DateServices forces this to always be = 1
		public string AccessToken { get; set; }
		public int UserId { get; set; }
		public int ClientId { get; set; }        // all, incomplete, not assigned for picking, not assigned for delivery, 
		public DateTime OrderStartDate { get; set; }        // time, person, suburb, delivery seq, reverse delivery seq
		public DateTime OrderEndDate { get; set; }
		public DateTime DeliverStartDate { get; set; }
		public DateTime DeliverEndDate { get; set; }
		public int? PickBy { get; set; }
		public int? DeliverBy { get; set; }
		public OrderStatus OrderStatus { get; set; }		
		#endregion
		#region Public Methods
		public async Task LogoutAsync()
		{
			this.AccessToken = "";
			this.UserId = 0;
			await this.SaveAsync();
		}
		#endregion
		#region Constructors
		public static MyCookie Cookie
		{
			get
			{
				if (_cookie == null)
					_cookie = new MyCookie();
				return _cookie;
			}
		}

		static private MeerkatDelivery.Database.IDataService _dataService;
		public MyCookie() : base()
		{
			_dataService = Settings.Container.Resolve<Database.IDataService>();
			this.LoadCookie();
		}

		private async Task LoadCookie()
		{
			var dbObj = await this.FindCookieAsync();
			this.LoadFromDB(dbObj);
		}

		private async Task< Database.MyCookie> FindCookieAsync()
		{
			var dbObj = await _dataService.GetCookieAsync();
			if (dbObj == null)
			{
				await this.CreateCookie();
				dbObj = await _dataService.GetCookieAsync();
			}
			return dbObj;
		}

		private async Task CreateCookie()
		{
			var dbObj = new MeerkatDelivery.Database.MyCookie();
			dbObj.Id = 1;
			dbObj.AccessToken = "";
			dbObj.UserId = 0;

			await _dataService.InsertAsync(dbObj);
		}

		~MyCookie()
		{
			Dispose(disposing: false);
		}

		#endregion

		#region Database actions
		public async Task SaveAsync()
		{
			var dbObj = new Database.MyCookie();

			dbObj.Id = 1;
            this.SetFields(ref dbObj);

			try
			{
				await _dataService.UpdateAsync(dbObj);
			}
			catch
			{
				await _dataService.InsertAsync(dbObj);
			}
		}

		public void Save()
		{
			var dbObj = new Database.MyCookie();

			dbObj.Id = 1;
			this.SetFields(ref dbObj);

			try
			{
				_dataService.Update(dbObj);
			}
			catch
			{
				_dataService.Insert(dbObj);
			}
		}
		private void SetFields(ref Database.MyCookie dbObj)
		{
			dbObj.AccessToken = this.AccessToken;
			dbObj.UserId = this.UserId;
			dbObj.ClientId = this.ClientId;
			dbObj.OrderStartDate = this.OrderStartDate;
			dbObj.OrderEndDate = this.OrderEndDate;
			dbObj.DeliverStartDate = this.DeliverStartDate;
			dbObj.DeliverEndDate = this.DeliverEndDate;
			dbObj.PickBy = this.PickBy;
			dbObj.DeliverBy = this.DeliverBy;
			dbObj.OrderStatus = this.OrderStatus.ToString();
		}

		public object LoadFromDB(Database.MyCookie dbObj, bool getChildren = false)
		{
		   this.Id  = (int)dbObj.Id;
		   this.AccessToken  = dbObj.AccessToken;
		   this.UserId  = dbObj.UserId;
		   this.ClientId  = dbObj.ClientId;
		   this.OrderStartDate  = dbObj.OrderStartDate;
		   this.OrderEndDate  = dbObj.OrderEndDate;
		   this.DeliverStartDate  = dbObj.DeliverStartDate;
		   this.DeliverEndDate  = dbObj.DeliverEndDate;
		   this.PickBy  = dbObj.PickBy;
		   this.DeliverBy  = dbObj.DeliverBy;
		   try {
		    this.OrderStatus = (OrderStatus)Enum.Parse(typeof(OrderStatus), dbObj.OrderStatus);
		   } catch {
		    throw new Exception("invalid DB OrderStatus");
		   }

			if (getChildren)
			{
				// Add consequential childred processing
			}

			return this;
		}

		#endregion
	}
}

