﻿using System;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjectServices;
using Microsoft.Practices.Unity;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using MeerkatDelivery.Services;
using System.Net.Http;
using MeerkatDelivery.Resources;
using Xamarin.Forms;
using System.Diagnostics;
using System.Reflection;
using MeerkatDelivery.CustomControls;
using XLabs.Platform.Services.Geolocation;
using MeerkatDelivery.Helpers;
using System.Linq;

namespace MeerkatDelivery.BusinessObjects
{

	//-----
	//
	// NOTE: This is a non-database example
	//
	//-----



	public class Order : BaseBusinessObject
	{
		#region Constants
		#endregion
		#region private Properties
		#endregion

		#region Properties

		// --- NOT GENERATED ---
		public Database.Order DbOrder;

		public string InstanceId { get; set; } // Only for testing
		public string InvalidReason { get; set; }

		private CustomPin _pin = null;
		public CustomPin Pin
		{
			get
			{
				if (_pin == null)
				{
					_pin = new CustomPin
					{
						// MKAnnotation.Title = Label		(top field in display)
						// MKAnnotation.Subtitle = Address	(second feild when click on pin)
						Pin = new Xamarin.Forms.Maps.Pin
						{
							Type = Xamarin.Forms.Maps.PinType.Generic,
							Position = new Xamarin.Forms.Maps.Position(this.GpsLatitude, this.GpsLongitude),
							Label = this.RecipientStreetAddress,
							Address = this.RecipientStreetAddress
						},
						OrderId = this.Id,
						Url = ""
					};
				}
				return _pin;
			}
		}

		// --- GENERATED ---

		public int Id { get; set; }        // service unique id
		public int ClientId { get; set; }

		public bool Deleted { get; set; }

		public double Amount { get; set; }
		public DateTime OrderDate { get; set; }
		public string CardText { get; set; }
		public int ClearingNumber { get; set; }
		public string ContactPhone { get; set; }

		private DateTime _deliveredTime = DateTime.MaxValue;
		public DateTime DeliveredTime
		{
			get { return _deliveredTime; }
			set { _deliveredTime = value; }
		}

		private DateTime _deliveryDate = DateTime.Today;
		public DateTime DeliveryDate
		{
			get { return _deliveryDate; }
			set { _deliveryDate = value; }
		}

		public double DeliveredGpsLatitude { get; set; }
		public double DeliveredGpsLongitude { get; set; }
		public byte[] DeliveredImage { get; set; }
		public int DeliverySequence { get; set; }
		public DateTime DeliveryTime { get; set; }
		public DeliveryTime DeliveryTimeCode { get; set; }
		public int CourierUserId { get; set; }
		public double GpsLatitude { get; set; }
		public double GpsLongitude { get; set; }
		public byte[] Image1 { get; set; }
		public byte[] Image2 { get; set; }
		public DateTime LoadedTime { get; set; }
		public string Notes { get; set; }
		public string OrderNumber { get; set; }
		public string OrderSystem { get; set; }
		public string OrderSystemClientId { get; set; }
		public DateTime PickedTime { get; set; }
		public DateTime PickingTime { get; set; }
		public int PickingUserId { get; set; }
		public string Priority { get; set; }
		public string Product1Code { get; set; }
		public string Product1Description { get; set; }
		public string Product2Code { get; set; }
		public string Product2Description { get; set; }
		public string Product3Code { get; set; }
		public string Product3Description { get; set; }

		public string Summary
		{
			get
			{
				switch (Status)
				{
					case OrderStatus.Delivered:
						return String.Format("Delivered at {0}", DeliveredTime);
					case OrderStatus.FailedDelivery:
						return String.Format("Failed to deliver at {0}", DeliveredTime);
					default:
						return String.Format("Last updated at {0}", DeviceUpdateTimestamp);
				}
			}
		}

		public string RecipientCompanyName { get; set; }
		public string RecipientEmail { get; set; }
		public string RecipientName { get; set; }
		public string RecipientPhone { get; set; }

		private string _recipientState;
		public string RecipientState
		{
			get { return _recipientState; }
			set
			{
				if (value != _recipientState)
				{
					_recipientState = value;
					_pin = null;                // Reset the map pin
				}
			}
		}
		private string _recipientStreetAddress;
		public string RecipientStreetAddress
		{
			get { return _recipientStreetAddress; }
			set
			{
				if (value != _recipientStreetAddress)
				{
					_recipientStreetAddress = value;
					_pin = null;                // Reset the map pin
				}
			}
		}
		private string _recipientSuburb;
		public string RecipientSuburb
		{
			get { return _recipientSuburb; }
			set
			{
				if (value != _recipientSuburb)
				{
					_recipientSuburb = value;
					_pin = null;                // Reset the map pin
				}
			}
		}
		private string _recipientZip;
		public string RecipientZip
		{
			get { return _recipientZip; }
			set
			{
				if (value != _recipientZip)
				{
					_recipientZip = value;
					_pin = null;                // Reset the map pin
				}
			}
		}

		public string SenderAddress { get; set; }
		public string SenderCompanyName { get; set; }
		public string SenderEmail { get; set; }
		public string SenderName { get; set; }
		public string SenderPhone { get; set; }
		public string SenderState { get; set; }
		public string SenderSuburb { get; set; }
		public string SenderZip { get; set; }
		public string SpecialInstructions { get; set; }
		public OrderStatus Status { get; set; }        // ordered, cancelled, picked, loaded, delivered, failed delivery


		#endregion
		#region Events
		public event EventHandler<OrderStateChangedEventArgs> OnStateChanged;
		public event EventHandler<ErrorMessageEventArgs> OnError;

		#endregion
		#region Public Methods
		public override string ToString()
		{
			return String.Format("Id {0}|OrderSystem {1}|OrderNumber {2}|ClientId {3}|Deleted {52}|OrderSystemClientId {4}|SenderName {5}|SenderCompanyName {6}|SenderAddress {7}|SenderSuburb {8}|SenderState {9}|SenderZip {10}|SenderPhone {11}|SenderEmail {12}|RecipientName {13}|RecipientCompanyName {14}|RecipientStreetAddress {15}|RecipientSuburb {16}|RecipientState {17}|RecipientZip {18}|RecipientPhone {19}|RecipientEmail {20}|ContactPhone {21}|DeliveryDate {22}|DeliveryTime {23}|DeliveryTimeCode {24}|SpecialInstructions {25}|Priority {26}|Product1Code {27}|Product1Description {28}|Product2Code {29}|Product2Description {30}|Product3Code {31}|Product3Description {32}|Notes {33}|Amount {34}|CardText {35}|ClearingNumber {36}|GpsLatitude {37}|GpsLongitude {38}|DeliverySequence {39}|Image1 {40}|Image2 {41}|PickingUserId {42}|PickingTime {43}|PickedTime {44}|DeliveryUserId {45}|LoadedTime {46}|DeliveredImage {47}|DeliveredTime {48}|DeliveredGpsLatitude {49}|DeliveredGpsLongitude {50}|Status {51}|",
				this.Id,
				this.OrderSystem,
				this.OrderNumber,
				this.ClientId,
				this.OrderSystemClientId,
				this.SenderName,
				this.SenderCompanyName,
				this.SenderAddress,
				this.SenderSuburb,
				this.SenderState,
				this.SenderZip,
				this.SenderPhone,
				this.SenderEmail,
				this.RecipientName,
				this.RecipientCompanyName,
				this.RecipientStreetAddress,
				this.RecipientSuburb,
				this.RecipientState,
				this.RecipientZip,
				this.RecipientPhone,
				this.RecipientEmail,
				this.ContactPhone,
				this.DeliveryDate,
				this.DeliveryTime,
				this.DeliveryTimeCode,
				this.SpecialInstructions,
				this.Priority,
				this.Product1Code,
				this.Product1Description,
				this.Product2Code,
				this.Product2Description,
				this.Product3Code,
				this.Product3Description,
				this.Notes,
				this.Amount,
				this.CardText,
				this.ClearingNumber,
				this.GpsLatitude,
				this.GpsLongitude,
				this.DeliverySequence,
				this.Image1,
				this.Image2,
				this.PickingUserId,
				this.PickingTime,
				this.PickedTime,
				this.CourierUserId,
				this.LoadedTime,
				this.DeliveredImage,
				this.DeliveredTime,
				this.DeliveredGpsLatitude,
				this.DeliveredGpsLongitude,
				this.Status,
								 this.Deleted
								);
		}


		public async Task<bool> Validate()
		{
			InvalidReason = "";
			var result = true;


			if (this.RecipientStreetAddress == "")
			{
				InvalidReason = "You must at least have a delivery address";
				result = false;
			}
			else
			{
				var address = String.Format("{0} {1} {2} {3}", RecipientStreetAddress, RecipientSuburb, RecipientState, RecipientZip);
				var gpsPosition = await MapHelper.GeoCoder.GetOnePositionFromAddressAsync(address, Settings.MaxDistanceFromCurrentLocation);
				if (gpsPosition == null)
				{
					InvalidReason = "No GPS location found for that address";
					result = false;
				}
				else
				{
					this.GpsLatitude = gpsPosition.Latitude;
					this.GpsLongitude = gpsPosition.Longitude;
				}
			}

			return result;
		}
		#endregion
		#region Private Methods
		#endregion
		#region DatabaseMethods
		public async Task SaveAsync()
		{
			var dbObj = new Database.Order();
			this.SetFields(ref dbObj);
			if (dbObj.Id != 0)
				await _dataService.UpdateAsync(dbObj);
			else
			{

				//var url = String.Format("{0}/clients/{1}/orders", Settings.BaseRestApiUrl, MyCookie.Cookie.ClientId);
				//RestResponse response = await _restService.MakeRequestAsync<int>(url, HttpMethod.Post, dbObj);

				//if (!response.HttpResponseMessage.IsSuccessStatusCode || response.Result == null || (int)response.Result == 0)
				//{
				//	var message = String.Format("Failed to add order: {0}", response.HttpResponseMessage.ReasonPhrase);
				//	var ev = OnError;
				//	if (ev != null)
				//		ev(this, new ErrorMessageEventArgs(message));
				//	return;
				//}

				//dbObj.Id = (int)response.Result;
				await _dataService.InsertAsync(dbObj);
			}
		}

		public void Save()
		{
			var dbObj = new Database.Order();
			this.SetFields(ref dbObj);
			if (dbObj.Id != 0)
				_dataService.Update(dbObj);
			else
				_dataService.Insert(dbObj);
		}

		public void Delete()
		{
			this.Deleted = true;
			this.Save();
		}

		private void SetFields(ref Database.Order dbObj)
		{
			Debug.WriteLine("Clinet id DB hard coded to 2");
			dbObj.ClientId = Settings.DefaultClientId;

		   dbObj.Amount   = this.Amount;
		   dbObj.OrderDate   = this.OrderDate;
		   dbObj.CardText   = this.CardText;
		   dbObj.ClearingNumber   = this.ClearingNumber;
		   dbObj.Deleted   = this.Deleted;
		   dbObj.ContactPhone   = this.ContactPhone;
		   dbObj.DeliveredTime   = this.DeliveredTime;
		   dbObj.DeliveryDate   = this.DeliveryDate;
		   dbObj.DeliveredGpsLatitude   = this.DeliveredGpsLatitude;
		   dbObj.DeliveredGpsLongitude   = this.DeliveredGpsLongitude;
		   dbObj.DeliveredImage   = this.DeliveredImage;
		   dbObj.DeliverySequence   = this.DeliverySequence;
		   dbObj.DeliveryTime   = this.DeliveryTime;
		   dbObj.DeliveryTimeCode   = this.DeliveryTimeCode.ToString();
		   dbObj.DeliveryUserId   = this.CourierUserId;
		   dbObj.GpsLatitude   = this.GpsLatitude;
		   dbObj.GpsLongitude   = this.GpsLongitude;
		   dbObj.Id   = (int)this.Id;
		   dbObj.Image1   = this.Image1;
		   dbObj.Image2   = this.Image2;
		   dbObj.LoadedTime   = this.LoadedTime;
		   dbObj.Notes   = this.Notes;
		   dbObj.OrderNumber   = this.OrderNumber;
		   dbObj.OrderSystem   = this.OrderSystem;
		   dbObj.OrderSystemClientId   = this.OrderSystemClientId;
		   dbObj.PickedTime   = this.PickedTime;
		   dbObj.PickingTime   = this.PickingTime;
		   dbObj.PickingUserId   = this.PickingUserId;
		   dbObj.Priority   = this.Priority;
		   dbObj.Product1Code   = this.Product1Code;
		   dbObj.Product1Description   = this.Product1Description;
		   dbObj.Product2Code   = this.Product2Code;
		   dbObj.Product2Description   = this.Product2Description;
		   dbObj.Product3Code   = this.Product3Code;
		   dbObj.Product3Description   = this.Product3Description;
		   dbObj.RecipientCompanyName   = this.RecipientCompanyName;
		   dbObj.RecipientEmail   = this.RecipientEmail;
		   dbObj.RecipientName   = this.RecipientName;
		   dbObj.RecipientPhone   = this.RecipientPhone;
		   dbObj.RecipientState   = this.RecipientState;
		   dbObj.RecipientStreetAddress   = this.RecipientStreetAddress;
		   dbObj.RecipientSuburb   = this.RecipientSuburb;
		   dbObj.RecipientZip   = this.RecipientZip;
		   dbObj.SenderAddress   = this.SenderAddress;
		   dbObj.SenderCompanyName   = this.SenderCompanyName;
		   dbObj.SenderEmail   = this.SenderEmail;
		   dbObj.SenderName   = this.SenderName;
		   dbObj.SenderPhone   = this.SenderPhone;
		   dbObj.SenderState   = this.SenderState;
		   dbObj.SenderSuburb   = this.SenderSuburb;
		   dbObj.SenderZip   = this.SenderZip;
		   dbObj.SpecialInstructions   = this.SpecialInstructions;
		   dbObj.Status   = this.Status.ToString();

			switch (dbObj.Status)
			{
				case "Delivered":
				case "FailedDelivery":
					dbObj.DeliveredTime = DateTime.Now;
					break;

				default:
					dbObj.DeliveredTime = DateTime.MaxValue;
					break;
			}

			base.SetFields((Database.BaseDatabaseObject)dbObj);
		}

		public object LoadFromDB(object databaseRecord, bool getChildren = false)
		{
			var dbObj = databaseRecord as Database.Order;

            this.DbOrder = dbObj;

		   this.Amount  = dbObj.Amount;
           this.OrderDate  = dbObj.OrderDate;
		   this.CardText  = dbObj.CardText;
		   this.ClearingNumber  = dbObj.ClearingNumber;
		   this.ClientId  = dbObj.ClientId;
		   this.ContactPhone  = dbObj.ContactPhone;
			this.Deleted  = dbObj.Deleted;
		   this.DeliveredTime  = dbObj.DeliveredTime;
		   this.DeliveryDate  = dbObj.DeliveryDate;
		   this.DeliveredGpsLatitude  = dbObj.DeliveredGpsLatitude;
		   this.DeliveredGpsLongitude  = dbObj.DeliveredGpsLongitude;
		   this.DeliveredImage  = dbObj.DeliveredImage;
		   this.DeliverySequence  = dbObj.DeliverySequence;
		   this.DeliveryTime  = dbObj.DeliveryTime;
		   try {
		    this.DeliveryTimeCode = (DeliveryTime)Enum.Parse(typeof(DeliveryTime), dbObj.DeliveryTimeCode);
		   } catch {
				Debug.WriteLine("ERROR TO BE FIXED: Invalid DeliveryTimeCode");
		    //throw new Exception("invalid DB DeliveryTimeCode");
		   }
           this.CourierUserId  = dbObj.DeliveryUserId;
		   this.GpsLatitude  = dbObj.GpsLatitude;
		   this.GpsLongitude  = dbObj.GpsLongitude;
		   this.Id  = (int)dbObj.Id;
		   this.Image1  = dbObj.Image1;
		   this.Image2  = dbObj.Image2;
		   this.LoadedTime  = dbObj.LoadedTime;
		   this.Notes  = dbObj.Notes;
		   this.OrderNumber  = dbObj.OrderNumber;
		   this.OrderSystem  = dbObj.OrderSystem;
		   this.OrderSystemClientId  = dbObj.OrderSystemClientId;
		   this.PickedTime  = dbObj.PickedTime;
		   this.PickingTime  = dbObj.PickingTime;
		   this.PickingUserId  = dbObj.PickingUserId;
		   this.Priority  = dbObj.Priority;
		   this.Product1Code  = dbObj.Product1Code;
		   this.Product1Description  = dbObj.Product1Description;
		   this.Product2Code  = dbObj.Product2Code;
		   this.Product2Description  = dbObj.Product2Description;
		   this.Product3Code  = dbObj.Product3Code;
		   this.Product3Description  = dbObj.Product3Description;
		   this.RecipientCompanyName  = dbObj.RecipientCompanyName;
		   this.RecipientEmail  = dbObj.RecipientEmail;
		   this.RecipientName  = dbObj.RecipientName;
		   this.RecipientPhone  = dbObj.RecipientPhone;
		   this.RecipientState  = dbObj.RecipientState;
		   this.RecipientStreetAddress  = dbObj.RecipientStreetAddress;
		   this.RecipientSuburb  = dbObj.RecipientSuburb;
		   this.RecipientZip  = dbObj.RecipientZip;
		   this.SenderAddress  = dbObj.SenderAddress;
		   this.SenderCompanyName  = dbObj.SenderCompanyName;
		   this.SenderEmail  = dbObj.SenderEmail;
		   this.SenderName  = dbObj.SenderName;
		   this.SenderPhone  = dbObj.SenderPhone;
		   this.SenderState  = dbObj.SenderState;
		   this.SenderSuburb  = dbObj.SenderSuburb;
		   this.SenderZip  = dbObj.SenderZip;
		   this.SpecialInstructions  = dbObj.SpecialInstructions;
		   try {
		    this.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), dbObj.Status);
		   } catch {
		    throw new Exception("invalid DB Status");
		   }

			if (getChildren)
			{
				// Add consequential childred processing
			}
			base.LoadFromDB(dbObj, getChildren);

			return this;
		}
		#endregion
		#region Static Database Methods
		/// <summary>
		/// Gets all orders.
		/// Orders them by DeliveredTime, DeliverySequence (route sequence) and the order they were added
		/// </summary>
		public async static Task<ObservableCollection<Order>> GetAllAsync()
		{
			var result = new ObservableCollection<Order>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();


			//var dbObjs = await _dataService.AllOrdersAsync(MyCookie.Cookie.ClientId);
			var dbObjs = await _dataService.AllOrdersAsync(Settings.DefaultClientId);

			var sorted = dbObjs.OrderByDescending(x => x.DeliveredTime).ThenBy(x => x.DeliverySequence).ThenBy(x => x.Id).ToList();

			foreach (var dbObj in sorted)
			{
				var obj = new Order(dbObj);
				result.Add(obj);
			}
			return result;
		}

		public static ObservableCollection<Order> GetAll()
		{
			var result = new ObservableCollection<Order>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();


			var dbObjs =  _dataService.AllOrders(MyCookie.Cookie.ClientId);


			foreach (var dbObj in dbObjs)
			{
				var obj = new Order(dbObj);
				result.Add(obj);
			}
			return result;
		}

		public async static Task<DateTime> GetLastUpdateDate()
		{
			return await _dataService.MaxOrderUpdateTimestampAsync(MyCookie.Cookie.ClientId);
		}

		public async static Task SyncWithServerAsync()
		{
			// Handle multiple simultaneous updates in a similar manner to GIT:
			// a) pull changes from the server since last update, not getting updates made by me
			// b) merge:
			//		- determine fields changed on the server, by comparing to my 'server image'
			//		- determine fields that I have changed, by comparing to my 'server image'
			//		- if no conflict, change field
			//		- if conflict
			//			- if there is a natural order apply this (e.g. status of 'cancelled' override status of 'picked')
			//			- if there isn't a natural order send event and prompt user to fix problem.
			// c) update local
			// d) push changes
			// NOTE: the UpdateUserId and UpdateTimestamp are only updated by the server and used to keep track of record level changes

			await ReceiveOrders();
			await SendOrders();

		}

		private async static Task ReceiveOrders()
		{
			_dataService = Settings.Container.Resolve<Database.IDataService>();
			_restService = Settings.Container.Resolve<IRestService2>();

			// Get all orders for client since last update date
			var lastUpdateDate = await Order.GetLastUpdateDate();

			//TODO
			var abclastUpdateDate = "2017-04-25T06:10:16";
			//var url = String.Format("{0}/clients/{1}/orders/{2}", Settings.BaseRestApiUrl, MyCookie.Cookie.ClientId, lastUpdateDate.ToString("s"));
var url = String.Format("{0}/clients/{1}/orders/{2}", Settings.BaseRestApiUrl, MyCookie.Cookie.ClientId, abclastUpdateDate);

			RestResponse response = await _restService.MakeRequestAsync<List<Database.Order>>(url, HttpMethod.Get, null);
			if (!response.HttpResponseMessage.IsSuccessStatusCode)
			{
				//TODO better fix this
				var message = String.Format("Failed to get orders from server: {0}", response.HttpResponseMessage);
				Debug.WriteLine(message);
				//var ev = OnError;
				//	if (ev != null)
				//		ev(this, new ErrorMessageEventArgs(message));
				return;
			}

			if (response.Result != null)
			{
				var orders = (List<Database.Order>)response.Result;
				if (orders != null)
				{
					foreach (var order in orders)
					{
						var localOrder = await _dataService.GetOrderAsync((int)order.Id);
						var mergedOrder = await MergeOrder(localOrder, order);
						await _dataService.SaveAsync(mergedOrder);

						//TODO Check this
						var serverImageOrder = new Database.ServerImageOrder();
						serverImageOrder.Copy(order);
						await _dataService.SaveAsync(serverImageOrder);
					}
				}
				MessagingCenter.Send(new Order(),"OrdersUpdated");

			}
			else
			{
				//TODO something (or maybe not?? If nothing changed do I get empty list or null??)
				Debug.WriteLine("ERROR: Result from server is null");
			}
		}

		private static async Task<Database.Order> MergeOrder(Database.Order localOrder, Database.Order incomingOrder)
		{
			// merge my local changes with the incoming order
			//TODO ... not tested!
			return incomingOrder;

			var serverImageOrder = await _dataService.GetServerImageOrderAsync((int)incomingOrder.Id);
			if (serverImageOrder == null)
				serverImageOrder = new Database.ServerImageOrder();

			var incomingProperties = incomingOrder.GetType().GetRuntimeProperties();

			foreach (var incomingProperty in incomingProperties)
			{
				string name = incomingProperty.Name;

				//TODO check this
				if (name == "UpdateTimestamp" || name == "UpdateUserId")
					continue;

				var incomingValue = incomingProperty.GetValue(serverImageOrder, null);
				var localProperty = localOrder.GetType().GetRuntimeProperty(name);
				var localValue = localProperty.GetValue(localOrder, null);
				var serverProperty = serverImageOrder.GetType().GetRuntimeProperty(name);
				var serverValue = serverProperty.GetValue(serverImageOrder, null);

				if (serverValue == localValue)
					localProperty.SetValue(localOrder, incomingValue);
				else if (incomingValue == localValue)
					continue;   // no change
				else            // potential conflict
				{
					var propertyType = incomingProperty.PropertyType;

					// Dates become the greater
					if (propertyType.ToString() == "System.DateTime")
						localProperty.SetValue(localOrder, (DateTime)incomingValue > (DateTime)localValue ? incomingValue : localValue);

					else
					{
						// everything else is a potential conflict
						localProperty.SetValue(localOrder, incomingValue);
						var message = String.Format("Merge conflict for {0}: local = '{1}' server = '{2}'", name, localValue.ToString(), serverValue.ToString());
						Debug.WriteLine(message);
						//var ev = OnError;
						//	if (ev != null)
						//		ev(this, new ErrorMessageEventArgs(message));


					}

				}
			}
			return localOrder;
		}

		private async static Task SendOrders()
		{
			// Only send fields that have changed of records that have changed

			_dataService = Settings.Container.Resolve<Database.IDataService>();
			_restService = Settings.Container.Resolve<IRestService2>();

			var dbObjs = await _dataService.AllChangedOrdersAsync();

			foreach (var order in dbObjs)
			{
				var url = String.Format("{0}/orders/{1}", Settings.BaseRestApiUrl,order.Id);
				RestResponse response = await _restService.MakeRequestAsync<int>(url, HttpMethod.Put, order);
				if (!response.HttpResponseMessage.IsSuccessStatusCode)
				{
					//TODO something
					Debug.WriteLine("ERROR: Failed to update order to server: {0}",response.HttpResponseMessage);
				} else
				{
					order.HasChanged = false;
					await _dataService.SaveAsync(order);
				}
			}
		}



		#endregion
		#region Constructors
		private readonly IExampleServices _exampleServices;
		static private  IRestService2 _restService;
		static private MeerkatDelivery.Database.IDataService _dataService;
		public Order()
		{
			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

			_exampleServices = Settings.Container.Resolve<IExampleServices>();
			_restService = Settings.Container.Resolve<IRestService2>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();

		}

		public Order(int orderId)
		{
			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

			_exampleServices = Settings.Container.Resolve<IExampleServices>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();

			var dbObj = _dataService.GetOrder(orderId);
			if (dbObj != null)
                this.LoadFromDB(dbObj);
		}

		public Order(Database.Order dbObj)
		{
			InstanceId = String.Format("P{0}", DateTime.Now.ToString("hhmmss.fff"));

			_exampleServices = Settings.Container.Resolve<IExampleServices>();
			_dataService = Settings.Container.Resolve<Database.IDataService>();


			this.LoadFromDB(dbObj);
		}


		#endregion
		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				//TODO: unmanaged objects

				disposedValue = true;
			}
		}

		// override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		 ~Order() {
		   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.

		   Dispose(false);
		 }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// uncomment the following line if the finalizer is overridden above.
			 GC.SuppressFinalize(this);
		}

		#endregion
	}
}
