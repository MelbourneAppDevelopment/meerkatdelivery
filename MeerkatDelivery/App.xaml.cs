﻿using System;
using System.Diagnostics;
using MeerkatDelivery.BusinessObjectServices;
using MeerkatDelivery.Views;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MeerkatDelivery.Services;
using MeerkatDelivery.Database;
using MeerkatDelivery.BusinessObjects;
using System.Threading.Tasks;
using System.Collections.Generic;
using MeerkatDelivery.Helpers;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MeerkatDelivery
{

	public partial class App : PrismApplication
	{
		static public int ScreenWidth;
		static public int ScreenHeight;

		public App(IPlatformInitializer initializer = null) : base(initializer)
		{
		}

		protected override void OnInitialized()
		{
 
			//var navPage = new NavigationPage(new OrdersPage())
			//{
			//	BarBackgroundColor = Color.Black,
			//	BarTextColor = Color.White
			//};
			var navPage = new NavigationPage(new TabPage())
			{
				BarBackgroundColor = Color.Black,
				BarTextColor = Color.White
			};

            InitializeComponent();
			MainPage = navPage;

			Device.BeginInvokeOnMainThread(async() =>
			  {
				  if (Settings.DebugMode)
				  {
					  this.DumpData();
					  this.CreateTestdata();
				  }
							// Initialise Forms pages


				//await NavigationService.NavigateAsync("OrdersPage");
				//await NavigationService.NavigateAsync("DeliveriesPage");
				await NavigationService.NavigateAsync("TabPage");
			  });
		}

		protected override void RegisterTypes()
		{
			Settings.Container = Container;

			Container.RegisterType<IExampleServices, ExampleServices>(new ContainerControlledLifetimeManager());
			Container.RegisterType<IDataService, DataService>(new ContainerControlledLifetimeManager());
			Container.RegisterType<IRestService2, RestService2>(new ContainerControlledLifetimeManager());

			Container.RegisterTypeForNavigation<MainPage>();
			Container.RegisterTypeForNavigation<OrderPage>();
			Container.RegisterTypeForNavigation<OrderPage>();
			Container.RegisterTypeForNavigation<TabPage>();
			Container.RegisterTypeForNavigation<FilterPage>();
			Container.RegisterTypeForNavigation<TestPage>();
			Container.RegisterTypeForNavigation<DeliveriesPage>();

			// For interface implemented in ios or Android project
			Container.RegisterType<IDeviceService>();
			//Container.RegisterType<IReachability>();

			// Mulit-language setup
			var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
			MeerkatDelivery.Resources.AppResources.Culture = ci; // set the RESX for resource localization
			DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
		}

		protected override void OnStart()
		{
			HockeyApp.MetricsManager.TrackEvent("App Starting");
		}

		protected override void OnSleep()
		{
			MessagingCenter.Send(this, "OnSleep");
			HockeyApp.MetricsManager.TrackEvent("App going to sleep");
		}

		protected override void OnResume()
		{
			MessagingCenter.Send(this, "OnResume");
			HockeyApp.MetricsManager.TrackEvent("App resuming");
		}

		private void DumpData()
		{
			var _dataService = Container.Resolve<IDataService>();

			var users = _dataService.AllUsers();
			foreach (var user in users)
				Debug.WriteLine("User: {0}",user.ToString());

			var orders = _dataService.AllOrders();
			foreach (var order in orders)
				Debug.WriteLine("Order: {0}",order.ToString());

		}


		private  void CreateTestdata()
		{
			var isAsync = false;

			var _dataService = Container.Resolve<IDataService>();

			var userCount = _dataService.Query("select count(*) from User");
			if (userCount == 0)
				this.AddUsers();

			BusinessObjects.MyCookie.Cookie.ClientId = Settings.DefaultClientId;
			BusinessObjects.MyCookie.Cookie.UserId = 2;
			BusinessObjects.MyCookie.Cookie.Save();

			return;

			_dataService.DeleteAllOrders();

			var obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "9823 0234";
			obj.DeliveredGpsLatitude = 0;
			obj.DeliveredGpsLongitude = 0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
			obj.CourierUserId = 0;
			obj.GpsLatitude = -37.815018;
			obj.GpsLongitude = 144.946014;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N1";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N122234";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 1;
			obj.Priority = "High Priority";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "43 Barton St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

			obj.UpdateTimestamp = DateTime.Now.AddHours(-2.0);

			obj.Save();

			obj = new BusinessObjects.Order();


			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Anytime;
			obj.CourierUserId = 2;
			obj.GpsLatitude = -37.825018;
			obj.GpsLongitude = 144.946014;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N2";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N1234";
			obj.PickedTime = DateTime.MinValue;
			obj.PickingTime = DateTime.MinValue;
			obj.PickingUserId = 0;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Bouquet Nbr 17, 20 long stem roses, with fruit basket and chocolate box 17";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "5 Morang Rd";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "Leave behind chimney";
			obj.Status = OrderStatus.Ordered;

			obj.UpdateTimestamp = DateTime.Now.AddHours(-1.0);
			obj.Save();

			obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
obj.CourierUserId = 0;
			obj.GpsLatitude = -37.815018;
			obj.GpsLongitude = 144.956014;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N3";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N122234";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 1;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "12 Barton St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

			obj.UpdateTimestamp = DateTime.Now.AddHours(-2.0);

			obj.Save();

			obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
obj.CourierUserId = 2;
			obj.GpsLatitude = -37.825018;
			obj.GpsLongitude = 144.956014;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N4";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N12343399";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 0;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "68 Denham St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

		obj.Save();

			obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
obj.CourierUserId = 2;
			obj.GpsLatitude = -37.826518;
			obj.GpsLongitude = 144.956414;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N5";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N12343399";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 0;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "68 Denham St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

		obj.Save();

			obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
obj.CourierUserId = 2;
			obj.GpsLatitude = -37.8261018;
			obj.GpsLongitude = 144.957714;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N6";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N12343399";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 0;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "68 Denham St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

		obj.Save();

			obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
obj.CourierUserId = 2;
			obj.GpsLatitude = -37.8244018;
			obj.GpsLongitude = 144.9534014;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N7";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N12343399";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 0;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "68 Denham St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

		obj.Save();

			obj = new BusinessObjects.Order();

			obj.Amount = 1.0;
			obj.CardText = "card text";
			obj.ClearingNumber = 3;
			obj.ClientId = BusinessObjects.MyCookie.Cookie.ClientId;
			obj.ContactPhone = "contact phone";
			obj.DeliveredGpsLatitude = 6.0;
			obj.DeliveredGpsLongitude = 7.0;
			obj.DeliveredImage = null;
			obj.DeliveredTime = DateTime.Now;
			obj.DeliveryDate = DateTime.Now;
			obj.DeliverySequence = 11;
			obj.DeliveryTime = DateTime.Now;
			obj.DeliveryTimeCode = DeliveryTime.Morning;
obj.CourierUserId = 2;
			obj.GpsLatitude = -37.8246018;
			obj.GpsLongitude = 144.956014;
			obj.Id = 0;
			obj.Image1 = null;
			obj.Image2 = null;
			obj.LoadedTime = DateTime.Now;
			obj.Notes = "These are our notes";
			obj.OrderNumber = "N8";
			obj.OrderSystem = "order system";
			obj.OrderSystemClientId = "N12343399";
			obj.PickedTime = DateTime.Now;
			obj.PickingTime = DateTime.Now;
			obj.PickingUserId = 0;
			obj.Priority = "";
			obj.Product1Code = "product1 code";
			obj.Product1Description = "Lots of roses";
			obj.Product2Code = "product2 code";
			obj.Product2Description = "product2 description";
			obj.Product3Code = "product3 code";
			obj.Product3Description = "product3 description";
			obj.RecipientCompanyName = "recipient company name";
			obj.RecipientEmail = "recipient email";
			obj.RecipientName = "recipient name";
			obj.RecipientPhone = "recipient phone";
			obj.RecipientState = "Vic";
			obj.RecipientStreetAddress = "68 Denham St";
			obj.RecipientSuburb = "Hawthorn";
			obj.RecipientZip = "3122";
			obj.SenderAddress = "sender address";
			obj.SenderCompanyName = "sender company name";
			obj.SenderEmail = "sender email";
			obj.SenderName = "sender name";
			obj.SenderPhone = "sender phone";
			obj.SenderState = "sender state";
			obj.SenderSuburb = "sender suburb";
			obj.SenderZip = "sender zip";
			obj.SpecialInstructions = "";
			obj.Status = OrderStatus.Picked;

		obj.Save();


			//var a = _dataService.AllOrders();
			//var b = await _dataService.AllOrdersAsync();

			//var id = b[1].Id;

			//var c = _dataService.GetOrder(id);
			//var d = await _dataService.GetOrderAsync(id);

			//var e = _dataService.AllUsers();
			//var f = await _dataService.AllUsersAsync();

			//var ida = f[1].Id;

			//var g = _dataService.GetUser(ida);
			//var h = await _dataService.GetUserAsync(ida);

		}

		private void AddUsers()
		{
			//_dataService.DeleteAllUsers();



			var userobj = new BusinessObjects.User();
			userobj.Id = 0;
			userobj.Type = UserType.admin;
			userobj.ClientId = 2;
			userobj.FirstName = "Matt";
			userobj.LastName = "Hemphill";
			userobj.Email = "email";
			userobj.Password = "password";
			userobj.ProposedPassword = "proposed password";
			userobj.AccessToken = "access token";
			userobj.FacebookId = "facebook id";
			userobj.FacebookToken = "facebook token";
			userobj.FacebookDetailsJson = "facebook details Json";
			userobj.Token = "token";
			userobj.Status = UserStatus.loggedin;
			userobj.Manufacturer = "manufacturer";
			userobj.Model = "model";
			userobj.Serial = "serial";
			userobj.AppVersion = "app version";
			userobj.Save();

			userobj = new BusinessObjects.User();
			userobj.Id = 0;
			userobj.Type = UserType.courier;
			userobj.ClientId = 2;
			userobj.FirstName = "Rod";
			userobj.LastName = "";
			userobj.Email = "email";
			userobj.Password = "password";
			userobj.ProposedPassword = "proposed password";
			userobj.AccessToken = "access token";
			userobj.FacebookId = "facebook id";
			userobj.FacebookToken = "facebook token";
			userobj.FacebookDetailsJson = "facebook details Json";
			userobj.Token = "token";
			userobj.Status = UserStatus.loggedin;
			userobj.Manufacturer = "manufacturer";
			userobj.Model = "model";
			userobj.Serial = "serial";
			userobj.AppVersion = "app version";
			userobj.Save();

			userobj = new BusinessObjects.User();
			userobj.Id = 0;
			userobj.Type = UserType.picker;
			userobj.ClientId = 2;
			userobj.FirstName = "Karen";
			userobj.LastName = "";
			userobj.Email = "email";
			userobj.Password = "password";
			userobj.ProposedPassword = "proposed password";
			userobj.AccessToken = "access token";
			userobj.FacebookId = "facebook id";
			userobj.FacebookToken = "facebook token";
			userobj.FacebookDetailsJson = "facebook details Json";
			userobj.Token = "token";
			userobj.Status = UserStatus.loggedin;
			userobj.Manufacturer = "manufacturer";
			userobj.Model = "model";
			userobj.Serial = "serial";
			userobj.AppVersion = "app version";
			userobj.Save();

			userobj = new BusinessObjects.User();
			userobj.Id = 0;
			userobj.Type = UserType.admin;
			userobj.ClientId = 2;
			userobj.FirstName = "Turkan";
			userobj.LastName = "";
			userobj.Email = "email";
			userobj.Password = "password";
			userobj.ProposedPassword = "proposed password";
			userobj.AccessToken = "access token";
			userobj.FacebookId = "facebook id";
			userobj.FacebookToken = "facebook token";
			userobj.FacebookDetailsJson = "facebook details Json";
			userobj.Token = "token";
			userobj.Status = UserStatus.loggedin;
			userobj.Manufacturer = "manufacturer";
			userobj.Model = "model";
			userobj.Serial = "serial";
			userobj.AppVersion = "app version";
			userobj.Save();

		}


	}
}
