﻿using System;
using System.Collections.Generic;
using MeerkatDelivery.BusinessObjects;
using Microsoft.Practices.Unity;

namespace MeerkatDelivery
{
	public enum BusinessType
	{
		Flowers,
		General
	}
	
	public static class Settings
	{
		public static string AppVersion = "0.0.02";
		public static bool DemoModeAllowed = true;
		public static bool DebugMode = true; 
		public static bool EncryptionTurnedOn = false;
		public static int DefaultClientId = 2;


		public static string BaseRestApiUrl = "http://52.40.77.254/apirest";
		public static string AuthoriseUrl = "http://52.40.77.254/apirest";
		public static string TestingApiUrl = "http://202.129.124.134/apirest";

		public static int CheckLocationMinutes = 5;     				// If need to know current location and more than this minutes since last, check again
		public static int GeolocationTimeoutMilliseconds = 4000;
		public static int MaxDistanceFromCurrentLocation = 0; 		// Kilometers. Ignore locations more than this distance away; Zero means ignore this.
        public static double MinMapRadius = 5.0;                    // Kilometers. Minimum scale of map.

		public static IUnityContainer Container;
		public static string BrandColor = "#fcbd36";
		public static string BaseTextColor = "#ffffff";
		public static BusinessType BusinessType = BusinessType.Flowers;

		public static string DefaultPin = "RedPin.png";

		public static double DefaultListViewRowHeight = 44.88; // this is a scaled value based on Forms size and actual physical size 
																//TODO I hope it's not different for Android)

		public static string HOCKEYAPP_APPID_IOS = "9c3bb345cc0e4e579bbc8095a746f76d";
		public static string HOCKEYAPP_APPID_DROID = "7abd61a685cd4c129c2fba466375e8f6";

		public static double LandscapeScreenProportion = 0.50; // When in landscape, width should be this proportion of height.
		public static byte[] SecretKey = new byte[] { 0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c };

	
		public static Dictionary<string, string> OrderStatusBackgroundColor = new Dictionary<string, string>()
				{
			{"Initialised", "White"},
			{"Ordered", "White"},
			{"Cancelled", "White"},
			{"Picked", "#fcf7bc"},
			{"Loaded", "#ccff74"},
			{"Delivering", "#ccff74"},
			{"Delivered", "Silver"},
			{"FailedDelivery", "Silver"}
        };
		public static Dictionary<string, string> OrderStatusTextColor = new Dictionary<string, string>()
				{
			{"Initialised", "Black"},
			{"Ordered", "Black"},
			{"Cancelled", "Red"},
			{"Picked", "Black"},
			{"Loaded", "Black"},
			{"Delivering", "Black"},
			{"Delivered", "Black"},
			{"FailedDelivery", "Red"}		
				};
		public static Dictionary<string, string> OrderStatusPin = new Dictionary<string, string>()
				{
			{"Initialised", "RedPin"},
			{"Ordered", "RedPin"},
			{"Cancelled", "RedPin"},
			{"Picked", "RedPin"},
			{"Loaded", "RedPin"},
			{"Delivering", "GrayPin"},
			{"Delivered", "GrayPin"},
			{"FailedDelivery", "GrayPin"}
				};
	
	}
}
