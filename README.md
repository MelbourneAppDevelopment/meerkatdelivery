# Meerkat Delivery - Using Maps with Xamarin Forms #

This is the repo for the Melbourne Xamarin Meetup presentation demonstrating how to use Google Maps within a Xamarin Forms App.
Being a Xamarin Forms app it has been constructed for both iOS and Android. The active implementation is iOS and has never been built for Android. A notable omission is the Android custom renderer for handling pin annotations.

It includes:

* converting street addresses to map positions
* show multiple pins on the same map
* click on a pin to show which order it is for “Annotations”
* zoom in on the map to fit in all orders
* change the pin style for some order (e.g. completed orders)
* turn by turn directions
* panning orders and size the map


# License #

The MIT License (MIT)

Copyright (c) 2017 Melbourne App Development

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
