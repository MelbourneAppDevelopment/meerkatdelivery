using System;
using System.Net;
using SystemConfiguration;
using CoreFoundation;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using MeerkatDelivery.Services;

public class Reachability : IReachability
{
	public static string HostName = "www.google.com";

	#region Interfaces
	private string _result = "";
	public string GetIPAddress()
	{
		if (_result != "")
			return _result;
		
		foreach (var netInterface in NetworkInterface.GetAllNetworkInterfaces())
		{
			if (netInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
				netInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
			{
				foreach (var addrInfo in netInterface.GetIPProperties().UnicastAddresses)
				{
					if (addrInfo.Address.AddressFamily == AddressFamily.InterNetwork)
					{
						var ipAddress = addrInfo.Address;
						_result += ipAddress.ToString() + " ";
					}
				}
			}
		}
		return _result;
	}


	// Is the host reachable with the current network configuration
	public bool IsHostReachable(string host = null)
	{
		if (string.IsNullOrEmpty(host))
			host = HostName;

		using (var r = new NetworkReachability(host))
		{
			NetworkReachabilityFlags flags;

			if (r.TryGetFlags(out flags))
			{
				return IsReachableWithoutRequiringConnection(flags);
			}
		}
		return false;
	}

	//public bool IsInternetConnected()
	//{
	//	try {
	//		if (   RemoteHostStatus ()          == NetworkStatus.NotReachable 
	//			//			&& LocalWifiConnectionStatus () == NetworkStatus.NotReachable 
	//			//			&& InternetConnectionStatus ()  == NetworkStatus.NotReachable 
	//		){
	//			return false;
	//		}
	//		return true;
	//	} catch (Exception e){
	//		return false;
	//	}
	//}
//
//	public static bool IsInternetConnected()
//	{
//		if (   RemoteHostStatus ()          == NetworkStatus.NotReachable 
//			&& LocalWifiConnectionStatus () == NetworkStatus.NotReachable 
//			&& InternetConnectionStatus ()  == NetworkStatus.NotReachable 
//		){
//			return false;
//		}
//		return true;
//	}

	private NetworkReachability remoteHostReachability;

	public NetworkStatus RemoteHostStatus()
	{
		NetworkReachabilityFlags flags;
		bool reachable;

		if (remoteHostReachability == null)
		{
			remoteHostReachability = new NetworkReachability(HostName);

			// Need to probe before we queue, or we wont get any meaningful values
			// this only happens when you create NetworkReachability from a hostname
			reachable = remoteHostReachability.TryGetFlags(out flags);

			//remoteHostReachability.SetNotification(OnChange);
			remoteHostReachability.Schedule(CFRunLoop.Current, CFRunLoop.ModeDefault);
		}
		else
			reachable = remoteHostReachability.TryGetFlags(out flags);			

		if (!reachable) {
			return NetworkStatus.Disconnected;
		}

		if (!IsReachableWithoutRequiringConnection (flags)) {
			return NetworkStatus.Disconnected;
		}

		if ((flags & NetworkReachabilityFlags.IsWWAN) != 0)
			return NetworkStatus.ConnectedViaMobile;

		return NetworkStatus.ConnectedViaWifi;
	}

	public NetworkStatus InternetConnectionStatus()
	{
		NetworkReachabilityFlags flags;
		bool defaultNetworkAvailable = IsNetworkAvailable(out flags);
		if (defaultNetworkAvailable && ((flags & NetworkReachabilityFlags.IsDirect) != 0))
		{
			return NetworkStatus.Disconnected;
		}
		else if ((flags & NetworkReachabilityFlags.IsWWAN) != 0)
			return NetworkStatus.ConnectedViaMobile;
		else if (flags == 0)
			return NetworkStatus.Disconnected;
		return NetworkStatus.ConnectedViaWifi;
	}

	public NetworkStatus LocalWifiConnectionStatus()
	{
		NetworkReachabilityFlags flags;
		if (IsAdHocWiFiNetworkAvailable(out flags))
		{
			if ((flags & NetworkReachabilityFlags.IsDirect) != 0)
				return NetworkStatus.ConnectedViaWifi;
		}
		return NetworkStatus.Disconnected;
	}

	#endregion


	public static bool IsReachableWithoutRequiringConnection(NetworkReachabilityFlags flags)
    {
        // Is it reachable with the current network configuration?
        bool isReachable = (flags & NetworkReachabilityFlags.Reachable) != 0;

        // Do we need a connection to reach it?
        bool noConnectionRequired = (flags & NetworkReachabilityFlags.ConnectionRequired) == 0;

        // Since the network stack will automatically try to get the WAN up,
        // probe that
        if ((flags & NetworkReachabilityFlags.IsWWAN) != 0)
            noConnectionRequired = true;

        return isReachable && noConnectionRequired;
    }

    //

    //
    // Returns true if it is possible to reach the AdHoc WiFi network
    // and optionally provides extra network reachability flags as the
    // out parameter
    //
    static NetworkReachability adHocWiFiNetworkReachability;

    public static bool IsAdHocWiFiNetworkAvailable(out NetworkReachabilityFlags flags)
    {
        if (adHocWiFiNetworkReachability == null)
        {
            adHocWiFiNetworkReachability = new NetworkReachability(new IPAddress(new byte [] { 169, 254, 0, 0 }));
            //adHocWiFiNetworkReachability.SetNotification(OnChange);
            adHocWiFiNetworkReachability.Schedule(CFRunLoop.Current, CFRunLoop.ModeDefault);
        }
        return adHocWiFiNetworkReachability.TryGetFlags(out flags) && IsReachableWithoutRequiringConnection(flags);
    }

    static NetworkReachability defaultRouteReachability;

    static bool IsNetworkAvailable(out NetworkReachabilityFlags flags)
    {
        if (defaultRouteReachability == null)
        {
            defaultRouteReachability = new NetworkReachability(new IPAddress(0));
            //defaultRouteReachability.SetNotification(OnChange);
            defaultRouteReachability.Schedule(CFRunLoop.Current, CFRunLoop.ModeDefault);
        }
        return defaultRouteReachability.TryGetFlags(out flags) && IsReachableWithoutRequiringConnection(flags);
    }

}

