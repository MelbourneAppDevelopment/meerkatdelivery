﻿//using System;
//using System.Collections.Generic;
//using CustomRenderer;
//using CustomRenderer.iOS;
//using Foundation;
//using MeerkatDelivery.CustomControls;
//using UIKit;
//using Xamarin.Forms;
//using Xamarin.Forms.Platform.iOS;

//[assembly: ExportRenderer(typeof(CustomListView), typeof(NativeiOSListViewRenderer))]
//namespace CustomRenderer.iOS
//{
	
//// See https://github.com/xamarin/xamarin-forms-samples/tree/master/CustomRenderers/ListView



//	public class NativeiOSListViewRenderer : ListViewRenderer
//	{
//		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
//		{
//			base.OnElementChanged(e);

//			if (e.OldElement != null)
//			{
//				// Unsubscribe
//			}

//			if (e.NewElement != null)
//			{
//				Control.Source = new NativeiOSListViewSource(e.NewElement as CustomListView);
//			}
//		}

//		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
//		{
//			base.OnElementPropertyChanged(sender, e);

//			if (e.PropertyName == CustomListView.ItemsProperty.PropertyName)
//			{
//				Control.Source = new NativeiOSListViewSource(Element as CustomListView);
//			}
//		}
//	}

//	public class NativeiOSListViewSource : UITableViewSource
//	{
//		// declare vars
//		IList<DataSource> tableItems;
//CustomListView listView;
//		readonly NSString cellIdentifier = new NSString("TableCell");

//		public IEnumerable<DataSource> Items
//		{
//			//get{ }
//			set
//			{
//				tableItems = value.ToList();
//			}
//		}

//		public NativeiOSListViewSource(CustomListView view)
//		{
//			tableItems = view.Items.ToList();
//			listView = view;
//		}

//		/// <summary>
//		/// Called by the TableView to determine how many cells to create for that particular section.
//		/// </summary>
//		public override nint RowsInSection(UITableView tableview, nint section)
//		{
//			return tableItems.Count;
//		}

//		#region user interaction methods

//		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
//		{
//			listView.NotifyItemSelected(tableItems[indexPath.Row]);
//			Console.WriteLine("Row " + indexPath.Row.ToString() + " selected");
//			tableView.DeselectRow(indexPath, true);
//		}

//		public override void RowDeselected(UITableView tableView, NSIndexPath indexPath)
//		{
//			Console.WriteLine("Row " + indexPath.Row.ToString() + " deselected");
//		}

//		#endregion

//		/// <summary>
//		/// Called by the TableView to get the actual UITableViewCell to render for the particular section and row
//		/// </summary>
//		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
//		{
//			// request a recycled cell to save memory
//			NativeiOSListViewCell cell = tableView.DequeueReusableCell(cellIdentifier) as NativeiOSListViewCell;

//			// if there are no cells to reuse, create a new one
//			if (cell == null)
//			{
//				cell = new NativeiOSListViewCell(cellIdentifier);
//			}

//			if (String.IsNullOrWhiteSpace(tableItems[indexPath.Row].ImageFilename))
//			{
//				cell.UpdateCell(tableItems[indexPath.Row].Name
//					, tableItems[indexPath.Row].Category
//					, null);
//			}
//			else
//			{
//				cell.UpdateCell(tableItems[indexPath.Row].Name
//					, tableItems[indexPath.Row].Category
//					, UIImage.FromFile("Images/" + tableItems[indexPath.Row].ImageFilename + ".jpg"));
//			}

//			return cell;
//		}
//	}

//	/// <summary>
//	/// Sample of a custom cell layout, taken from the iOS docs at
//	/// http://developer.xamarin.com/guides/ios/user_interface/tables/part_3_-_customizing_a_table's_appearance/
//	/// </summary>
//	public class NativeiOSListViewCell : UITableViewCell
//	{
//		UILabel headingLabel, subheadingLabel;
//		UIImageView imageView;

//		public NativeiOSListViewCell(NSString cellId) : base(UITableViewCellStyle.Default, cellId)
//		{
//			SelectionStyle = UITableViewCellSelectionStyle.Gray;

//			ContentView.BackgroundColor = UIColor.FromRGB(218, 255, 127);

//			imageView = new UIImageView();

//			headingLabel = new UILabel()
//			{
//				Font = UIFont.FromName("Cochin-BoldItalic", 22f),
//				TextColor = UIColor.FromRGB(127, 51, 0),
//				BackgroundColor = UIColor.Clear
//			};

//			subheadingLabel = new UILabel()
//			{
//				Font = UIFont.FromName("AmericanTypewriter", 12f),
//				TextColor = UIColor.FromRGB(38, 127, 0),
//				TextAlignment = UITextAlignment.Center,
//				BackgroundColor = UIColor.Clear
//			};

//			ContentView.Add(headingLabel);
//			ContentView.Add(subheadingLabel);
//			ContentView.Add(imageView);
//		}

//		public void UpdateCell(string caption, string subtitle, UIImage image)
//		{
//			headingLabel.Text = caption;
//			subheadingLabel.Text = subtitle;
//			imageView.Image = image;
//		}

//		public override void LayoutSubviews()
//		{
//			base.LayoutSubviews();

//			//headingLabel.Frame = new CoreGraphics.CGRect(5, 4, ContentView.Bounds.Width - 63, 25);
//			//subheadingLabel.Frame = new CoreGraphics.CGRect(100, 18, 100, 20);
//			//imageView.Frame = new CoreGraphics.CGRect(ContentView.Bounds.Width - 63, 5, 33, 33);
//		}
//	}
//}