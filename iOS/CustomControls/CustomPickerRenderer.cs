﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using MeerkatDelivery;
using MeerkatDelivery.CustomControls;
using MeerkatDelivery.iOS;
using System;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace MeerkatDelivery.iOS
{
	public class CustomPickerRenderer : PickerRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
		{
			base.OnElementChanged(e);

			//this.Control.TextColor = UIColor.White;

			//this.Control.BackgroundColor = UIColor.Clear;
			this.Control.BorderStyle = UITextBorderStyle.None;
			//this.Layer.BorderWidth = 1.0f;
			//this.Layer.CornerRadius = 4.0f;
			//this.Layer.MasksToBounds = true;
			//this.Layer.BorderColor = UIColor.White.CGColor;
		}
	}
}
