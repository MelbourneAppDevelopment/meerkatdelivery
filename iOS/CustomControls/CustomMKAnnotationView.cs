﻿using System;
using MapKit;


namespace MeerkatDelivery.CustomControls
{
	public class CustomMKAnnotationView : MKAnnotationView
	{
		public int OrderId { get; set; }
		public string Url { get; set; }
		public string PinUrl { get; set; }

		public CustomMKAnnotationView(IMKAnnotation annotation, int orderId)
		{
			OrderId = orderId;
			this.Annotation = annotation;
		}
	}
}
