﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using CoreGraphics;
using MapKit;
using MeerkatDelivery.CustomControls;
using MeerkatDelivery.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace MeerkatDelivery.iOS
{
	public class CustomMapRenderer : MapRenderer
	{
		UIView customPinView;
		//ObservableCollection<CustomPin> customPins;
		CustomMap formsMap;


		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				var nativeMap = Control as MKMapView;
				nativeMap.GetViewForAnnotation = null;
				nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
				nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
				nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
			}

			if (e.NewElement != null)
			{
				formsMap = (CustomMap)e.NewElement;     // Cast to Meerkat to get access to annotation
				//var formsMap = (CustomMap)e.NewElement;
				//customPins = formsMap.CustomPins;

				var nativeMap = Control as MKMapView;
				nativeMap.GetViewForAnnotation = GetViewForAnnotation;
				if (nativeMap.GetViewForAnnotation != null)
				{
					nativeMap.CalloutAccessoryControlTapped += OnCalloutAccessoryControlTapped;
					nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
					nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;
				}
			}
		}

        MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            if (annotation is MKUserLocation)
                return null;

            var anno = annotation as MKPointAnnotation;
            var customPin = GetCustomPin(anno);
            if (customPin == null)
            {
                Debug.WriteLine("Custom pin not found");
                return null;
            }

            annotationView = mapView.DequeueReusableAnnotation(customPin.OrderId.ToString());
            if (annotationView == null)
            {
                annotationView = new CustomMKAnnotationView(annotation, customPin.OrderId);

                annotationView.Image = UIImage.FromFile(customPin.PinUrl);

                annotationView.CalloutOffset = new CGPoint(0, 0);
                annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("monkey.png"));
                annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
                ((CustomMKAnnotationView)annotationView).OrderId = customPin.OrderId;
                ((CustomMKAnnotationView)annotationView).Url = customPin.Url;
            }
            annotationView.CanShowCallout = true;

            return annotationView;
        }

		CustomPin GetCustomPin(MKPointAnnotation anno)
		{
			// try a few times in case pin corords are changing (especially in teting case)
			for (var i = 0; i < 5; i++)
			{
				var pin = LocalGetCustomPin(anno);
				if (pin != null)
					return pin;
			}

            return null;
		}
		CustomPin LocalGetCustomPin(MKPointAnnotation anno)
		{

			//foreach (var pin in customPins)
			foreach (var pin in formsMap.CustomPins)
			{
				if (pin != null && pin.Pin != null && pin.Pin.Position != null && anno != null)
					if (pin.Pin.Position.Latitude == anno.Coordinate.Latitude && pin.Pin.Position.Longitude == anno.Coordinate.Longitude)
						return pin;
			}
			return null;
		}
	
		void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
			var customView = e.View as CustomMKAnnotationView;
			customPinView = new UIView();

			//if (customView.OrderId == "Xamarin")
			//{
			//	customPinView.Frame = new CGRect(0, 0, 200, 84);
			//	var image = new UIImageView(new CGRect(0, 0, 200, 84));
			//	image.Image = UIImage.FromFile("xamarin.png");
			//	customPinView.AddSubview(image);
			//	customPinView.Center = new CGPoint(0, -(e.View.Frame.Height + 75));
			//	e.View.AddSubview(customPinView);
			//}
		}

		void OnCalloutAccessoryControlTapped(object sender, MKMapViewAccessoryTappedEventArgs e)
		{
			var customView = e.View as CustomMKAnnotationView;
			if (!string.IsNullOrWhiteSpace(customView.Url))
			{
				UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(customView.Url));
			}
		}

		void OnDidDeselectAnnotationView(object sender, MKAnnotationViewEventArgs e)
		{
			if (!e.View.Selected)
			{
				customPinView.RemoveFromSuperview();
				customPinView.Dispose();
				customPinView = null;
			}
		}

	}
}
