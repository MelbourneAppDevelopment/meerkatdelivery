using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using AstralPump.Droid;
using Xamarin.Forms;

namespace MeerkatDelivery.Droid
{
	[Activity(Theme = "@style/MyTheme.Splash", Icon = "@drawable/icon", MainLauncher = true, NoHistory = true)]
	public class SplashScreen : AppCompatActivity
	{
		public override void OnCreate(Bundle bundle, PersistableBundle persistentState)
		{
			base.OnCreate(bundle, persistentState);
		}

		// Launches the startup task
		protected override void OnResume()
		{
			base.OnResume();
			Task startupWork = new Task(() => { StartupTasks(); });
			startupWork.Start();
		}

		// Runs background work that happens behind the splash screen
		void StartupTasks()
		{
			// Performing some startup work that takes a bit of time.

			//await Task.Delay(8000); // Simulate a bit of startup work.
			// Startup work is finished - starting MainActivity.

			RunOnUiThread(() => StartActivity(typeof(MainActivity)));
		}
	}
}
