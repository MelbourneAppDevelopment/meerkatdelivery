﻿using System;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Util;
using Android.Widget;
using MeerkatDelivery.CustomControls;
using MeerkatDelivery.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace MeerkatDelivery.Droid
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> args)
		{
			base.OnElementChanged(args);

			if (Control != null)
			{
				//DXStyleUtils_Droid utils = new DXStyleUtils_Droid();

				EditText textField = (EditText)Control;

				// Set colors
				textField.SetBackgroundColor(Android.Graphics.Color.Transparent);
				//textField.SetTextColor(Android.Graphics.Color.White);
				//textField.SetHintTextColor(Android.Graphics.Color.Gray);
				textField.SetHighlightColor(Android.Graphics.Color.White);

				// Set font
				//textField.SetTextSize(ComplexUnitType.Dip, (float)DXStyle.FontSmall.Size);
				textField.SetTypeface(Typeface.Create("Helvetica", TypefaceStyle.Normal), TypefaceStyle.Normal);


				// Misc
				textField.SetSingleLine(true);
				textField.SetHorizontallyScrolling(true);
				textField.SetSelectAllOnFocus(false);
				textField.Ellipsize = TextUtils.TruncateAt.End;
				textField.SetCursorVisible(true);
			}
		}
	}
}