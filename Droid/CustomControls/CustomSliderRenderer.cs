﻿using System;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.Support.V7.Widget;
using MeerkatDelivery.CustomControls;
using MeerkatDelivery.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomSlider), typeof(CustomSliderRenderer))]
namespace MeerkatDelivery.Droid
{
	public class CustomSliderRenderer : SliderRenderer
	{
		private int lastHeight = -1;
		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			var view = (CustomSlider)this.Element;

			if (Control != null)
			{
				// make the seek bar button 8% of the size of the screen width (min 40)
				int height = (int)Math.Round(0.08 * (double)App.ScreenWidth);
				height = (height < 40) ? 40 : height;

				if (height == lastHeight)
					return;
				lastHeight = height;

				Control.ProgressDrawable.SetColorFilter(Android.Graphics.Color.White,PorterDuff.Mode.SrcIn);
				Control.Thumb.SetColorFilter(Android.Graphics.Color.White, PorterDuff.Mode.SrcIn);

				float cornerPX = height;
				var thumbHeight = height;

				if (!Resources.GetBoolean(MeerkatDelivery.Droid.Resource.Boolean.is_phone))
					thumbHeight = thumbHeight > 50 ? 50 : height;

				ShapeDrawable thumb = new ShapeDrawable(new RoundRectShape(new float[] { cornerPX,cornerPX,cornerPX,cornerPX,cornerPX,cornerPX,cornerPX,cornerPX},null,null));
				thumb.Paint.Color = Android.Graphics.Color.White;
				thumb.SetIntrinsicHeight(thumbHeight);
				thumb.SetIntrinsicWidth(thumbHeight);
				Control.SetThumb(thumb);


			}
		}
	}
}
