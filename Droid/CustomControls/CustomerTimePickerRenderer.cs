﻿//using System;
//using Xamarin.Forms.Platform.Android;
//using Xamarin.Forms;
//using MeerkatDelivery.Droid;
//using Android.Views;
//using Android.Widget;
//using Android.Content;
//using Android.App;
//using Android.Runtime;
//using MeerkatDelivery.CustomControls;

//[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
//namespace MeerkatDelivery.Droid
//{
//	public class CustomTimePickerRenderer : TimePickerRenderer
//	{
//		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
//		{
//			base.OnElementChanged(e);

//			TimePickerDialogWithIntervals timePickerDlg = new TimePickerDialogWithIntervals(this.Context, new EventHandler<TimePickerDialogWithIntervals.TimeSetEventArgs>(UpdateDuration),
//				Element.Time.Hours, Element.Time.Minutes, true);

//			var control = new EditText(this.Context);

//			control.SetTextColor(Color.Gray.ToAndroid());
//			control.Focusable = false;
//			control.FocusableInTouchMode = false;
//			control.Clickable = false;

//			var a = e.NewElement.Time.ToString();

//			control.Click += (sender, ea) => {
//				timePickerDlg.Show();
//			};
//			control.Text = Element.Time.Hours.ToString("00") + ":" + Element.Time.Minutes.ToString("00");

//			SetNativeControl(control);
//		}

//		void UpdateDuration(object sender, Android.App.TimePickerDialog.TimeSetEventArgs e)
//		{
//			Element.Time = new TimeSpan(e.HourOfDay, e.Minute, 0);
//			Control.Text = Element.Time.Hours.ToString("00") + ":" + Element.Time.Minutes.ToString("00");
//		}
//	}

//	public class TimePickerDialogWithIntervals : TimePickerDialog
//	{
//		public const int TimePickerInterval = 15;
//		private bool _ignoreEvent = false;

//		public TimePickerDialogWithIntervals(Context context, EventHandler<TimeSetEventArgs> callBack, int hourOfDay, int minute, bool is24HourView)
//		: base(context, (sender, e) =>
//		{
//			var newHourOfDay = e.HourOfDay;
//			var newMinutes = (int)Math.Round((double)e.Minute / (double)TimePickerInterval, 0);
//			newMinutes = newMinutes * TimePickerInterval;
//			if (newMinutes == 60)
//			{
//				newMinutes = 0;
//				if (newHourOfDay == 23)
//					newHourOfDay = 0;
//				else
//					newHourOfDay += 1;
//			}

//			var b = e.Minute;
//			var c = is24HourView;

//			callBack(sender, new TimeSetEventArgs(newHourOfDay, newMinutes));
//		}, hourOfDay, minute, is24HourView)
//						//callBack(sender, new TimeSetEventArgs(e.HourOfDay, (e.Minute % 4) * TimePickerInterval));
//		//}, hourOfDay, minute / TimePickerInterval, is24HourView)
//		{
//		}
//		protected TimePickerDialogWithIntervals(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
//		{
//		}

//		public override void SetView(Android.Views.View view)
//		{
//			SetupMinutePicker(view);
//			base.SetView(view);
//		}

//		void SetupMinutePicker(Android.Views.View view)
//		{
//			var numberPicker = FindMinuteNumberPicker(view as ViewGroup);
//			if (numberPicker != null)
//			{
//				numberPicker.MinValue = 0;
//				numberPicker.MaxValue = 7;
//				numberPicker.SetDisplayedValues(new String[] { "00", "15", "30", "45", "00", "15", "30", "45" });
//			}
//		}

//		protected override void OnCreate(Android.OS.Bundle savedInstanceState)
//		{
//			base.OnCreate(savedInstanceState);
//			GetButton((int)DialogButtonType.Negative).Visibility = Android.Views.ViewStates.Gone;
//			this.SetCanceledOnTouchOutside(false);

//		}

//		// Recurse all children until find a child that successfully casts as a NumberPicker
//		private NumberPicker FindMinuteNumberPicker(ViewGroup viewGroup)
//		{
//			for (var i = 0; i < viewGroup.ChildCount; i++)
//			{
//				var child = viewGroup.GetChildAt(i);
//				var numberPicker = child as NumberPicker;
//				if (numberPicker != null)
//				{
//					if (numberPicker.MaxValue == 59)
//					{
//						return numberPicker;
//					}
//				}

//				var childViewGroup = child as ViewGroup;
//				if (childViewGroup != null)
//				{
//					var childResult = FindMinuteNumberPicker(childViewGroup);
//					if (childResult != null)
//						return childResult;
//				}
//			}

//			return null;
//		}
//	}
//}
