﻿using Android.OS;
using MeerkatDelivery.Services;

namespace MeerkatDelivery.Droid.Services
{
	public class DeviceService : IDeviceService
	{
		public string DeviceOrSimulator
		{
			get
			{
				return "Unknown";
			}
		}

		private string _manufacturer = "";
		public string Manufacturer
		{
			get
			{
				if (_manufacturer == "")
					_manufacturer = Build.Manufacturer;
				return _manufacturer;
			}
		}

		private string _model = "";
		public string Model
		{
			get
			{
				if (_model == "")
					_model = Build.Model;
				return _model;
			}
		}

		private string _serial = "";
		public string Serial
		{
			// WARNING: NOT PERSISTENT - NEED TO STORE IN COOKIE
			get
			{
				if (_serial == "")
					_serial = Build.Serial;
				return _serial;
			}
		}

	}
}

